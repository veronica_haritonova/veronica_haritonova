
public class Runner {
    public static void main(String[] args) {
	C c = new C();
	System.out.println("Class reference");
	c.method();
	A a = new C();
	System.out.println("Interface A reference");
	a.method();
	B b = new C();
	System.out.println("Interface B reference");
	b.method();
}
}