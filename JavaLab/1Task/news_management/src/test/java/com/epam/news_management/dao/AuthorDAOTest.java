package com.epam.news_management.dao;

import com.epam.news_management.entity.Author;
import com.epam.news_management.exception.DAOException;
import org.dbunit.DBTestCase;
import org.dbunit.PropertiesBasedJdbcDatabaseTester;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.oracle.OracleDataTypeFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import java.io.FileInputStream;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * @author Veronica_Haritonova
 *         Class for testing basic operations with authors in database
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:test-spring.xml")
@TestExecutionListeners({
        DependencyInjectionTestExecutionListener.class
}
)
public class AuthorDAOTest extends DBTestCase {

    @Autowired
    private IAuthorDAO authorDAOImpl;
    @Autowired
    private INewsDAO newsDAO;

    /**
     * Set configuration parameters for connection with database
     */
    public AuthorDAOTest() {
        super();
        ResourceBundle bundle = ResourceBundle.getBundle("test-database", new Locale("en", "EN"));
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_DRIVER_CLASS, bundle.getString("db.driver"));
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_CONNECTION_URL, bundle.getString("db.path"));
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_USERNAME, bundle.getString("db.login"));
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_PASSWORD, bundle.getString("db.password"));
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_SCHEMA, bundle.getString("db.schema"));
    }

    /**
     * Get initial data for database
     */
    @Override
    public IDataSet getDataSet() throws Exception {
        return new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/dataset.xml"));
    }

    /**
     * Add special configuration property for including all oracle data types
     */
    protected void setUpDatabaseConfig(DatabaseConfig config) {
        config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY,
                new OracleDataTypeFactory());
    }

    /**
     * Put initial data to database before every test
     *
     * @throws Exception
     */
    @Before
    public void setDatabaseData() throws Exception {
        setUp();
    }

    /**
     * Test method which inserts author into database
     *
     * @throws DAOException
     */
    @Test
    public void addAuthor() throws DAOException {
        authorDAOImpl.add(new Author("Nick"));
        assertEquals("Amount of updated tags is incorrect", 4, authorDAOImpl.selectAll().size());

    }

    /**
     * Test method which updates author
     *
     * @throws DAOException
     */
    @Test
    public void updateAuthor() throws DAOException {
        Long authorId = 2L;
        authorDAOImpl.update(new Author(authorId, "Nick"));
        assertEquals("Updated name is incorrect", "Nick", authorDAOImpl.get(authorId).getAuthorName());

    }

    /**
     * Test method which deletes author with given id
     *
     * @throws DAOException
     */
    @Test
    public void deleteAuthor() throws DAOException {
        Long authorId = 3L;
        authorDAOImpl.deleteAuthorInNews(authorId);
        authorDAOImpl.delete(authorId);
        assertEquals("Amount of updated tags is incorrect", 2, authorDAOImpl.selectAll().size());
    }

    /**
     * Test method which deletes author with given id
     *
     * @throws DAOException
     */
    @Test
    public void deleteAuthorInNews() throws DAOException {
        Long authorId = 3L;
        Long newsId = 4L;
        authorDAOImpl.deleteAuthorInNews(authorId);
        assertEquals("Amount of updated tags is incorrect", 0, newsDAO.countAuthors(newsId));
    }

    /**
     * Test method which gets all authors from database
     *
     * @throws DAOException
     */
    @Test
    public void selectAllAuthors() throws DAOException {
        assertEquals("Amount of updated tags is incorrect", 3, authorDAOImpl.selectAll().size());
    }

    /**
     * Test method which finds author by its id
     *
     * @throws DAOException
     */
    @Test
    public void findById() throws DAOException {
        Long authorId = 3L;
        Author authorExpected = new Author(authorId, "Patrick");
        Author author = authorDAOImpl.get(authorId);
        assertEquals("Name of found author is incorrect", authorExpected, author);
    }

    /**
     * Test method which sets expired date to author with given id
     *
     * @throws DAOException
     */
    @Test
    public void setExpired() throws DAOException {
        Long authorId = 2L;
        authorDAOImpl.setExpired(authorId);
        assertNotNull("Expired date wasn't set", authorDAOImpl.get(authorId).getExpired());
    }

}
