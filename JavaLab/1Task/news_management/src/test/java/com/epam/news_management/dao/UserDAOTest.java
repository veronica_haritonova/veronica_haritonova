package com.epam.news_management.dao;

import com.epam.news_management.dao.impl.UserDAOImpl;
import com.epam.news_management.entity.User;
import com.epam.news_management.exception.DAOException;
import org.dbunit.DBTestCase;
import org.dbunit.PropertiesBasedJdbcDatabaseTester;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.oracle.OracleDataTypeFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * @author Veronica_Haritonova
 *         Class for testing basic operations with user in database
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:test-spring.xml")
@TestExecutionListeners({
        DependencyInjectionTestExecutionListener.class
}
)
public class UserDAOTest extends DBTestCase {
    @Autowired
    private UserDAOImpl userDAOImpl;


    /**
     * Set configuration parameters for connection with database
     */
    public UserDAOTest() {
        super();
        ResourceBundle bundle = ResourceBundle.getBundle("test-database", new Locale("en", "EN"));
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_DRIVER_CLASS, bundle.getString("db.driver"));
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_CONNECTION_URL, bundle.getString("db.path"));
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_USERNAME, bundle.getString("db.login"));
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_PASSWORD, bundle.getString("db.password"));
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_SCHEMA, bundle.getString("db.schema"));
    }

    /**
     * Get initial data for database
     */
    @Override
    public IDataSet getDataSet() throws Exception {
        return new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/dataset.xml"));
    }

    /**
     * Add special configuration property for including all oracle data types
     */
    @Override
    protected void setUpDatabaseConfig(DatabaseConfig config) {
        config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY,
                new OracleDataTypeFactory());
    }

    /**
     * Put initial data to database before every test
     *
     * @throws Exception
     */
    @Before
    public void setDatabaseData() throws Exception {
        setUp();
    }

    /**
     * Test method which inserts user into database
     *
     * @throws DAOException
     */
    @Test
    public void addUser() throws DAOException {
        userDAOImpl.add(new User(1L, "Tom", "Tom", "1234"));
        assertEquals("Amount of all users after adding new user is incorrect", 4, userDAOImpl.selectAll().size());
    }

    /**
     * Test method which deletes user's roles from database
     *
     * @throws DAOException
     */
    @Test
    public void deleteUserRoles() throws DAOException {
        userDAOImpl.deleteRoles(2L);
        assertEquals("Amount of all roles after deleting is incorrect", 0, userDAOImpl.getRoles(2L).size());
    }

    /**
     * Test method which deletes user from database
     *
     * @throws DAOException
     */
    @Test
    public void deleteUser() throws DAOException {
        userDAOImpl.deleteRoles(2L);
        userDAOImpl.delete(2L);
        assertEquals("Amount of all users after deleting an user is incorrect", 2, userDAOImpl.selectAll().size());
    }

    /**
     * Test method which gets all users from database
     *
     * @throws DAOException
     */
    @Test
    public void getAllUsers() throws DAOException {
        assertEquals("Amount of all users is incorrect", 3, userDAOImpl.selectAll().size());
    }

    /**
     * Test method which gets user roles by id of user
     *
     * @throws DAOException
     */
    @Test
    public void getUserRoles() throws DAOException {
        List<String> roles = userDAOImpl.getRoles(2L);
        assertEquals("Amount of roles is incorrect", 2, roles.size());
    }

    /**
     * Test method which adds roles to user in database
     *
     * @throws DAOException
     */
    @Test
    public void addUserRoles() throws DAOException {
        List<String> roles = new ArrayList<String>(1);
        roles.add("User");
        userDAOImpl.addRoles(2L, roles);
        List<String> newRoles = userDAOImpl.getRoles(2L);
        assertEquals("Amount of roles after adding new roles is incorrect", 3, newRoles.size());
    }
}

