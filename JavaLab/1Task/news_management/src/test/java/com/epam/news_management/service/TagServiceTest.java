package com.epam.news_management.service;

import com.epam.news_management.dao.ITagDAO;
import com.epam.news_management.entity.Tag;
import com.epam.news_management.exception.DAOException;
import com.epam.news_management.exception.ServiceException;
import com.epam.news_management.service.impl.TagServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Class for testing TagServiceImpl
 */
@RunWith(MockitoJUnitRunner.class)
public class TagServiceTest {
    @Mock
    private ITagDAO tagDAO;
    @Mock
    private Tag tag;

    private ITagService tagService;

    @Before
    public void createService() {
        tagService = new TagServiceImpl(tagDAO);
    }

    @Test
    public void deleteTag() throws ServiceException, DAOException {
        Long tagId = 1L;
        tagService.deleteTag(tagId);
        verify(tagDAO).delete(tagId);
        verify(tagDAO).deleteTagInNews(tagId);
    }

    @Test
    public void addTag() throws ServiceException, DAOException {
        tagService.addTag(tag);
        verify(tagDAO).add(tag);
    }

    @Test
    public void getAllTags() throws ServiceException, DAOException {
        List<Tag> tags = new ArrayList<Tag>();
        tags.add(tag);
        when(tagDAO.selectAll()).thenReturn(tags);
        List<Tag> foundTags = tagService.getAllTags();
        assertEquals("Size is incorrect", 1, foundTags.size());
    }

    @Test
    public void updateTag() throws DAOException, ServiceException {
        tagService.updateTag(tag);
        verify(tagDAO).update(tag);

    }

    @Test
    public void deleteTagInNews() throws ServiceException, DAOException {
        Long tagId = 1L;
        tagService.deleteTagsInNews(tagId);
        verify(tagDAO).deleteTagInNews(tagId);
    }
}
