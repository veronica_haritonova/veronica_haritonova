package com.epam.news_management.service;

import com.epam.news_management.dao.ICommentDAO;
import com.epam.news_management.entity.Comment;
import com.epam.news_management.exception.DAOException;
import com.epam.news_management.exception.ServiceException;
import com.epam.news_management.service.impl.CommentServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;

/**
 * Class for testing CommentServiceImpl
 */
@RunWith(MockitoJUnitRunner.class)
public class CommentServiceTest {
    @Mock
    private ICommentDAO commentDAO;

    private ICommentService commentService;

    @Mock
    private Comment comment;

    @Before
    public void createService() {
        commentService = new CommentServiceImpl(commentDAO);
    }

    /**
     * Test method which deletes comment
     *
     * @throws ServiceException
     * @throws DAOException
     */
    @Test
    public void deleteComment() throws ServiceException, DAOException {
        commentService.deleteComment(1L);
        verify(commentDAO).delete(1L);
    }

    /**
     * Test method which adds comment
     *
     * @throws ServiceException
     * @throws DAOException
     */
    @Test
    public void addComment() throws ServiceException, DAOException {
        commentService.addComment(comment);
        verify(commentDAO).add(comment);
    }
}
