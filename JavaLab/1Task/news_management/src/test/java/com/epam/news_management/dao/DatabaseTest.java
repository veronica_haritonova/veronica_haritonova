package com.epam.news_management.dao;

import com.epam.news_management.exception.DAOException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.junit.Assert.assertTrue;

/**
 * @author Veronica_Haritonova
 *         Class for testing connection with database
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:test-spring.xml")
public class DatabaseTest {

    @Autowired
    private ApplicationContext appContext;

    /**
     * Test connection with database
     *
     * @throws SQLException
     * @throws DAOException
     */
    @Test
    public void testConnection() throws SQLException, DAOException {
        DriverManagerDataSource dataSource = (DriverManagerDataSource) appContext.getBean("dataSource");
        Connection connection = dataSource.getConnection();
        PreparedStatement ps = connection.prepareStatement("SELECT * FROM tag WHERE tag_id = 1");
        ResultSet rs = ps.executeQuery();
        assertTrue("No rows", rs.next());
        assertTrue(rs.getInt("tag_id") == 1);
        rs.close();
        ps.close();
        connection.close();
    }
}
