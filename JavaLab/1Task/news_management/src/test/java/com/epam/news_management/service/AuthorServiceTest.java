package com.epam.news_management.service;

import com.epam.news_management.dao.IAuthorDAO;
import com.epam.news_management.entity.Author;
import com.epam.news_management.exception.DAOException;
import com.epam.news_management.exception.ServiceException;
import com.epam.news_management.service.impl.AuthorServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.context.ContextConfiguration;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

/**
 * Class for testing AuthorServiceImpl
 */
@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(locations = "classpath:test-spring.xml")
public class AuthorServiceTest {
    @Mock
    private IAuthorDAO authorDAO;

    private IAuthorService authorServiceImpl;

    private Author author;

    @Before
    public void createAuthorAndService() {
        authorServiceImpl = new AuthorServiceImpl(authorDAO);
        Long userId = 1L;
        author = new Author(userId, "Tom");
    }

    @Test
    public void addAuthor() throws DAOException, ServiceException {
        authorServiceImpl.addAuthor(author);
        verify(authorDAO).add(author);
    }

    @Test
    public void updateAuthor() throws ServiceException, DAOException {
        authorServiceImpl.updateAuthor(author);
        verify(authorDAO).update(author);
    }

    @Test
    public void deleteAuthor() throws DAOException, ServiceException {
        authorServiceImpl.deleteAuthor(1L);
        verify(authorDAO).setExpired(anyLong());
    }

    @Test
    public void getAuthors() throws DAOException, ServiceException {
        List<Author> authors = new ArrayList<Author>();
        authors.add(author);
        authors.add(author);
        when(authorDAO.selectAll()).thenReturn(authors);
        List<Author> foundAuthors = authorServiceImpl.getAllAuthors();
        assertEquals("Size is incorrect", 2, foundAuthors.size());
    }
}
