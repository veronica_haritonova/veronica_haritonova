package com.epam.news_management.service;

import com.epam.news_management.dao.INewsDAO;
import com.epam.news_management.dao.ISearchDAO;
import com.epam.news_management.entity.*;
import com.epam.news_management.exception.DAOException;
import com.epam.news_management.exception.ServiceException;
import com.epam.news_management.service.impl.NewsServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Class for testing NewsServiceImpl
 */
@RunWith(MockitoJUnitRunner.class)
public class NewsServiceTest {
    @Mock
    private INewsDAO newsDAO;
    @Mock
    private ISearchDAO searchDAO;

    private INewsService newsService;

    private News news;

    private Author author;

    private Long newsId = 1L;

    @Before
    public void createData() {
        newsService = new NewsServiceImpl(newsDAO, searchDAO);
        news = new News(newsId, "title", "short text", "full text");
        author = new Author();
    }

    @Test
    public void addFullNews() throws ServiceException, DAOException {
        when(newsDAO.add(news)).thenReturn(newsId);
        List<Long> tagsId = new ArrayList<Long>();
        Long authorId = 1L;
        newsService.addFullNews(news, authorId, tagsId);
        verify(newsDAO).addTags(news.getId(), tagsId);
        verify(newsDAO).addAuthor(news.getId(), authorId);
    }

    @Test
    public void deleteFullNews() throws ServiceException, DAOException {
        newsService.deleteFullNews(newsId);
        verify(newsDAO).delete(newsId);
        verify(newsDAO).deleteAllComments(newsId);
        verify(newsDAO).deleteAuthor(newsId);
        verify(newsDAO).deleteAllTags(newsId);
    }

    @Test
    public void updateFullNews() throws ServiceException, DAOException {
        when(newsDAO.countAuthors(anyLong())).thenReturn(0);
        List<Long> tagsId = new ArrayList<Long>();
        Long authorId = 1L;
        newsService.updateFullNews(news, authorId, tagsId);
        verify(newsDAO).update(news);
        verify(newsDAO).deleteAllTags(newsId);
        verify(newsDAO).addTags(newsId, tagsId);
        verify(newsDAO).addAuthor(newsId, authorId);

    }

    @Test
    public void findAllNews() throws ServiceException, DAOException {
        List<News> listNews = new ArrayList<News>(2);
        listNews.add(news);
        listNews.add(news);
        when(newsDAO.selectAll()).thenReturn(listNews);
        assertEquals("Amount of all news is incorrect", 2, newsService.findAll().size());
    }

    @Test
    public void countAllNews() throws ServiceException, DAOException {
        when(newsDAO.countAll()).thenReturn(2);
        assertEquals("Amount of all news is incorrect", 2, newsService.countAllNews());
    }

    @Test
    public void getFullNews() throws ServiceException, DAOException {
        when(newsDAO.get(anyLong())).thenReturn(news);
        when(newsDAO.getAuthor(anyLong())).thenReturn(author);
        when(newsDAO.getComments(anyLong())).thenReturn(new ArrayList<Comment>());
        when(newsDAO.getTags(anyLong())).thenReturn(new ArrayList<Tag>());
        FullNewsVO foundNews = newsService.getFullNews(newsId);
        assertNotNull("No found news", foundNews);
    }

    @Test
    public void addTags() throws ServiceException, DAOException {
        List<Long> tagsId = new ArrayList<Long>();
        newsService.addTags(newsId, tagsId);
        verify(newsDAO).addTags(newsId, tagsId);
    }

    @Test
    public void addAuthor() throws ServiceException, DAOException {
        Long authorId = 1L;
        newsService.addAuthor(newsId, authorId);
        verify(newsDAO).addAuthor(newsId, authorId);
    }

    @Test
    public void findByCriteria() throws ServiceException, DAOException {
        SearchCriteriaFullVO criteria = new SearchCriteriaFullVO();
        List<News> listNews = new ArrayList<News>(2);
        listNews.add(news);
        listNews.add(news);
        when(searchDAO.find(criteria)).thenReturn(listNews);
        assertEquals("Amount of found news is incorrect", listNews.size(), newsService.findByCriteria(criteria).size());
    }

    @Test
    public void findByCriteriaId() throws ServiceException, DAOException {
        SearchCriteriaVO criteria = new SearchCriteriaVO();
        List<News> listNews = new ArrayList<News>(2);
        listNews.add(news);
        listNews.add(news);
        when(searchDAO.find(criteria)).thenReturn(listNews);
        assertEquals("Amount of found news is incorrect", listNews.size(), newsService.findByCriteria(criteria).size());
    }

    @Test
    public void addNews() throws ServiceException, DAOException {
        newsService.addNews(news);
        verify(newsDAO).add(news);
    }

    @Test
    public void getTags() throws ServiceException, DAOException {
        when(newsDAO.getTags(anyLong())).thenReturn(new ArrayList<Tag>());
        assertNotNull(newsService.getTags(newsId));
    }

    @Test
    public void getComments() throws ServiceException, DAOException {
        when(newsDAO.getComments(anyLong())).thenReturn(new ArrayList<Comment>());
        assertTrue(newsService.getComments(newsId).isEmpty());
    }

    @Test
    public void getAuthor() throws ServiceException, DAOException {
        when(newsDAO.getAuthor(anyLong())).thenReturn(author);
        assertNotNull(newsService.getAuthor(newsId));
    }

    @Test
    public void getNews() throws ServiceException, DAOException {
        when(newsDAO.get(anyLong())).thenReturn(news);
        assertNotNull(newsService.getNews(newsId));
    }

    @Test
    public void updateNews() throws ServiceException, DAOException {
        newsService.updateNews(news);
        verify(newsDAO).update(news);
    }

    @Test
    public void deleteTags() throws ServiceException, DAOException {
        newsService.deleteTags(newsId);
        verify(newsDAO).deleteAllTags(newsId);
    }

    @Test
    public void updateAuthor() throws ServiceException, DAOException {
        Long authorId = 1L;
        newsService.updateAuthor(newsId, authorId);
        verify(newsDAO).updateAuthor(newsId, authorId);
    }

    @Test
    public void countAuthors() throws ServiceException, DAOException {
        when(newsDAO.countAuthors(anyLong())).thenReturn(2);
        assertEquals("Amount of authors is incorrect", 2, newsService.countAuthors(newsId));
    }

    @Test
    public void deleteAuthor() throws ServiceException, DAOException {
        newsService.deleteAuthor(newsId);
        verify(newsDAO).deleteAuthor(newsId);
    }

    @Test
    public void deleteComments() throws ServiceException, DAOException {
        newsService.deleteComments(newsId);
        verify(newsDAO).deleteAllComments(newsId);
    }

    @Test
    public void deleteNews() throws ServiceException, DAOException {
        newsService.deleteNews(newsId);
        verify(newsDAO).delete(newsId);
    }
}
