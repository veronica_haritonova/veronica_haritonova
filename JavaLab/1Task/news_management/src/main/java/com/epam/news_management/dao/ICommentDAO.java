package com.epam.news_management.dao;

import com.epam.news_management.entity.Comment;
import com.epam.news_management.exception.DAOException;

import java.util.List;

public interface ICommentDAO {
    /**
     * Insert new comment into database
     *
     * @param comment
     * @return id of inserted comment
     * @throws DAOException
     */
    Long add(Comment comment) throws DAOException;

    /**
     * Delete comment with given id from database
     *
     * @param commentId of comment to delete
     * @throws DAOException
     */
    void delete(Long commentId) throws DAOException;

    /**
     * Update comment in database
     *
     * @param comment to update
     * @throws DAOException
     */
    void update(Comment comment) throws DAOException;

    /**
     * Select comment by its id from database
     *
     * @param commentId
     * @return
     * @throws DAOException
     */
    Comment get(Long commentId) throws DAOException;

    /**
     * Get list of all comments
     *
     * @return list of all comments
     * @throws DAOException
     */
    List<Comment> selectAll() throws DAOException;
}
