package com.epam.news_management.service.impl;

import com.epam.news_management.dao.INewsDAO;
import com.epam.news_management.dao.ISearchDAO;
import com.epam.news_management.entity.*;
import com.epam.news_management.exception.DAOException;
import com.epam.news_management.exception.ServiceException;
import com.epam.news_management.service.INewsService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Veronica_Haritonova
 *         Service for work with news
 */
@Service
@Transactional(rollbackFor = ServiceException.class)
public class NewsServiceImpl implements INewsService {
    Logger logger = Logger.getLogger(NewsServiceImpl.class);
    @Autowired
    private INewsDAO newsDAO;
    @Autowired
    private ISearchDAO searchDAO;

    public NewsServiceImpl(INewsDAO newsDAO, ISearchDAO searchDAO) {
        this.newsDAO = newsDAO;
        this.searchDAO = searchDAO;
    }

    public NewsServiceImpl() {
    }

    /**
     * Add full news info
     *
     * @param news
     * @param authorId
     * @param tagsId
     * @throws ServiceException
     */
    public void addFullNews(News news, Long authorId, List<Long> tagsId) throws ServiceException {

        Long id = addNews(news);
        addAuthor(id, authorId);
        addTags(id, tagsId);


    }

    /**
     * Delete full news info
     *
     * @param newsId
     * @throws ServiceException
     */
    public void deleteFullNews(Long newsId) throws ServiceException {

        deleteComments(newsId);
        deleteTags(newsId);
        deleteAuthor(newsId);
        deleteNews(newsId);

    }

    /**
     * Update full news info
     *
     * @param news
     * @param authorId
     * @param tagsId
     * @throws ServiceException
     */
    public void updateFullNews(News news, Long authorId, List<Long> tagsId) throws ServiceException {

        Long id = news.getId();
        updateNews(news);
        deleteTags(id);
        addTags(id, tagsId);
        if (countAuthors(id) != 0) {
            updateAuthor(id, authorId);
        } else {
            addAuthor(id, authorId);
        }

    }

    /**
     * Find all news
     *
     * @return list of news
     * @throws ServiceException
     */
    public List<News> findAll() throws ServiceException {
        try {
            List<News> newsList = newsDAO.selectAll();
            return newsList;
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * Count all news
     *
     * @return amount of news
     * @throws ServiceException
     */
    public int countAllNews() throws ServiceException {
        try {
            int count = newsDAO.countAll();
            return count;
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * Get full news info
     *
     * @param newsId
     * @return fullNews
     * @throws ServiceException
     */
    public FullNewsVO getFullNews(Long newsId) throws ServiceException {
        FullNewsVO fullNewsVO = new FullNewsVO();
        News news = getNews(newsId);
        Author author = getAuthor(newsId);
        List<Tag> tags = getTags(newsId);
        List<Comment> comments = getComments(newsId);
        fullNewsVO.setAuthor(author);
        fullNewsVO.setTags(tags);
        fullNewsVO.setNews(news);
        fullNewsVO.setComments(comments);
        return fullNewsVO;
    }

    /**
     * Add tags to news with given id
     *
     * @param newsId
     * @param tagsId
     * @throws ServiceException
     */
    public void addTags(Long newsId, List<Long> tagsId) throws ServiceException {
        try {
            newsDAO.addTags(newsId, tagsId);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * Add author to news with given id
     *
     * @param newsId
     * @param authorId
     * @throws ServiceException
     */
    public void addAuthor(Long newsId, Long authorId) throws ServiceException {
        try {
            newsDAO.addAuthor(newsId, authorId);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * Find news by criteria, which contains full objects
     *
     * @param criteria
     * @return list of found news
     * @throws ServiceException
     */
    public List<News> findByCriteria(SearchCriteriaFullVO criteria) throws ServiceException {
        try {
            List<News> newsList = searchDAO.find(criteria);
            return newsList;
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * Add news
     *
     * @param news
     * @return id of added news
     * @throws ServiceException
     */
    public Long addNews(News news) throws ServiceException {
        try {
            Long id = newsDAO.add(news);
            return id;
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * Get tags of news with given id
     *
     * @param newsId
     * @return list of tags
     * @throws ServiceException
     */
    public List<Tag> getTags(Long newsId) throws ServiceException {
        try {
            List<Tag> tags = newsDAO.getTags(newsId);
            return tags;
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * Get comments of news with given id
     *
     * @param newsId
     * @return list of comments
     * @throws ServiceException
     */
    public List<Comment> getComments(Long newsId) throws ServiceException {
        try {
            List<Comment> comments = newsDAO.getComments(newsId);
            return comments;
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * Get author of news with given id
     *
     * @param newsId
     * @return author
     * @throws ServiceException
     */
    public Author getAuthor(Long newsId) throws ServiceException {
        try {
            Author author = newsDAO.getAuthor(newsId);
            return author;
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * Get news with given id
     *
     * @param newsId
     * @return news
     * @throws ServiceException
     */
    public News getNews(Long newsId) throws ServiceException {
        try {
            News news = newsDAO.get(newsId);
            return news;
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * Update news with given id
     *
     * @param news
     * @throws ServiceException
     */
    public void updateNews(News news) throws ServiceException {
        try {
            newsDAO.update(news);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * Delete all tags of news with given id
     *
     * @param newsId
     * @throws ServiceException
     */
    public void deleteTags(Long newsId) throws ServiceException {
        try {
            newsDAO.deleteAllTags(newsId);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * Update author of news with given id
     *
     * @param newsId
     * @param authorId
     * @throws ServiceException
     */
    public void updateAuthor(Long newsId, Long authorId) throws ServiceException {
        try {
            newsDAO.updateAuthor(newsId, authorId);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * Count authors of news with given id
     *
     * @param newsId
     * @return
     * @throws ServiceException
     */
    public int countAuthors(Long newsId) throws ServiceException {
        try {
            return newsDAO.countAuthors(newsId);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * Delete author of news with given id
     *
     * @param newsId
     * @throws ServiceException
     */
    public void deleteAuthor(Long newsId) throws ServiceException {
        try {
            newsDAO.deleteAuthor(newsId);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * Delete all comments of news with given id
     *
     * @param newsId
     * @throws ServiceException
     */
    public void deleteComments(Long newsId) throws ServiceException {
        try {
            newsDAO.deleteAllComments(newsId);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * Delete news with given id
     *
     * @param newsId
     * @throws ServiceException
     */
    public void deleteNews(Long newsId) throws ServiceException {
        try {
            newsDAO.delete(newsId);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * Find news by criteria, which contains id
     *
     * @param criteria
     * @return list of found news
     * @throws ServiceException
     */
    public List<News> findByCriteria(SearchCriteriaVO criteria) throws ServiceException {
        List<News> newsList = null;
        try {
            newsList = searchDAO.find(criteria);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
        return newsList;
    }
}
