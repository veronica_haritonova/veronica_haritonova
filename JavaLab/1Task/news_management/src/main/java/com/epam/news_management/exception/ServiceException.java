package com.epam.news_management.exception;

public class ServiceException extends Exception {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 6902934254405130989L;

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(String message, Throwable e) {
        super(message, e);
    }

    public ServiceException(Throwable e) {
        super(e);
    }
}
