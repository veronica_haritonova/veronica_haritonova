package com.epam.news_management.entity;

/**
 * @author Veronica_Haritonova
 * Class to store criteria with full objects for news search
 */

import java.io.Serializable;
import java.util.List;

public class SearchCriteriaFullVO implements Serializable {
    /**
     * Author for news search, can be null
     */
    private Author author;

    /**
     * List of tags for news search, can be null or empty
     */
    private List<Tag> tags;

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 8910710441740289068L;

    public SearchCriteriaFullVO() {
    }

    public SearchCriteriaFullVO(Author author, List<Tag> tags) {
        this.author = author;
        this.tags = tags;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((author == null) ? 0 : author.hashCode());
        result = prime * result + ((tags == null) ? 0 : tags.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof SearchCriteriaFullVO))
            return false;
        SearchCriteriaFullVO other = (SearchCriteriaFullVO) obj;
        if (author == null) {
            if (other.author != null)
                return false;
        } else if (!author.equals(other.author))
            return false;
        if (tags == null) {
            if (other.tags != null)
                return false;
        } else if (!tags.equals(other.tags))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "SearchCriteria [author=" + author + ", tags=" + tags + "]";
    }
}
