package com.epam.news_management.service.impl;

import com.epam.news_management.dao.ICommentDAO;
import com.epam.news_management.entity.Comment;
import com.epam.news_management.exception.DAOException;
import com.epam.news_management.exception.ServiceException;
import com.epam.news_management.service.ICommentService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Veronica_Haritonova
 *         Service for work with comments
 */
@Service
@Transactional(rollbackFor = ServiceException.class)
public class CommentServiceImpl implements ICommentService {
    Logger logger = Logger.getLogger(CommentServiceImpl.class);

    @Autowired
    private ICommentDAO commentDAO;

    public CommentServiceImpl(ICommentDAO commentDAO) {
        this.commentDAO = commentDAO;
    }

    public CommentServiceImpl() {
    }

    /**
     * Add comment
     *
     * @param comment
     * @throws ServiceException
     */
    public void addComment(Comment comment) throws ServiceException {
        try {
            commentDAO.add(comment);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * Delete comment with given id
     *
     * @param commentId
     * @throws ServiceException
     */
    public void deleteComment(Long commentId) throws ServiceException {
        try {
            commentDAO.delete(commentId);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }
}
