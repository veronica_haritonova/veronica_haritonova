package com.epam.news_management.service.impl;

import com.epam.news_management.dao.ITagDAO;
import com.epam.news_management.entity.Tag;
import com.epam.news_management.exception.DAOException;
import com.epam.news_management.exception.ServiceException;
import com.epam.news_management.service.ITagService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Veronica_Haritonova
 *         Service for work with tags
 */
@Service
@Transactional(rollbackFor = ServiceException.class)
public class TagServiceImpl implements ITagService {
    Logger logger = Logger.getLogger(TagServiceImpl.class);
    @Autowired
    private ITagDAO tagDAO;

    public TagServiceImpl(ITagDAO tagDAO) {
        this.tagDAO = tagDAO;
    }

    public TagServiceImpl() {
    }

    /**
     * Add given tag
     *
     * @param tag to add
     * @throws ServiceException
     */
    public void addTag(Tag tag) throws ServiceException {
        try {
            tagDAO.add(tag);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * Delete tag with given id
     *
     * @param tagId
     * @throws ServiceException
     */
    public void deleteTag(Long tagId) throws ServiceException {
        try {
            deleteTagsInNews(tagId);
            tagDAO.delete(tagId);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * Update given tag
     *
     * @param tag
     * @throws ServiceException
     */
    public void updateTag(Tag tag) throws ServiceException {
        try {
            tagDAO.update(tag);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * Get all tags
     *
     * @return list of tags
     * @throws ServiceException
     */
    public List<Tag> getAllTags() throws ServiceException {
        try {
            List<Tag> tags = tagDAO.selectAll();
            return tags;
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * Delete link between tag with given id and news
     *
     * @param tagId
     * @throws ServiceException
     */
    public void deleteTagsInNews(Long tagId) throws ServiceException {
        try {
            tagDAO.deleteTagInNews(tagId);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }
}
