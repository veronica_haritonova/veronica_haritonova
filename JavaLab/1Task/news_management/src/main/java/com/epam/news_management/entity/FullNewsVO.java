package com.epam.news_management.entity;

import java.io.Serializable;
import java.util.List;

/**
 * @author Veronica_Haritonova
 *         Class is used to store all full information about news
 */
public class FullNewsVO implements Serializable {

    /**
     * Contains basic information about news
     */
    private News news;
    /**
     * Author of news, can be null or empty
     */
    private Author author;
    /**
     * Tags of news, can be null or empty
     */
    private List<Tag> tags;
    /**
     * Comments of news, can be null or empty
     */
    private List<Comment> comments;

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -5262874554392630809L;


    public News getNews() {
        return news;
    }

    public void setNews(News news) {
        this.news = news;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((author == null) ? 0 : author.hashCode());
        result = prime * result
                + ((comments == null) ? 0 : comments.hashCode());
        result = prime * result + ((news == null) ? 0 : news.hashCode());
        result = prime * result + ((tags == null) ? 0 : tags.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof FullNewsVO))
            return false;
        FullNewsVO other = (FullNewsVO) obj;
        if (author == null) {
            if (other.author != null)
                return false;
        } else if (!author.equals(other.author))
            return false;
        if (comments == null) {
            if (other.comments != null)
                return false;
        } else if (!comments.equals(other.comments))
            return false;
        if (news == null) {
            if (other.news != null)
                return false;
        } else if (!news.equals(other.news))
            return false;
        if (tags == null) {
            if (other.tags != null)
                return false;
        } else if (!tags.equals(other.tags))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "FullNews [news=" + news + ", author=" + author + ", tags="
                + tags + ", comments=" + comments + "]";
    }

}
