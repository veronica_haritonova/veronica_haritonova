package com.epam.news_management.service;

import com.epam.news_management.entity.Tag;
import com.epam.news_management.exception.ServiceException;

import java.util.List;

/**
 * @author Veronica_Haritonova
 *         Service for work with tags
 */
public interface ITagService {
    /**
     * Add given tag
     *
     * @param tag to add
     * @throws ServiceException
     */
    void addTag(Tag tag) throws ServiceException;

    /**
     * Delete tag with given id
     *
     * @param tagId
     * @throws ServiceException
     */
    void deleteTag(Long tagId) throws ServiceException;

    /**
     * Update given tag
     *
     * @param tag
     * @throws ServiceException
     */
    void updateTag(Tag tag) throws ServiceException;

    /**
     * Get all tags
     *
     * @return list of tags
     * @throws ServiceException
     */
    List<Tag> getAllTags() throws ServiceException;

    /**
     * Delete link between tag with given id and news
     *
     * @param tagId
     * @throws ServiceException
     */
    void deleteTagsInNews(Long tagId) throws ServiceException;
}
