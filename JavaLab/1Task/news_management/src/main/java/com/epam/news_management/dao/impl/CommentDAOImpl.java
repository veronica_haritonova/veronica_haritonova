package com.epam.news_management.dao.impl;

import com.epam.news_management.dao.ICommentDAO;
import com.epam.news_management.entity.Comment;
import com.epam.news_management.exception.DAOException;
import com.epam.news_management.util.DBUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CommentDAOImpl implements ICommentDAO {
    /**
     * object for getting connections
     */
    @Autowired
    DriverManagerDataSource dataSource;

    private static final String SQL_INSERT_COMMENT = "INSERT INTO comments (comment_id, news_id,comment_text) VALUES (comments_seq.nextval, ?, ?)";
    private static final String SQL_DELETE_COMMENT = "DELETE FROM comments WHERE comment_id = ?";
    private static final String SQL_SELECT_ALL = "SELECT c.comment_id, c.news_id, c.comment_text,c.creation_date FROM comments c";
    private static final String SQL_UPDATE_COMMENT = "UPDATE comments SET news_id = ?, comment_text = ? WHERE comment_id = ?";
    private static final String SQL_SELECT_BY_ID = "SELECT c.comment_id, c.news_id, c.comment_text,c.creation_date FROM comments c WHERE c.comment_id = ?";

    /**
     * Insert new comment into database
     *
     * @param comment
     * @return id of inserted comment
     * @throws DAOException
     */
    public Long add(Comment comment) throws DAOException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            cn = DataSourceUtils.doGetConnection(dataSource);
            String[] gen_id = {"comment_id"};
            ps = cn.prepareStatement(SQL_INSERT_COMMENT, gen_id);
            ps.setLong(1, comment.getNewsId());
            ps.setString(2, comment.getText());
            ps.executeUpdate();
            rs = ps.getGeneratedKeys();
            rs.next();
            Long id = rs.getLong(1);
            return id;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(rs, ps, cn, dataSource);
        }
    }

    /**
     * Delete comment with given id from database
     *
     * @param commentId of comment to delete
     * @throws DAOException
     */
    public void delete(Long commentId) throws DAOException {
        Connection cn = null;
        PreparedStatement ps = null;
        try {
            cn = DataSourceUtils.doGetConnection(dataSource);
            ps = cn.prepareStatement(SQL_DELETE_COMMENT);
            ps.setLong(1, commentId);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(ps, cn, dataSource);
        }

    }

    /**
     * Update comment in database
     *
     * @param comment to update
     * @throws DAOException
     */
    public void update(Comment comment) throws DAOException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            cn = DataSourceUtils.doGetConnection(dataSource);
            ps = cn.prepareStatement(SQL_UPDATE_COMMENT);
            ps.setLong(1, comment.getNewsId());
            ps.setString(2, comment.getText());
            ps.setLong(3, comment.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(rs, ps, cn, dataSource);
        }

    }

    /**
     * Get list of all comments
     *
     * @return list of all comments
     * @throws DAOException
     */
    public Comment get(Long commentId) throws DAOException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            cn = DataSourceUtils.doGetConnection(dataSource);
            ps = cn.prepareStatement(SQL_SELECT_BY_ID);
            ps.setLong(1, commentId);
            rs = ps.executeQuery();
            Comment comment = null;
            if (rs.next()) {
                comment = new Comment(rs.getLong(1), rs.getLong(2), rs.getString(3), rs.getTimestamp(4));
            }
            return comment;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(rs, ps, cn, dataSource);
        }
    }

    /**
     * Get list of all comments
     *
     * @return list of all comments
     * @throws DAOException
     */
    public List<Comment> selectAll() throws DAOException {
        Connection cn = null;
        Statement st = null;
        ResultSet rs = null;
        try {
            cn = DataSourceUtils.doGetConnection(dataSource);
            st = cn.createStatement();
            rs = st.executeQuery(SQL_SELECT_ALL);
            List<Comment> comments = new ArrayList<Comment>();
            while (rs.next()) {
                comments.add(new Comment(rs.getLong(1), rs.getLong(2), rs.getString(3), rs.getTimestamp(4)));
            }
            return comments;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(rs, st, cn, dataSource);
        }
    }

    public DriverManagerDataSource getDataSource() {
        return dataSource;
    }

}
