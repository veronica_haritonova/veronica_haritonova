package com.epam.news_management.dao;

import com.epam.news_management.entity.Author;
import com.epam.news_management.exception.DAOException;

import java.util.List;

public interface IAuthorDAO {
    /**
     * Insert author into database
     *
     * @param author
     * @return id of inserted author
     * @throws DAOException
     */
    Long add(Author author) throws DAOException;

    /**
     * Update given author in database
     *
     * @param author
     * @throws DAOException
     */
    void update(Author author) throws DAOException;

    /**
     * Delete author with given id from database
     *
     * @param authorId
     * @throws DAOException
     */
    void delete(Long authorId) throws DAOException;

    /**
     * Select all authors in database
     *
     * @return List of all authors
     * @throws DAOException
     */
    List<Author> selectAll() throws DAOException;

    /**
     * Set date when author expired to author with given id
     *
     * @param authorId
     * @throws DAOException
     */
    void setExpired(Long authorId) throws DAOException;

    /**
     * Find author with given id in database
     *
     * @param id of author to find
     * @return
     * @throws DAOException
     */
    Author get(Long id) throws DAOException;

    /**
     * Delete links between author and news
     *
     * @param authorId
     * @throws DAOException
     */
    void deleteAuthorInNews(Long authorId) throws DAOException;
}
