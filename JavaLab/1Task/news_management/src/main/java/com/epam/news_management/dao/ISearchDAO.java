package com.epam.news_management.dao;

import com.epam.news_management.entity.News;
import com.epam.news_management.entity.SearchCriteriaFullVO;
import com.epam.news_management.entity.SearchCriteriaVO;
import com.epam.news_management.exception.DAOException;

import java.util.List;

public interface ISearchDAO {
    /**
     * Find news by given criteria, which includes tags and author
     *
     * @param criteria for news search
     * @return list of found news
     * @throws DAOException
     */
    List<News> find(SearchCriteriaFullVO criteria) throws DAOException;

    /**
     * Find news by given criteria, which includes id of tags and id of author
     *
     * @param criteriaVO
     * @return list of found news
     * @throws DAOException
     */
    List<News> find(SearchCriteriaVO criteriaVO) throws DAOException;
}
