package com.epam.news_management.service;

import com.epam.news_management.entity.*;
import com.epam.news_management.exception.ServiceException;

import java.util.List;

/**
 * @author Veronica_Haritonova
 *         Class to perform basic operations of news with its elements separately
 */
public interface INewsService {
    /**
     * Add full news info
     *
     * @param news
     * @param authorId
     * @param tagsId
     * @throws ServiceException
     */
    void addFullNews(News news, Long authorId, List<Long> tagsId) throws ServiceException;

    /**
     * Delete full news info
     *
     * @param newsId
     * @throws ServiceException
     */
    void deleteFullNews(Long newsId) throws ServiceException;

    /**
     * Update full news info
     *
     * @param news
     * @param authorId
     * @param tagsId
     * @throws ServiceException
     */
    void updateFullNews(News news, Long authorId, List<Long> tagsId) throws ServiceException;

    /**
     * Find all news
     *
     * @return list of news
     * @throws ServiceException
     */
    List<News> findAll() throws ServiceException;

    /**
     * Count all news
     *
     * @return amount of news
     * @throws ServiceException
     */
    int countAllNews() throws ServiceException;

    /**
     * Get full news info
     *
     * @param newsId
     * @return fullNews
     * @throws ServiceException
     */
    FullNewsVO getFullNews(Long newsId) throws ServiceException;

    /**
     * Add tags to news with given id
     *
     * @param newsId
     * @param tagsId
     * @throws ServiceException
     */
    void addTags(Long newsId, List<Long> tagsId) throws ServiceException;

    /**
     * Add author to news with given id
     *
     * @param newsId
     * @param authorId
     * @throws ServiceException
     */
    void addAuthor(Long newsId, Long authorId) throws ServiceException;

    /**
     * Find news by criteria
     *
     * @param criteria
     * @return list of found news
     * @throws ServiceException
     */
    List<News> findByCriteria(SearchCriteriaFullVO criteria) throws ServiceException;

    /**
     * Add news
     *
     * @param news
     * @return id of added news
     * @throws ServiceException
     */
    Long addNews(News news) throws ServiceException;

    /**
     * Get tags of news with given id
     *
     * @param newsId
     * @return list of tags
     * @throws ServiceException
     */
    List<Tag> getTags(Long newsId) throws ServiceException;

    /**
     * Get comments of news with given id
     *
     * @param newsId
     * @return list of comments
     * @throws ServiceException
     */
    List<Comment> getComments(Long newsId) throws ServiceException;

    /**
     * Get author of news with given id
     *
     * @param newsId
     * @return author
     * @throws ServiceException
     */
    Author getAuthor(Long newsId) throws ServiceException;

    /**
     * Get news with given id
     *
     * @param newsId
     * @return news
     * @throws ServiceException
     */
    News getNews(Long newsId) throws ServiceException;

    /**
     * Update news with given id
     *
     * @param news
     * @throws ServiceException
     */
    void updateNews(News news) throws ServiceException;

    /**
     * Delete all tags of news with given id
     *
     * @param newsId
     * @throws ServiceException
     */
    void deleteTags(Long newsId) throws ServiceException;

    /**
     * Update author of news with given id
     *
     * @param newsId
     * @param authorId
     * @throws ServiceException
     */
    void updateAuthor(Long newsId, Long authorId) throws ServiceException;

    /**
     * Count authors of news with given id
     *
     * @param newsId
     * @return
     * @throws ServiceException
     */
    int countAuthors(Long newsId) throws ServiceException;

    /**
     * Delete author of news with given id
     *
     * @param newsId
     * @throws ServiceException
     */
    void deleteAuthor(Long newsId) throws ServiceException;

    /**
     * Delete all comments of news with given id
     *
     * @param newsId
     * @throws ServiceException
     */
    void deleteComments(Long newsId) throws ServiceException;

    /**
     * Delete news with given id
     *
     * @param newsId
     * @throws ServiceException
     */
    void deleteNews(Long newsId) throws ServiceException;

    /**
     * Find news by criteria
     *
     * @param criteria
     * @return list of found news
     * @throws ServiceException
     */
    List<News> findByCriteria(SearchCriteriaVO criteria) throws ServiceException;

}
