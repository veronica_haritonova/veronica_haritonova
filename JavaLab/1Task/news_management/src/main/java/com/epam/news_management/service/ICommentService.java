package com.epam.news_management.service;

import com.epam.news_management.entity.Comment;
import com.epam.news_management.exception.ServiceException;

/**
 * @author Veronica_Haritonova
 *         Service for work with comments
 */
public interface ICommentService {
    /**
     * Add comment
     *
     * @param comment
     * @throws ServiceException
     */
    void addComment(Comment comment) throws ServiceException;

    /**
     * Delete comment with given id
     *
     * @param commentId
     * @throws ServiceException
     */
    void deleteComment(Long commentId) throws ServiceException;
}
