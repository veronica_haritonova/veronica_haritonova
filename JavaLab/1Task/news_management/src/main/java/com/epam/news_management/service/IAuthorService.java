package com.epam.news_management.service;

import com.epam.news_management.entity.Author;
import com.epam.news_management.exception.ServiceException;

import java.util.List;

public interface IAuthorService {
    /**
     * Add author
     *
     * @param author to add
     * @throws ServiceException
     */
    void addAuthor(Author author) throws ServiceException;

    /**
     * Delete author with given id
     *
     * @param authorId
     * @throws ServiceException
     */
    void deleteAuthor(Long authorId) throws ServiceException;

    /**
     * Update author
     *
     * @param author
     * @throws ServiceException
     */
    void updateAuthor(Author author) throws ServiceException;

    /**
     * Get all authors
     *
     * @return list of authors
     * @throws ServiceException
     */
    List<Author> getAllAuthors() throws ServiceException;
}
