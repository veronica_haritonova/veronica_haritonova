package com.epam.news_management.dao.impl;

import com.epam.news_management.dao.ISearchDAO;
import com.epam.news_management.entity.*;
import com.epam.news_management.exception.DAOException;
import com.epam.news_management.util.DBUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class SearchDAOImpl implements ISearchDAO {
    /**
     * object for getting connections
     */
    @Autowired
    DriverManagerDataSource dataSource;

    private static final String SQL_SELECT_NEWS = "SELECT n.news_id, n.title, n.short_text, "
            + "n.full_text, n.creation_date, n.modification_date FROM news n ";
    private static final String SQL_ORDER_BY = " ORDER BY (SELECT COUNT(*) FROM comments c WHERE c.news_id = n.news_id) DESC";
    private static final String SQL_SELECT_BY_TAGS_BEGIN = " n.news_id IN (";
    private static final String SQL_INTERSECT = " INTERSECT ";
    private static final String SQL_SELECT_BY_AUTHOR = " JOIN news_author na ON n.news_id = na.news_id WHERE na.author_id = ? ";
    private static final String SQL_WHERE = " WHERE ";
    private static final String SQL_AND = " AND ";
    private static final String SQL_SELECT_BY_TAGS = " (SELECT nt.news_id FROM news_tag nt WHERE nt.tag_id = ? ) ";
    private static final String SQL_CLOSED_BRACKET = ")";

    /**
     * Find news by given criteria
     *
     * @param criteria for news search
     * @return list of found news
     * @throws DAOException
     */
    public List<News> find(SearchCriteriaFullVO criteria) throws DAOException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            cn = DataSourceUtils.doGetConnection(dataSource);
            String query = buildQuery(criteria);
            ps = buildStatement(cn, query, criteria);
            rs = ps.executeQuery();
            List<News> news = new ArrayList<News>();
            while (rs.next()) {
                news.add(new News(rs.getLong(1), rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getTimestamp(5), rs.getDate(6)));
            }
            return news;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(rs, ps, cn, dataSource);
        }
    }

    /**
     * Find news by given criteria, which includes id of tags and id of author
     *
     * @param criteria
     * @return list of found news
     * @throws DAOException
     */
    public List<News> find(SearchCriteriaVO criteria) throws DAOException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            cn = DataSourceUtils.doGetConnection(dataSource);
            String query = buildQuery(criteria);
            ps = buildStatement(cn, query, criteria);
            rs = ps.executeQuery();
            List<News> news = new ArrayList<News>();
            while (rs.next()) {
                news.add(new News(rs.getLong(1), rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getTimestamp(5), rs.getDate(6)));
            }
            return news;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(rs, ps, cn, dataSource);
        }
    }

    /**
     * Add in query string search by author
     *
     * @param query
     */
    private void addAuthorConditionInQuery(StringBuffer query) {
        query.append(SQL_SELECT_BY_AUTHOR);
    }

    /**
     * Add in query string search by tags
     *
     * @param query
     * @param tagsAmount
     */
    private void addTagsConditionInQuery(StringBuffer query, int tagsAmount) {
        for(int i = 0; i < tagsAmount; i++) {
            if(i != 0) {
                query.append(SQL_INTERSECT);
            } else {
                query.append(SQL_SELECT_BY_TAGS_BEGIN);
            }
            query.append(SQL_SELECT_BY_TAGS);
        }
        query.append(SQL_CLOSED_BRACKET);
    }
    /**
     * Build string of query with given criteria with id
     *
     * @param criteria
     * @return string of query
     */
    private String buildQuery(SearchCriteriaVO criteria) {
        StringBuffer query = new StringBuffer(SQL_SELECT_NEWS);
        Long authorId = criteria.getAuthorId();
        List<Long> tagsId = criteria.getTagsId();
        boolean authorExist = (authorId != null);
        boolean tagsExist = (tagsId != null) ? !tagsId.isEmpty() : false;
        if (!authorExist && !tagsExist) {
            return query.toString();
        }
        if(authorExist) {
            addAuthorConditionInQuery(query);
        }
        if(tagsExist) {
            if(authorExist && tagsExist) {
                query.append(SQL_AND);
            } else {
                query.append(SQL_WHERE);
            }
            int tagsAmount = tagsId.size();
            addTagsConditionInQuery(query, tagsAmount);
        }
        query.append(SQL_ORDER_BY);
        return query.toString();
    }

    /**
     * Build string of query with given criteria with full objects
     *
     * @param criteria
     * @return string of query
     */
    private String buildQuery(SearchCriteriaFullVO criteria) {
        StringBuffer query = new StringBuffer(SQL_SELECT_NEWS);
        Author author = criteria.getAuthor();
        List<Tag> tags = criteria.getTags();
        boolean authorExist = (author != null);
        boolean tagsExist = (tags != null) ? !tags.isEmpty() : false;
        if (!authorExist && !tagsExist) {
            return query.toString();
        }
        if(authorExist) {
            addAuthorConditionInQuery(query);
        }
        if(tagsExist) {
            if(authorExist && tagsExist) {
                query.append(SQL_AND);
            } else {
                query.append(SQL_WHERE);
            }
            int tagsAmount = tags.size();
            addTagsConditionInQuery(query, tagsAmount);
        }
        query.append(SQL_ORDER_BY);
        return query.toString();
    }

    /**
     * Build preparedStatement with given query and search criteria with id
     *
     * @param cn
     * @param query
     * @param criteria
     * @return preparedStatement
     * @throws SQLException
     */
    private PreparedStatement buildStatement(Connection cn, String query, SearchCriteriaVO criteria) throws SQLException {
        Long authorId = criteria.getAuthorId();
        List<Long> tagsId = criteria.getTagsId();
        boolean authorExist = (authorId != null);
        boolean tagsExist = (tagsId != null) ? !tagsId.isEmpty() : false;
        PreparedStatement ps = cn.prepareStatement(query);
        if (!authorExist && !tagsExist) {
            return ps;
        }
        int index = 1;
        if (authorExist) {
            ps.setLong(index, authorId);
            index++;
        }
        if (tagsExist) {
            for (Long tagId : tagsId) {
                ps.setLong(index, tagId);
                index++;
            }
        }
        return ps;
    }

    /**
     * Build preparedStatement with given query and search criteria with full objects
     *
     * @param cn
     * @param query
     * @param criteria
     * @return preparedStatement
     * @throws SQLException
     */
    private PreparedStatement buildStatement(Connection cn, String query, SearchCriteriaFullVO criteria) throws SQLException {
        Author author = criteria.getAuthor();
        List<Tag> tags = criteria.getTags();
        boolean authorExist = (author != null);
        boolean tagsExist = (tags != null) ? !tags.isEmpty() : false;
        PreparedStatement ps = cn.prepareStatement(query);
        if (!authorExist && !tagsExist) {
            return ps;
        }
        int index = 1;
        if (authorExist) {
            ps.setLong(index, author.getId());
            index++;
        }
        if (tagsExist) {
            for (Tag tag : tags) {
                ps.setLong(index, tag.getId());
                index++;
            }
        }
        return ps;
    }
}
