INSERT INTO news (news_id, title, short_text, full_text) VALUES (news_seq.nextval, 'sterx', 'ccx','cvcvc');
INSERT INTO author (author_id, author_name) VALUES (author_seq.nextval, 'tom');
INSERT INTO tag (tag_id, tag_name) VALUES (tag_seq.nextval, 'sport');
INSERT INTO news_tag (news_id, tag_id) VALUES (1, 1);
INSERT INTO news_author (news_id, author_id) VALUES (1,1);
INSERT INTO news_author (news_id, author_id) VALUES (2,(SELECT author_id FROM author WHERE author_name = 'tom'));
INSERT INTO comments (comment_id, news_id, comment_text) VALUES (comments_seq.nextval, 1, 'some comment');
INSERT INTO users (user_id, user_name, login, password) VALUES (users_seq.nextval, 'veronica', 'veronica','1234');


UPDATE news SET title = 'win', short_text = 'short text', full_text = 'full text', modification_date = SYSDATE WHERE news_id = 1;

DELETE FROM news WHERE news_id = 3;

SELECT news.*, author_name FROM news LEFT JOIN news_author ON news.news_id = news_author.news_id 
	LEFT JOIN author ON news_author.author_id = author.author_id  ORDER BY (SELECT COUNT(*) FROM comments WHERE news_id = news.news_id) DESC;

SELECT news.*, author_name FROM news LEFT JOIN news_author ON news.news_id = news_author.news_id 
	LEFT JOIN author ON news_author.author_id = author.author_id WHERE news.news_id = 1;
SELECT * FROM comments WHERE news_id = 1;
SELECT tag.* FROM news_tag JOIN tag ON tag.tag_id = news_tag.tag_id
	JOIN news ON news_tag.news_id = news.news_id
 WHERE news.news_id = 1;

SELECT COUNT(*) FROM news;

DELETE FROM comments WHERE comment_id = 3;

SELECT news.*, author_name FROM news LEFT JOIN news_author ON news.news_id = news_author.news_id 
	LEFT JOIN author ON news_author.author_id = author.author_id 
	JOIN news_tag ON news_tag.news_id = news.news_id
	JOIN tag ON news_tag.tag_id = tag.tag_id WHERE author.author_id = 1 AND tag.tag_id = 1



SELECT * FROM news JOIN news_author ON news.news_id = news_author.news_id
WHERE news.news_id IN (
SELECT news_id FROM news_tag WHERE tag_id = 3
INTERSECT
SELECT news_id FROM news_tag WHERE tag_id = 2
) AND author_id = 3;
