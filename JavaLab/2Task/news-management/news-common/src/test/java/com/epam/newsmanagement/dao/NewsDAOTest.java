package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;
import org.dbunit.DBTestCase;
import org.dbunit.PropertiesBasedJdbcDatabaseTester;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.oracle.OracleDataTypeFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import java.io.FileInputStream;
import java.util.*;

/**
 * @author Veronica_Haritonova
 *         Class for testing operations with news
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:test-spring.xml")
@TestExecutionListeners({
        DependencyInjectionTestExecutionListener.class
}
)
public class NewsDAOTest extends DBTestCase {
    @Autowired
    private INewsDAO newsDAO;

    /**
     * Set configuration parameters for connection with database
     */
    public NewsDAOTest() {
        super();
        ResourceBundle bundle = ResourceBundle.getBundle("test-database", new Locale("en", "EN"));
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_DRIVER_CLASS, bundle.getString("db.driver"));
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_CONNECTION_URL, bundle.getString("db.path"));
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_USERNAME, bundle.getString("db.login"));
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_PASSWORD, bundle.getString("db.password"));
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_SCHEMA, bundle.getString("db.schema"));
    }

    /**
     * Get initial data for database
     */
    @Override
    public IDataSet getDataSet() throws Exception {
        return new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/dataset.xml"));
    }

    /**
     * Add special configuration property for including all oracle data types
     */
    @Override
    public void setUpDatabaseConfig(DatabaseConfig config) {
        config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY,
                new OracleDataTypeFactory());
    }

    /**
     * Put initial data to database before every test
     *
     * @throws Exception
     */
    @Before
    public void setDatabaseData() throws Exception {
        setUp();
    }

    /**
     * Test method which inserts news into database
     *
     * @throws DAOException
     */
    @Test
    public void addNews() throws DAOException {
        News news = new News("News1", "short text", "full text");
        news.setCreationDate(Calendar.getInstance().getTime());
        Long id = newsDAO.add(news);
        assertNotNull("No inserted news", newsDAO.get(id));
    }

    /**
     * Test method which updates news in database
     *
     * @throws DAOException
     */
    @Test
    public void updateNews() throws DAOException {
        String newTitle = "News1";
        Long id = 2L;
        News news = new News(id, newTitle, "short text", "full text");
        news.setModificationDate(Calendar.getInstance().getTime());
        newsDAO.update(news);
        assertEquals("Updates news title is incorrect", newTitle, newsDAO.get(id).getTitle());
    }

    /**
     * Test method which deletes news from database
     *
     * @throws DAOException
     */
    @Test
    public void deleteNews() throws DAOException {
        Long id = 2L;
        newsDAO.deleteAllComments(id);
        newsDAO.deleteAllTags(id);
        newsDAO.deleteAuthor(id);
        newsDAO.delete(id);
        assertEquals("Amount of all news after deleting is incorrect", 10, newsDAO.selectAll().size());
        assertTrue("Tags weren't deleted", newsDAO.getTags(id).isEmpty());
        assertTrue("Comments weren't deleted", newsDAO.getComments(id).isEmpty());
        assertNull("Author wasn't deleted", newsDAO.getAuthor(id));
    }

    /**
     * Test method which gets all news from database
     *
     * @throws DAOException
     */
    @Test
    public void selectAll() throws DAOException {
        assertEquals("Amount of all news is incorrect", 11, newsDAO.selectAll().size());
    }

    /**
     * Test method which counts all news in database
     *
     * @throws DAOException
     */
    @Test
    public void countAllNews() throws DAOException {
        assertEquals("Amount of all news is incorrect", 11, newsDAO.countAll());
    }

    /**
     * Test method which select news with given id from database
     *
     * @throws DAOException
     */
    @Test
    public void getNewsById() throws DAOException {
        News news = newsDAO.get(2L);
        assertNotNull("No given news", news);
        assertEquals("Title is incorrect", "News2", news.getTitle());
    }

    /**
     * Test method which adds tags to news with given id in database
     *
     * @throws DAOException
     */
    @Test
    public void addTags() throws DAOException {
        List<Long> tagsId = new ArrayList<Long>(1);
        tagsId.add(1L);
        Long id = 5L;
        newsDAO.addTags(id, tagsId);
        assertEquals("Amount of tags is incorrect", 1, newsDAO.getTags(id).size());
    }

    /**
     * Test method which add author of news with given id in database
     *
     * @throws DAOException
     */
    @Test
    public void addNewsAuthor() throws DAOException {
        Long authorId = 2L;
        Long id = 5L;
        newsDAO.addAuthor(id, authorId);
        assertEquals("Author is incorrect", authorId, newsDAO.getAuthor(id).getId());
    }

    /**
     * Test method which gets author of news with given id
     *
     * @throws DAOException
     */
    @Test
    public void getNewsAuthor() throws DAOException {
        Long id = 2L;
        Long authorId = 2L;
        assertEquals("Author is incorrect", authorId, newsDAO.getAuthor(id).getId());
    }

    /**
     * Test method which deletes author of news with given id
     *
     * @throws DAOException
     */
    @Test
    public void deleteAuthor() throws DAOException {
        Long id = 2L;
        newsDAO.deleteAuthor(id);
        assertNull("Author wasn't deleted", newsDAO.getAuthor(id));
    }

    /**
     * Test method which updates author of news with given id
     *
     * @throws DAOException
     */
    @Test
    public void updateAuthor() throws DAOException {
        Long authorId = 2L;
        Long id = 2L;
        newsDAO.updateAuthor(id, authorId);
        assertEquals("Updated author is incorrect", authorId, newsDAO.getAuthor(id).getId());
    }

    /**
     * Test method which counts authors of news with given id
     *
     * @throws DAOException
     */
    @Test
    public void countAuthors() throws DAOException {
        assertEquals("Amount of authors is incorrect", 0, newsDAO.countAuthors(5L));
    }

    /**
     * Test method which deletes given tag of news with given id
     *
     * @throws DAOException
     */
    @Test
    public void deleteTag() throws DAOException {
        Long id = 2L;
        Tag tag = new Tag(1L, "sport");
        newsDAO.deleteTag(id, tag.getId());
        assertTrue("Tag wasn't deleted", !newsDAO.getTags(id).contains(tag));
    }

    /**
     * Test method which deletes all tags of news with given id
     *
     * @throws DAOException
     */
    @Test
    public void deleteAllTags() throws DAOException {
        Long id = 2L;
        newsDAO.deleteAllTags(id);
        assertTrue("Tag weren't deleted", newsDAO.getTags(id).isEmpty());
    }

    /**
     * Test method which gets all tags of news with given id
     *
     * @throws DAOException
     */
    @Test
    public void getNewsTags() throws DAOException {
        assertFalse("No tags in news", newsDAO.getTags(2L).isEmpty());
    }

    /**
     * Test method which gets all comments of news with given id
     *
     * @throws DAOException
     */
    @Test
    public void getComments() throws DAOException {
        assertFalse("No comments of news", newsDAO.getComments(2L).isEmpty());
    }

    /**
     * Test method which deletes all tags of news with given id
     *
     * @throws DAOException
     */
    @Test
    public void deleteAllComments() throws DAOException {
        Long id = 2L;
        newsDAO.deleteAllComments(id);
        assertTrue("Tag weren't deleted", newsDAO.getComments(id).isEmpty());
    }

    /**
     * Test method which gets list of news with defined amount starting from defined number
     * @throws DAOException
     */
    @Test
    public void getDefinedAmountOfNews() throws DAOException {
        int start = 1;
        int amount = 5;
        assertEquals("Amount of news is incorrect",amount, newsDAO.getNewsList(start,amount).size());
    }
    /**
     * Test method which gets list tags' names of defined news
     * @throws DAOException
     */
    @Test
    public void getTagsNames() throws DAOException {
        Long newsId = 9L;
        int expectedAmount = 2;
        assertEquals("Amount of tags is incorrect",expectedAmount, newsDAO.getTagsNames(newsId).size());
    }
    /**
     * Test method which gets author of news with given id
     *
     * @throws DAOException
     */
    @Test
    public void getNewsAuthorName() throws DAOException {
        Long id = 2L;
        String authorName = "Tom";
        assertEquals("Author is incorrect", authorName, newsDAO.getAuthorName(id));
    }
    /**
     * Test method which gets list tags' id of defined news
     * @throws DAOException
     */
    @Test
    public void getTagsId() throws DAOException {
        Long newsId = 9L;
        int expectedAmount = 2;
        assertEquals("Amount of tags is incorrect",expectedAmount, newsDAO.getTagsId(newsId).size());
    }
    /**
     * Test method which gets authorId of news with given id
     *
     * @throws DAOException
     */
    @Test
    public void getNewsAuthorId() throws DAOException {
        Long id = 2L;
        Long authorId = 2L;
        assertEquals("Author is incorrect", authorId, newsDAO.getAuthorId(id));
    }
}
