package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.entity.*;
import com.epam.newsmanagement.exception.DAOException;
import org.dbunit.DBTestCase;
import org.dbunit.PropertiesBasedJdbcDatabaseTester;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.oracle.OracleDataTypeFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * @author Veronica_Haritonova
 *         Class for testing SearchDAOImpl
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:test-spring.xml")
@TestExecutionListeners({
        DependencyInjectionTestExecutionListener.class
}
)
public class SearchDAOTest extends DBTestCase {
    @Autowired
    private ISearchDAO searchDAO;

    /**
     * Set configuration parameters for connection with database
     */
    public SearchDAOTest() {
        super();
        ResourceBundle bundle = ResourceBundle.getBundle("test-database", new Locale("en", "EN"));
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_DRIVER_CLASS, bundle.getString("db.driver"));
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_CONNECTION_URL, bundle.getString("db.path"));
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_USERNAME, bundle.getString("db.login"));
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_PASSWORD, bundle.getString("db.password"));
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_SCHEMA, bundle.getString("db.schema"));
    }

    /**
     * Get initial data for database
     */
    @Override
    public IDataSet getDataSet() throws Exception {
        return new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/dataset.xml"));
    }

    /**
     * Add special configuration property for including all oracle data types
     */
    protected void setUpDatabaseConfig(DatabaseConfig config) {
        config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY,
                new OracleDataTypeFactory());
    }

    /**
     * Put initial data to database before every test
     *
     * @throws Exception
     */
    @Before
    public void setDatabaseData() throws Exception {
        setUp();
    }



    /**
     * Test search of news when search criteria contains tagsId and authorId
     *
     * @throws DAOException
     */
    @Test
    public void paginationSearchByFullCriteriaId() throws DAOException {
        List<Long> tagsId = new ArrayList<Long>(2);
        tagsId.add(3L);
        tagsId.add(2L);
        Long authorId = 3L;
        SearchCriteriaVO criteria = new SearchCriteriaVO();
        criteria.setAuthorId(authorId);
        criteria.setTagsId(tagsId);
        int expectedAmount = 1;
        List<NewsWorkItem> foundNews = searchDAO.find(criteria,1,5);
        assertEquals("Size of found news is incorrect", expectedAmount, foundNews.size());
    }

    /**
     * Test search of news when search criteria contains only authorId
     *
     * @throws DAOException
     */
    @Test
    public void paginationSearchByAuthorId() throws DAOException {
        Long authorId = 3L;
        SearchCriteriaVO criteria = new SearchCriteriaVO();
        criteria.setAuthorId(authorId);
        int expectedAmount = 2;
        List<NewsWorkItem> foundNews = searchDAO.find(criteria,1,5);
        assertEquals("Size of found news by author is incorrect", expectedAmount, foundNews.size());
    }

    /**
     * Test search of news when search criteria contains only tagsId
     *
     * @throws DAOException
     */
    @Test
    public void paginationSearchByTagsId() throws DAOException {
        Long tagId = 3L;
        List<Long> tagsId = new ArrayList<Long>(1);
        tagsId.add(tagId);
        SearchCriteriaVO criteria = new SearchCriteriaVO();
        criteria.setTagsId(tagsId);
        int expectedAmount = 1;
        List<NewsWorkItem> foundNews = searchDAO.find(criteria,1, 5);
        assertEquals("Size of found news by tags is incorrect", expectedAmount, foundNews.size());
    }

    /**
     * Test search of news when search criteria is empty
     *
     * @throws DAOException
     */
    @Test
    public void paginationSearchByEmptyCriteriaId() throws DAOException {
        SearchCriteriaVO criteria = new SearchCriteriaVO();
        int expectedAmount = 5;
        List<NewsWorkItem> foundNews = searchDAO.find(criteria,1,expectedAmount);
        assertEquals("Size of found news is incorrect", expectedAmount, foundNews.size());
    }

    /**
     * Test method which counts news when search criteria has only author
     *
     * @throws DAOException
     */
    @Test
    public void countFoundNewsByAuthorId() throws DAOException {
        Long authorId = 3L;
        SearchCriteriaVO criteria = new SearchCriteriaVO();
        criteria.setAuthorId(authorId);
        int expectedAmount = 2;
        int actualAmount = searchDAO.countFoundNews(criteria);
        assertEquals("Size of found news by author is incorrect", expectedAmount, actualAmount);
    }
    /**
     * Test method which counts news when search criteria contains tagsId and authorId
     *
     * @throws DAOException
     */
    @Test
    public void countFoundNewsByFullCriteriaId() throws DAOException {
        List<Long> tagsId = new ArrayList<Long>(2);
        tagsId.add(3L);
        tagsId.add(2L);
        Long authorId = 3L;
        SearchCriteriaVO criteria = new SearchCriteriaVO();
        criteria.setAuthorId(authorId);
        criteria.setTagsId(tagsId);
        int expectedAmount = 1;
        int actualAmount = searchDAO.countFoundNews(criteria);
        assertEquals("Size of found news is incorrect", expectedAmount, actualAmount);
    }

    /**
     * Test method which counts news when search criteria contains only tagsId
     *
     * @throws DAOException
     */
    @Test
    public void countFoundNewsByTagsId() throws DAOException {
        Long tagId = 3L;
        List<Long> tagsId = new ArrayList<Long>(1);
        tagsId.add(tagId);
        SearchCriteriaVO criteria = new SearchCriteriaVO();
        criteria.setTagsId(tagsId);
        int expectedAmount = 1;
        int actualAmount = searchDAO.countFoundNews(criteria);
        assertEquals("Size of found news by tags is incorrect", expectedAmount, actualAmount);
    }

    /**
     * Test method which counts news when search criteria is empty
     *
     * @throws DAOException
     */
    @Test
    public void countFoundNewsByEmptyCriteriaId() throws DAOException {
        SearchCriteriaVO criteria = new SearchCriteriaVO();
        int expectedAmount = 11;
        int actualAmount = searchDAO.countFoundNews(criteria);
        assertEquals("Size of found news is incorrect", expectedAmount, actualAmount);
    }
    /**
     * Test search of news when search criteria contains tags and author
     *
     * @throws DAOException
     */
    @Test
    public void searchIdByFullCriteria() throws DAOException {
        List<Long> tagsId = new ArrayList<Long>(2);
        tagsId.add(3L);
        tagsId.add(2L);
        Long authorId = 3L;
        SearchCriteriaVO criteria = new SearchCriteriaVO();
        criteria.setAuthorId(authorId);
        criteria.setTagsId(tagsId);
        int expectedAmount = 1;
        int actualAmount = searchDAO.findIdList(criteria,1,10).size();
        assertEquals("Size of found news is incorrect", expectedAmount, actualAmount);
    }
}
