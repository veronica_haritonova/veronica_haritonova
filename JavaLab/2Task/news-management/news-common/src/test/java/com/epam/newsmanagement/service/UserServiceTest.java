package com.epam.newsmanagement.service;

import com.epam.newsmanagement.dao.IUserDAO;
import com.epam.newsmanagement.entity.User;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.impl.UserServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Class for testing UserServiceImpl
 */
@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {
    @Mock
    private IUserDAO userDAO;
    @Mock
    private User user;

    private IUserService userService;

    @Before
    public void createService() {
        userService = new UserServiceImpl(userDAO);
    }

    @Test
    public void addFullUser() throws DAOException, ServiceException {
        List<String> roles = new ArrayList<String>();
        roles.add("Admin");
        Long userId = 2L;
        User newUser = new User(userId, "Tom", "Tom", "1");
        newUser.setRoles(roles);
        when(userDAO.add(newUser)).thenReturn(userId);
        userService.addFullUser(newUser);
        verify(userDAO).addRoles(newUser.getId(), newUser.getRoles());
    }

    @Test
    public void getRoles() throws DAOException, ServiceException {
        List<String> expectedRoles = new ArrayList<String>();
        expectedRoles.add("Admin");
        when(userDAO.getRoles(anyLong())).thenReturn(expectedRoles);
        Long userId = 2L;
        List<String> actualRoles = userService.getUserRoles(userId);
        assertEquals("Amount of roles is incorrect", expectedRoles.size(), actualRoles.size());
    }

    @Test
    public void addRoles() throws DAOException, ServiceException {
        Long userId = 1L;
        List<String> roles = new ArrayList<String>();
        userService.addUserRoles(userId, roles);
        verify(userDAO).addRoles(userId, roles);
    }

    @Test
    public void addUser() throws DAOException, ServiceException {
        Long userId = 2L;
        User newUser = new User(userId, "Tom", "Tom", "1");
        when(userDAO.add(newUser)).thenReturn(userId);
        assertEquals(newUser.getId(), userService.addUser(newUser));
    }
}
