package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.dao.impl.CommentDAOImpl;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DAOException;
import org.dbunit.DBTestCase;
import org.dbunit.PropertiesBasedJdbcDatabaseTester;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.oracle.OracleDataTypeFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import java.io.FileInputStream;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * @author Veronica_Haritonova
 *         Class for testing basic operations with comments in database
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:test-spring.xml")
@TestExecutionListeners({
        DependencyInjectionTestExecutionListener.class
}
)
public class CommentDAOTest extends DBTestCase {
    @Autowired
    CommentDAOImpl commentDAOImpl;

    /**
     * Set configuration parameters for connection with database
     */
    public CommentDAOTest() {
        super();
        ResourceBundle bundle = ResourceBundle.getBundle("test-database", new Locale("en", "EN"));
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_DRIVER_CLASS, bundle.getString("db.driver"));
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_CONNECTION_URL, bundle.getString("db.path"));
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_USERNAME, bundle.getString("db.login"));
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_PASSWORD, bundle.getString("db.password"));
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_SCHEMA, bundle.getString("db.schema"));
    }

    /**
     * Get initial data for database
     */
    @Override
    protected IDataSet getDataSet() throws Exception {
        return new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/dataset.xml"));
    }

    /**
     * Add special configuration property for including all oracle data types
     */
    protected void setUpDatabaseConfig(DatabaseConfig config) {
        config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY,
                new OracleDataTypeFactory());
    }

    /**
     * Put initial data to database before every test
     *
     * @throws Exception
     */
    @Before
    public void setDatabaseData() throws Exception {
        setUp();
    }

    /**
     * Test method which inserts comment into database
     *
     * @throws DAOException
     */
    @Test
    public void addComment() throws DAOException {
        assertNotNull(commentDAOImpl);
        assertNotNull(commentDAOImpl.getDataSource());
        commentDAOImpl.add(new Comment("some comment", 2L));
        assertEquals("Amount of all comments after adding new comment is incorrect", 7, commentDAOImpl.selectAll().size());
    }

    /**
     * Test method which deletes comment from database
     *
     * @throws DAOException
     */
    @Test
    public void deleteComment() throws DAOException {
        commentDAOImpl.delete(1L);
        assertEquals("Amount of all comments after deleting existing comment is incorrect", 5, commentDAOImpl.selectAll().size());
    }

    /**
     * Test method which get all comments from database
     *
     * @throws DAOException
     */
    @Test
    public void getComments() throws DAOException {
        assertEquals("Amount of all comments comment is incorrect", 6, commentDAOImpl.selectAll().size());
    }

    /**
     * Test method which updates comment in database
     *
     * @throws DAOException
     */
    @Test
    public void updateComment() throws DAOException {
        Comment comment = new Comment(1L, 2L, "text");
        commentDAOImpl.update(comment);
        assertEquals("NewsId of updated comment is incorrect", comment.getNewsId(), commentDAOImpl.get(comment.getId()).getNewsId());
    }

    /**
     * Test method which select comment by id in database
     *
     * @throws DAOException
     */
    @Test
    public void getComment() throws DAOException {
        Long newsId = 4L;
        Long commentId = 1L;
        assertEquals("NewsId of comment is incorrect", newsId, commentDAOImpl.get(commentId).getNewsId());
    }
}
