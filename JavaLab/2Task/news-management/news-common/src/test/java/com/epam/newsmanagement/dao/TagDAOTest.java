package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.dao.impl.NewsDAOImpl;
import com.epam.newsmanagement.dao.impl.TagDAOImpl;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;
import org.dbunit.DBTestCase;
import org.dbunit.PropertiesBasedJdbcDatabaseTester;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.oracle.OracleDataTypeFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import java.io.FileInputStream;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * @author Veronica_Haritonova
 *         Class for testing basic operations with tags in database
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:test-spring.xml")
@TestExecutionListeners({
        DependencyInjectionTestExecutionListener.class
}
)
public class TagDAOTest extends DBTestCase {

    @Autowired
    private TagDAOImpl tagDAOImpl;
    @Autowired
    private NewsDAOImpl newsDAOImpl;

    /**
     * Set configuration parameters for connection with database
     */
    public TagDAOTest() {
        super();
        ResourceBundle bundle = ResourceBundle.getBundle("test-database", new Locale("en", "EN"));
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_DRIVER_CLASS, bundle.getString("db.driver"));
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_CONNECTION_URL, bundle.getString("db.path"));
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_USERNAME, bundle.getString("db.login"));
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_PASSWORD, bundle.getString("db.password"));
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_SCHEMA, bundle.getString("db.schema"));
    }

    /**
     * Get initial data for database
     */
    @Override
    public IDataSet getDataSet() throws Exception {
        return new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/dataset.xml"));
    }

    /**
     * Add special configuration property for including all oracle data types
     */
    protected void setUpDatabaseConfig(DatabaseConfig config) {
        config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY,
                new OracleDataTypeFactory());
    }

    /**
     * Put initial data to database before every test
     *
     * @throws Exception
     */
    @Before
    public void setDatabaseData() throws Exception {
        setUp();
    }

    /**
     * Test the select method by comparing amount of all tags in database with expected amount
     *
     * @throws DAOException
     */
    @Test
    public void selectAllTags() throws DAOException {
        assertNotNull(tagDAOImpl);
        assertNotNull(tagDAOImpl.getDataSource());
        assertEquals("Amount of tags is incorrect", 5, tagDAOImpl.selectAll().size());
    }

    /**
     * Test method which inserts tag into database
     *
     * @throws DAOException
     */
    @Test
    public void addTag() throws DAOException {
        assertNotNull(tagDAOImpl);
        assertNotNull(tagDAOImpl.getDataSource());
        tagDAOImpl.add(new Tag("nature"));
        assertEquals("Amount of new tags is incorrect", 6, tagDAOImpl.selectAll().size());
    }

    /**
     * Test method which deletes tag with given id from database
     *
     * @throws DAOException
     */
    @Test
    public void deleteTag() throws DAOException {
        assertNotNull(tagDAOImpl);
        assertNotNull(tagDAOImpl.getDataSource());
        tagDAOImpl.deleteTagInNews(1L);
        tagDAOImpl.delete(1L);
        assertEquals("Amount of updated tags is incorrect", 4, tagDAOImpl.selectAll().size());
    }

    /**
     * Test method which updates given tag
     *
     * @throws DAOException
     */
    @Test
    public void updateTag() throws DAOException {
        tagDAOImpl.update(new Tag(1L, "nature"));
        assertEquals("Not updated value", "nature", tagDAOImpl.get(1L).getTagName());
    }

    /**
     * Test method which finds tag with given id
     *
     * @throws DAOException
     */
    @Test
    public void findTag() throws DAOException {
        assertEquals("Not updated value", "sport", tagDAOImpl.get(1L).getTagName());
    }

    /**
     * Test method which deletes tag in all news which has this tag
     *
     * @throws DAOException
     */
    @Test
    public void deleteTagInNews() throws DAOException {
        tagDAOImpl.deleteTagInNews(1L);
        assertEquals("Amount of updated tags is incorrect", 0, newsDAOImpl.getTags(2L).size());
    }
}

