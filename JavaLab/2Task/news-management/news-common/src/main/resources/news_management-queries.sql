INSERT INTO news (news_id, title, short_text, full_text) VALUES (news_seq.nextval, 'sterx', 'ccx','cvcvc');
INSERT INTO author (author_id, author_name) VALUES (author_seq.nextval, 'tom');
INSERT INTO tag (tag_id, tag_name) VALUES (tag_seq.nextval, 'sport');
INSERT INTO news_tag (news_id, tag_id) VALUES (1, 1);
INSERT INTO news_author (news_id, author_id) VALUES (1,1);
INSERT INTO news_author (news_id, author_id) VALUES (2,(SELECT author_id FROM author WHERE author_name = 'tom'));
INSERT INTO comments (comment_id, news_id, comment_text) VALUES (comments_seq.nextval, 1, 'some comment');
INSERT INTO users (user_id, user_name, login, password) VALUES (users_seq.nextval, 'admin', 'admin','202cb962ac59075b964b07152d234b70');


UPDATE news SET title = 'win', short_text = 'short text', full_text = 'full text', modification_date = SYSDATE WHERE news_id = 1;

DELETE FROM news WHERE news_id = 3;

SELECT news.*, author_name FROM news LEFT JOIN news_author ON news.news_id = news_author.news_id 
	LEFT JOIN author ON news_author.author_id = author.author_id  ORDER BY (SELECT COUNT(*) FROM comments WHERE news_id = news.news_id) DESC;

SELECT news.*, author_name FROM news LEFT JOIN news_author ON news.news_id = news_author.news_id 
	LEFT JOIN author ON news_author.author_id = author.author_id WHERE news.news_id = 1;
SELECT * FROM comments WHERE news_id = 1;
SELECT tag.* FROM news_tag JOIN tag ON tag.tag_id = news_tag.tag_id
	JOIN news ON news_tag.news_id = news.news_id
 WHERE news.news_id = 1;

SELECT COUNT(*) FROM news;

DELETE FROM comments WHERE comment_id = 3;

SELECT news.*, author_name FROM news LEFT JOIN news_author ON news.news_id = news_author.news_id 
	LEFT JOIN author ON news_author.author_id = author.author_id 
	JOIN news_tag ON news_tag.news_id = news.news_id
	JOIN tag ON news_tag.tag_id = tag.tag_id WHERE author.author_id = 1 AND tag.tag_id = 1



SELECT * FROM news JOIN news_author ON news.news_id = news_author.news_id
WHERE news.news_id IN (
SELECT news_id FROM news_tag WHERE tag_id = 3
INTERSECT
SELECT news_id FROM news_tag WHERE tag_id = 2
) AND author_id = 3;


 SELECT *
 FROM (SELECT n.news_id, n.title, n.short_text, n.creation_date,
    a.author_name, (SELECT COUNT(comment_id) FROM comments c WHERE c.news_id = n.news_id) as comments_amount
              ,n.modification_date, ROWNUM rnum
          FROM news n LEFT JOIN news_author na ON n.news_id = na.news_id
          LEFT JOIN AUTHOR a ON a.AUTHOR_ID = na.AUTHOR_ID
          ORDER BY n.modification_date DESC, comments_amount DESC)
 WHERE rnum BETWEEN 1 AND 5;