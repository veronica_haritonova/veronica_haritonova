package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.ICommentDAO;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.ICommentService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Veronica_Haritonova
 *         Service for work with comments
 */
@Service
@Transactional(rollbackFor = ServiceException.class)
public class CommentServiceImpl implements ICommentService {
    Logger logger = Logger.getLogger(CommentServiceImpl.class);

    @Autowired
    private ICommentDAO commentDAO;

    public CommentServiceImpl(ICommentDAO commentDAO) {
        this.commentDAO = commentDAO;
    }

    public CommentServiceImpl() {
    }

    /**
     * Add comment
     *
     * @param comment
     * @throws ServiceException
     */
    public void addComment(Comment comment) throws ServiceException {
        try {
            commentDAO.add(comment);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * Delete comment with given id
     *
     * @param commentId
     * @throws ServiceException
     */
    public void deleteComment(Long commentId) throws ServiceException {
        try {
            commentDAO.delete(commentId);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }
}
