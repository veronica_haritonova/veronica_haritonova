package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.INewsDAO;
import com.epam.newsmanagement.dao.ISearchDAO;
import com.epam.newsmanagement.entity.*;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.INewsService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Veronica_Haritonova
 *         Service for work with news
 */
@Service
@Transactional(rollbackFor = ServiceException.class)
public class NewsServiceImpl implements INewsService {
    Logger logger = Logger.getLogger(NewsServiceImpl.class);
    @Autowired
    private INewsDAO newsDAO;
    @Autowired
    private ISearchDAO searchDAO;

    public NewsServiceImpl(INewsDAO newsDAO, ISearchDAO searchDAO) {
        this.newsDAO = newsDAO;
        this.searchDAO = searchDAO;
    }

    public NewsServiceImpl() {
    }

    /**
     * Add full news info
     *
     * @param news
     * @param authorId
     * @param tagsId
     * @return id of news
     * @throws ServiceException
     */
    public Long addFullNews(News news, Long authorId, List<Long> tagsId) throws ServiceException {

        Long id = addNews(news);
        addAuthor(id, authorId);
        if(tagsId != null) {
            addTags(id, tagsId);
        }
        return id;

    }

    /**
     * Delete full news info
     *
     * @param newsId
     * @throws ServiceException
     */
    public void deleteFullNews(Long newsId) throws ServiceException {

        deleteComments(newsId);
        deleteTags(newsId);
        deleteAuthor(newsId);
        deleteNews(newsId);
    }

    /**
     * Update full news info
     *
     * @param news
     * @param authorId
     * @param tagsId
     * @throws ServiceException
     */
    public void updateFullNews(News news, Long authorId, List<Long> tagsId) throws ServiceException {

        Long id = news.getId();
        updateNews(news);
        deleteTags(id);
        if(tagsId != null) {
            addTags(id, tagsId);
        }
        if (countAuthors(id) != 0) {
            updateAuthor(id, authorId);
        } else {
            addAuthor(id, authorId);
        }

    }

    /**
     * Get full news info
     *
     * @param newsId
     * @return fullNews
     * @throws ServiceException
     */
    public FullNewsWorkItem getFullNews(Long newsId) throws ServiceException {
        FullNewsWorkItem fullNews = new FullNewsWorkItem();
        News news = getNews(newsId);
        String authorName = getAuthorName(newsId);
        List<String> tagsNames = getTagsNames(newsId);
        List<CommentWorkItem> comments = getComments(newsId);
        fullNews.setAuthorName(authorName);
        fullNews.setTagsNames(tagsNames);
        fullNews.setNews(news);
        fullNews.setComments(comments);
        return fullNews;
    }

    /**
     * Add tags to news with given id
     *
     * @param newsId
     * @param tagsId
     * @throws ServiceException
     */
    public void addTags(Long newsId, List<Long> tagsId) throws ServiceException {
        try {
            newsDAO.addTags(newsId, tagsId);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * Add author to news with given id
     *
     * @param newsId
     * @param authorId
     * @throws ServiceException
     */
    public void addAuthor(Long newsId, Long authorId) throws ServiceException {
        try {
            newsDAO.addAuthor(newsId, authorId);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }



    /**
     * Add news
     *
     * @param news
     * @return id of added news
     * @throws ServiceException
     */
    public Long addNews(News news) throws ServiceException {
        try {
            Long id = newsDAO.add(news);
            return id;
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * Get tags of news with given id
     *
     * @param newsId
     * @return list of tags
     * @throws ServiceException
     */
    public List<Tag> getTags(Long newsId) throws ServiceException {
        try {
            List<Tag> tags = newsDAO.getTags(newsId);
            return tags;
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * Get comments of news with given id
     *
     * @param newsId
     * @return list of comments
     * @throws ServiceException
     */
    public List<CommentWorkItem> getComments(Long newsId) throws ServiceException {
        try {
            List<CommentWorkItem> comments = newsDAO.getComments(newsId);
            return comments;
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * Get author of news with given id
     *
     * @param newsId
     * @return author
     * @throws ServiceException
     */
    public Author getAuthor(Long newsId) throws ServiceException {
        try {
            Author author = newsDAO.getAuthor(newsId);
            return author;
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * Get news with given id
     *
     * @param newsId
     * @return news
     * @throws ServiceException
     */
    public News getNews(Long newsId) throws ServiceException {
        try {
            News news = newsDAO.get(newsId);
            return news;
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * Update news with given id
     *
     * @param news
     * @throws ServiceException
     */
    public void updateNews(News news) throws ServiceException {
        try {
            newsDAO.update(news);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * Delete all tags of news with given id
     *
     * @param newsId
     * @throws ServiceException
     */
    public void deleteTags(Long newsId) throws ServiceException {
        try {
            newsDAO.deleteAllTags(newsId);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * Update author of news with given id
     *
     * @param newsId
     * @param authorId
     * @throws ServiceException
     */
    public void updateAuthor(Long newsId, Long authorId) throws ServiceException {
        try {
            newsDAO.updateAuthor(newsId, authorId);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * Count authors of news with given id
     *
     * @param newsId
     * @return
     * @throws ServiceException
     */
    public int countAuthors(Long newsId) throws ServiceException {
        try {
            return newsDAO.countAuthors(newsId);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * Delete author of news with given id
     *
     * @param newsId
     * @throws ServiceException
     */
    public void deleteAuthor(Long newsId) throws ServiceException {
        try {
            newsDAO.deleteAuthor(newsId);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * Delete all comments of news with given id
     *
     * @param newsId
     * @throws ServiceException
     */
    public void deleteComments(Long newsId) throws ServiceException {
        try {
            newsDAO.deleteAllComments(newsId);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * Delete news with given id
     *
     * @param newsId
     * @throws ServiceException
     */
    public void deleteNews(Long newsId) throws ServiceException {
        try {
            newsDAO.delete(newsId);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * Get tags' names of news with given id
     *
     * @param newsId
     * @return list of tags' names
     * @throws ServiceException
     */
    public List<String> getTagsNames(Long newsId) throws ServiceException {
        try {
            List<String> tagsNames = newsDAO.getTagsNames(newsId);
            return tagsNames;
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * Find news of defined amount by criteria
     *
     * @param criteria
     * @param from
     * @param amount
     * @return
     * @throws ServiceException
     */
    public List<NewsWorkItem> findByCriteria(SearchCriteriaVO criteria, int from, int amount) throws ServiceException {
        try {
            List<NewsWorkItem> newsList = searchDAO.find(criteria, from, amount);
            for (NewsWorkItem news : newsList) {
                news.setTagsNames(getTagsNames(news.getId()));
            }
            return newsList;
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }


    /**
     * Get name of author
     *
     * @param newsId
     * @return author name
     * @throws ServiceException
     */
    public String getAuthorName(Long newsId) throws ServiceException {
        try {
            String authorName = newsDAO.getAuthorName(newsId);
            return authorName;
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * Get amount of all news with criteria
     *
     * @param criteria
     * @return amount of news
     * @throws ServiceException
     */
    public int getCriteriaNewsAmount(SearchCriteriaVO criteria) throws ServiceException {
        try {
            int amount = searchDAO.countFoundNews(criteria);
            return amount;
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * Get author's id of news with given id
     *
     * @param newsId
     * @return authorId
     * @throws ServiceException
     */
    public Long getAuthorId(Long newsId) throws ServiceException {
        try {
            return newsDAO.getAuthorId(newsId);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * Get tag's id of news with given id
     *
     * @param newsId
     * @return list of tag's id
     * @throws ServiceException
     */
    public List<Long> getTagsId(Long newsId) throws ServiceException {
        try {
            return newsDAO.getTagsId(newsId);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * Get defined amount of news' id
     *
     * @param from
     * @param amount
     * @return list of news' id
     * @throws ServiceException
     */
    public List<Long> getNewsIdList(SearchCriteriaVO criteria, int from, int amount) throws ServiceException {
        try {
            List<Long> newsIdList = searchDAO.findIdList(criteria,from, amount);
            return newsIdList;
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }
}


