package com.epam.newsmanagement.dao.impl;

import com.epam.newsmanagement.dao.INewsDAO;
import com.epam.newsmanagement.entity.*;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.util.DBUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class NewsDAOImpl implements INewsDAO {
    /**
     * object for getting connections
     */
    @Autowired
    DriverManagerDataSource dataSource;

    private static final String SQL_INSERT_NEWS = "INSERT INTO news (news_id, title, short_text,full_text,creation_date, modification_date) VALUES (news_seq.nextval,?,?,?,?,?)";
    private static final String SQL_SELECT_ALL_NEWS = "SELECT n.news_id, n.title, n.short_text, "
            + "n.full_text, n.creation_date, n.modification_date FROM news n ORDER BY (SELECT COUNT(*) FROM comments c WHERE c.news_id = n.news_id) DESC,modification_date DESC ";
    private static final String SQL_DELETE_NEWS = "DELETE FROM news WHERE news_id = ?";
    private static final String SQL_COUNT_NEWS = "SELECT COUNT(*) FROM news";
    private static final String SQL_UPDATE_NEWS = "UPDATE news SET title = ?, short_text = ?,full_text = ?, modification_date = ? WHERE news_id = ?";
    private static final String SQL_SELECT_NEWS_BY_ID = "SELECT n.news_id, n.title, n.short_text, n.full_text, n.creation_date, n.modification_date FROM news n WHERE n.news_id = ? ";
    private static final String SQL_INSERT_NEWS_TAG = "INSERT INTO news_tag (news_id, tag_id) VALUES (?,?) ";
    private static final String SQL_INSERT_AUTHOR = "INSERT INTO news_author (news_id, author_id) VALUES (?,?) ";
    private static final String SQL_GET_AUTHOR = "SELECT a.author_id, a.author_name, a.expired FROM author a JOIN news_author na ON a.author_id = na.author_id WHERE na.news_id = ? ";
    private static final String SQL_GET_COMMENTS = "SELECT c.comment_id, c.comment_text, c.creation_date  FROM comments c WHERE c.news_id = ? ORDER BY c.creation_date";
    private static final String SQL_GET_TAGS = "SELECT t.tag_id, t.tag_name FROM tag t JOIN news_tag nt ON t.tag_id = nt.tag_id WHERE nt.news_id = ? ";
    private static final String SQL_DELETE_AUTHOR = "DELETE FROM news_author WHERE news_id = ? ";
    private static final String SQL_UPDATE_AUTHOR = "UPDATE news_author SET author_id = ? WHERE news_id = ? ";
    private static final String SQL_COUNT_AUTHORS = "SELECT COUNT(*) FROM news_author na WHERE na.news_id = ? ";
    private static final String SQL_DELETE_TAG = "DELETE FROM news_tag WHERE news_id = ? AND tag_id = ? ";
    private static final String SQL_DELETE_ALL_TAGS = "DELETE FROM news_tag WHERE news_id = ?";
    private static final String SQL_DELETE_NEWS_COMMENTS = "DELETE FROM comments WHERE news_id = ?";
    private static final String SQL_SELECT_DEFINED_AMOUNT_OF_NEWS = " SELECT * " +
            " FROM (SELECT n.news_id, n.title, n.short_text, n.creation_date, " +
            "    a.author_name, (SELECT COUNT(comment_id) FROM comments c WHERE c.news_id = n.news_id) as comments_amount " +
            "              ,n.modification_date, ROWNUM rnum " +
            "          FROM news n LEFT JOIN news_author na ON n.news_id = na.news_id " +
            "          LEFT JOIN AUTHOR a ON a.AUTHOR_ID = na.AUTHOR_ID " +
            "          ORDER BY comments_amount DESC,n.modification_date DESC) " +
            " WHERE rnum BETWEEN ? AND ? ";
    private static final String SQL_GET_TAGS_NAMES = "SELECT t.tag_name FROM tag t JOIN news_tag nt ON t.tag_id = nt.tag_id WHERE nt.news_id = ? ";
    private static final String SQL_GET_AUTHOR_NAME =  "SELECT a.author_name FROM author a JOIN news_author na ON a.author_id = na.author_id WHERE na.news_id = ? ";
    private static final String SQL_GET_TAGS_ID = "SELECT nt.tag_id FROM news_tag nt WHERE nt.news_id = ? ";
    private static final String SQL_GET_AUTHOR_ID =  "SELECT na.author_id FROM news_author na WHERE na.news_id = ? ";

    /**
     * Insert news in database
     *
     * @param news to add
     * @return id of inserted news
     * @throws DAOException
     */
    public Long add(News news) throws DAOException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            cn = DataSourceUtils.doGetConnection(dataSource);
            String[] gen_id = {"news_id"};
            ps = cn.prepareStatement(SQL_INSERT_NEWS, gen_id);
            ps.setString(1, news.getTitle());
            ps.setString(2, news.getShortText());
            ps.setString(3, news.getFullText());
            ps.setTimestamp(4, new Timestamp(news.getCreationDate().getTime()));
            ps.setDate(5, new Date(news.getCreationDate().getTime()));
            ps.executeUpdate();
            rs = ps.getGeneratedKeys();
            rs.next();
            Long id = rs.getLong(1);
            return id;

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(rs, ps, cn, dataSource);
        }
    }

    /**
     * Update given news in database
     *
     * @param news to update
     * @throws DAOException
     */
    public void update(News news) throws DAOException {
        Connection cn = null;
        PreparedStatement ps = null;
        try {
            cn = DataSourceUtils.doGetConnection(dataSource);
            ps = cn.prepareStatement(SQL_UPDATE_NEWS);
            ps.setString(1, news.getTitle());
            ps.setString(2, news.getShortText());
            ps.setString(3, news.getFullText());
            ps.setDate(4, new Date(news.getModificationDate().getTime()));
            ps.setLong(5, news.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(ps, cn, dataSource);
        }

    }

    /**
     * Delete news with given id from database
     *
     * @param newsId
     * @throws DAOException
     */
    public void delete(Long newsId) throws DAOException {
        Connection cn = null;
        PreparedStatement ps = null;
        try {
            cn = DataSourceUtils.doGetConnection(dataSource);
            ps = cn.prepareStatement(SQL_DELETE_NEWS);
            ps.setLong(1, newsId);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(ps, cn, dataSource);
        }

    }

    /**
     * Get all news from database
     *
     * @return list of news
     * @throws DAOException
     */
    public List<News> selectAll() throws DAOException {
        Connection cn = null;
        Statement st = null;
        ResultSet rs = null;
        try {
            cn = DataSourceUtils.doGetConnection(dataSource);
            st = cn.createStatement();
            rs = st.executeQuery(SQL_SELECT_ALL_NEWS);
            List<News> news = new ArrayList<News>();
            while (rs.next()) {
                news.add(new News(rs.getLong(1), rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getTimestamp(5), rs.getDate(6)));
            }
            return news;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(rs, st, cn, dataSource);
        }

    }

    /**
     * Count amount of all news in database
     *
     * @return amount of all news
     * @throws DAOException
     */
    public int countAll() throws DAOException {
        Connection cn = null;
        Statement st = null;
        ResultSet rs = null;
        try {
            cn = DataSourceUtils.doGetConnection(dataSource);
            st = cn.createStatement();
            rs = st.executeQuery(SQL_COUNT_NEWS);
            int count;
            rs.next();
            count = rs.getInt(1);
            return count;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(rs, st, cn, dataSource);
        }
    }

    /**
     * Get news with given id
     *
     * @param newsId
     * @return news
     * @throws DAOException
     */
    public News get(Long newsId) throws DAOException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        News news = null;
        try {
            cn = DataSourceUtils.doGetConnection(dataSource);
            ps = cn.prepareStatement(SQL_SELECT_NEWS_BY_ID);
            ps.setLong(1, newsId);
            rs = ps.executeQuery();
            if (rs.next()) {
                news = new News(rs.getLong(1), rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getTimestamp(5), rs.getDate(6));
            }
            return news;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(rs, ps, cn, dataSource);
        }
    }

    /**
     * Add tags to news with given id
     *
     * @param newsId of news
     * @param tagsId to add
     * @throws DAOException
     */
    public void addTags(Long newsId, List<Long> tagsId) throws DAOException {
        Connection cn = null;
        PreparedStatement ps = null;
        try {
            cn = DataSourceUtils.doGetConnection(dataSource);
            ps = cn.prepareStatement(SQL_INSERT_NEWS_TAG);
            for (Long tagId : tagsId) {
                ps.setLong(1, newsId);
                ps.setLong(2, tagId);
                ps.addBatch();
            }
            ps.executeBatch();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(ps, cn, dataSource);
        }

    }

    /**
     * Add author to news with given id
     *
     * @param newsId
     * @param authorId to add
     * @throws DAOException
     */
    public void addAuthor(Long newsId, Long authorId) throws DAOException {
        Connection cn = null;
        PreparedStatement ps = null;
        try {
            cn = DataSourceUtils.doGetConnection(dataSource);
            ps = cn.prepareStatement(SQL_INSERT_AUTHOR);
            ps.setLong(1, newsId);
            ps.setLong(2, authorId);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(ps, cn, dataSource);
        }

    }

    /**
     * Get author of news with given id
     *
     * @param newsId
     * @return author
     * @throws DAOException
     */
    public Author getAuthor(Long newsId) throws DAOException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Author author = null;
        try {
            cn = DataSourceUtils.doGetConnection(dataSource);
            ps = cn.prepareStatement(SQL_GET_AUTHOR);
            ps.setLong(1, newsId);
            rs = ps.executeQuery();
            if (rs.next()) {
                author = new Author(rs.getLong(1), rs.getString(2), rs.getTimestamp(3));
            }
            return author;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(rs, ps, cn, dataSource);
        }
    }

    /**
     * Delete author from news with given id
     *
     * @param newsId
     * @throws DAOException
     */
    public void deleteAuthor(Long newsId) throws DAOException {
        Connection cn = null;
        PreparedStatement ps = null;
        try {
            cn = DataSourceUtils.doGetConnection(dataSource);
            ps = cn.prepareStatement(SQL_DELETE_AUTHOR);
            ps.setLong(1, newsId);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(ps, cn, dataSource);
        }
    }

    /**
     * Update author of news with given id
     *
     * @param newsId
     * @param authorId
     * @throws DAOException
     */
    public void updateAuthor(Long newsId, Long authorId) throws DAOException {
        Connection cn = null;
        PreparedStatement ps = null;
        try {
            cn = DataSourceUtils.doGetConnection(dataSource);
            ps = cn.prepareStatement(SQL_UPDATE_AUTHOR);
            ps.setLong(1, authorId);
            ps.setLong(2, newsId);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(ps, cn, dataSource);
        }
    }

    /**
     * Count authors of news with given id
     *
     * @param newsId
     * @return amount of authors
     * @throws DAOException
     */
    public int countAuthors(Long newsId) throws DAOException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            cn = DataSourceUtils.doGetConnection(dataSource);
            ps = cn.prepareStatement(SQL_COUNT_AUTHORS);
            ps.setLong(1, newsId);
            rs = ps.executeQuery();
            int count;
            rs.next();
            count = rs.getInt(1);
            return count;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(rs, ps, cn, dataSource);
        }

    }

    /**
     * Delete tag with given tagId from news with newsId
     *
     * @param newsId
     * @param tagId
     * @throws DAOException
     */
    public void deleteTag(Long newsId, Long tagId) throws DAOException {
        Connection cn = null;
        PreparedStatement ps = null;
        try {
            cn = DataSourceUtils.doGetConnection(dataSource);
            ps = cn.prepareStatement(SQL_DELETE_TAG);
            ps.setLong(1, newsId);
            ps.setLong(2, tagId);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(ps, cn, dataSource);
        }
    }

    /**
     * Delete all tags of news with given id
     *
     * @param newsId
     * @throws DAOException
     */
    public void deleteAllTags(Long newsId) throws DAOException {
        Connection cn = null;
        PreparedStatement ps = null;
        try {
            cn = DataSourceUtils.doGetConnection(dataSource);
            ps = cn.prepareStatement(SQL_DELETE_ALL_TAGS);
            ps.setLong(1, newsId);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(ps, cn, dataSource);
        }

    }

    /**
     * Get all news tags by given id
     *
     * @param newsId
     * @return list of tags
     * @throws DAOException
     */
    public List<Tag> getTags(Long newsId) throws DAOException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            cn = DataSourceUtils.doGetConnection(dataSource);
            ps = cn.prepareStatement(SQL_GET_TAGS);
            ps.setLong(1, newsId);
            rs = ps.executeQuery();
            List<Tag> tags = new ArrayList<Tag>();
            while (rs.next()) {
                tags.add(new Tag(rs.getLong(1), rs.getString(2)));
            }
            return tags;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(rs, ps, cn, dataSource);
        }

    }

    /**
     * Get all comments of news with given id
     *
     * @param newsId
     * @return
     * @throws DAOException
     */
    public List<CommentWorkItem> getComments(Long newsId) throws DAOException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            cn = DataSourceUtils.doGetConnection(dataSource);
            ps = cn.prepareStatement(SQL_GET_COMMENTS);
            ps.setLong(1, newsId);
            rs = ps.executeQuery();
            List<CommentWorkItem> comments = new ArrayList<CommentWorkItem>();
            while (rs.next()) {
                comments.add(new CommentWorkItem(rs.getLong(1), rs.getString(2), rs.getTimestamp(3)));
            }
            return comments;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(rs, ps, cn, dataSource);
        }

    }

    /**
     * Delete all comments of news with given id
     *
     * @param newsId
     * @throws DAOException
     */
    public void deleteAllComments(Long newsId) throws DAOException {
        Connection cn = null;
        PreparedStatement ps = null;
        try {
            cn = DataSourceUtils.doGetConnection(dataSource);
            ps = cn.prepareStatement(SQL_DELETE_NEWS_COMMENTS);
            ps.setLong(1, newsId);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(ps, cn, dataSource);
        }
    }

    /**
     * Get list of news with defined amount starting from defined number
     *
     * @param from
     * @param amount
     * @return list of news
     * @throws DAOException
     */
    public List<NewsWorkItem> getNewsList(int from, int amount) throws DAOException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            cn = DataSourceUtils.doGetConnection(dataSource);
            ps = cn.prepareStatement(SQL_SELECT_DEFINED_AMOUNT_OF_NEWS);
            ps.setInt(1,from);
            ps.setInt(2, from + amount - 1);
            rs = ps.executeQuery();
            List<NewsWorkItem> news = new ArrayList<NewsWorkItem>();
            while (rs.next()) {
                news.add(new NewsWorkItem(rs.getLong(1), rs.getString(2), rs.getString(3),
                        rs.getTimestamp(4), rs.getString(5), rs.getInt(6)));
            }
            return news;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(rs, ps, cn, dataSource);
        }
    }

    /**
     * Get list of tags names of news with given id
     *
     * @param newsId
     * @return list of tags' names
     * @throws DAOException
     */
    public List<String> getTagsNames(Long newsId) throws DAOException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            cn = DataSourceUtils.doGetConnection(dataSource);
            ps = cn.prepareStatement(SQL_GET_TAGS_NAMES);
            ps.setLong(1, newsId);
            rs = ps.executeQuery();
            List<String> tagsNames = new ArrayList<String>();
            while (rs.next()) {
                tagsNames.add(rs.getString(1));
            }
            return tagsNames;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(rs, ps, cn, dataSource);
        }
    }

    /**
     * Get name of news author
     *
     * @param newsId
     * @return author name
     * @throws DAOException
     */
    public String getAuthorName(Long newsId) throws DAOException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            cn = DataSourceUtils.doGetConnection(dataSource);
            ps = cn.prepareStatement(SQL_GET_AUTHOR_NAME);
            ps.setLong(1, newsId);
            rs = ps.executeQuery();
            String authorName = null;
            if (rs.next()) {
                authorName = rs.getString(1);
            }
            return authorName;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(rs, ps, cn, dataSource);
        }
    }

    /**
     * Get tagsId of news
     *
     * @param newsId
     * @return list of tags' id
     * @throws DAOException
     */
    public List<Long> getTagsId(Long newsId) throws DAOException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            cn = DataSourceUtils.doGetConnection(dataSource);
            ps = cn.prepareStatement(SQL_GET_TAGS_ID);
            ps.setLong(1, newsId);
            rs = ps.executeQuery();
            List<Long> tagsId = new ArrayList<Long>();
            while (rs.next()) {
                tagsId.add(rs.getLong(1));
            }
            return tagsId;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(rs, ps, cn, dataSource);
        }
    }

    /**
     * Get id of author of news
     *
     * @param newsId
     * @return author id
     * @throws DAOException
     */
    public Long getAuthorId(Long newsId) throws DAOException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            cn = DataSourceUtils.doGetConnection(dataSource);
            ps = cn.prepareStatement(SQL_GET_AUTHOR_ID);
            ps.setLong(1, newsId);
            rs = ps.executeQuery();
            Long authorId = null;
            if (rs.next()) {
                authorId= rs.getLong(1);
            }
            return authorId;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(rs, ps, cn, dataSource);
        }
    }

}
