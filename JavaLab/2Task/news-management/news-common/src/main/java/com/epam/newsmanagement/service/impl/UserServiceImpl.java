package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.IUserDAO;
import com.epam.newsmanagement.entity.User;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.IUserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Veronica_Haritonova
 *         Service for work with users
 */
@Service
@Transactional(rollbackFor = ServiceException.class)
public class UserServiceImpl implements IUserService {
    Logger logger = Logger.getLogger(UserServiceImpl.class);
    @Autowired
    private IUserDAO userDAO;

    public UserServiceImpl(IUserDAO userDAO) {
        this.userDAO = userDAO;
    }

    public UserServiceImpl() {
    }

    /**
     * Add full user info
     *
     * @param user
     * @throws ServiceException
     */

    public void addFullUser(User user) throws ServiceException {
        Long id = addUser(user);
        addUserRoles(id, user.getRoles());
    }

    /**
     * Get user's roles
     *
     * @param userId
     * @return list of roles
     * @throws ServiceException
     */
    public List<String> getUserRoles(Long userId) throws ServiceException {
        try {
            List<String> roles = userDAO.getRoles(userId);
            return roles;
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * Add roles to user with given id
     *
     * @param userId
     * @param roles
     * @throws ServiceException
     */
    public void addUserRoles(Long userId, List<String> roles) throws ServiceException {
        try {
            userDAO.addRoles(userId, roles);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * Add user
     *
     * @param user
     * @return id of added user
     * @throws ServiceException
     */
    public Long addUser(User user) throws ServiceException {
        try {
            Long id = userDAO.add(user);
            return id;
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }
}
