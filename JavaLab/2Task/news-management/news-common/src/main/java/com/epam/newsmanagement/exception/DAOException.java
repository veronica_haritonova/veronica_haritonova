package com.epam.newsmanagement.exception;

public class DAOException extends Exception {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -3589884635248361196L;

    public DAOException(String message) {
        super(message);
    }

    public DAOException(String message, Throwable e) {
        super(message, e);
    }

    public DAOException(Throwable e) {
        super(e);
    }
}
