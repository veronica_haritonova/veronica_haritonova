package com.epam.newsmanagement.dao.impl;

import com.epam.newsmanagement.dao.ISearchDAO;
import com.epam.newsmanagement.entity.*;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.util.DBUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class SearchDAOImpl implements ISearchDAO {
    /**
     * object for getting connections
     */
    @Autowired
    DriverManagerDataSource dataSource;
    private static final String SQL_SELECT_DEFINED_AMOUNT_OF_NEWS_START = " SELECT * FROM (SELECT t.*, ROWNUM rnum " +
            " FROM (SELECT n.news_id, n.title, n.short_text, n.creation_date, " +
            "    a.author_name, (SELECT COUNT(comment_id) FROM comments c WHERE c.news_id = n.news_id) as comments_amount " +
            "              ,n.modification_date " +
            "          FROM news n LEFT JOIN news_author na ON n.news_id = na.news_id " +
            "          LEFT JOIN AUTHOR a ON a.AUTHOR_ID = na.AUTHOR_ID ";
    private static final String SQL_SELECT_BY_AUTHOR = " WHERE na.author_id = ? ";
    private static final String SQL_SELECT_DEFINED_AMOUNT_OF_NEWS_END =
            "          ORDER BY comments_amount DESC, n.modification_date DESC) t ) " +
            " WHERE rnum BETWEEN ? AND ? ";
    private static final String SQL_ORDER_BY = " ORDER BY (SELECT COUNT(*) FROM comments c WHERE c.news_id = n.news_id) DESC, modification_date DESC ";
    private static final String SQL_SELECT_BY_TAGS_BEGIN = " n.news_id IN (";
    private static final String SQL_INTERSECT = " INTERSECT ";
    private static final String SQL_SELECT_BY_AUTHOR_WITH_JOIN = " JOIN news_author na ON n.news_id = na.news_id WHERE na.author_id = ? ";
    private static final String SQL_WHERE = " WHERE ";
    private static final String SQL_AND = " AND ";
    private static final String SQL_SELECT_BY_TAGS = " (SELECT nt.news_id FROM news_tag nt WHERE nt.tag_id = ? ) ";
    private static final String SQL_CLOSED_BRACKET = ")";
    private static final String SQL_COUNT_START = "SELECT COUNT (n.news_id) FROM news n";
    private static final String SQL_SELECT_NEWS_ID = "SELECT * FROM (SELECT t.*, ROWNUM rnum  " +
            " FROM (SELECT n.news_id, " +
            "     (SELECT COUNT(comment_id) FROM comments c WHERE c.news_id = n.news_id) as comments_amount " +
            "              ,n.modification_date " +
            "          FROM news n LEFT JOIN news_author na ON n.news_id = na.news_id " +
            "          LEFT JOIN AUTHOR a ON a.AUTHOR_ID = na.AUTHOR_ID ";


    /**
     * Find defined amount of news by given criteria, which includes id of tags and id of author
     *
     * @param criteria
     * @param from
     * @param amount
     * @return list of news
     * @throws DAOException
     */
    public List<NewsWorkItem> find(SearchCriteriaVO criteria, int from, int amount) throws DAOException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            cn = DataSourceUtils.doGetConnection(dataSource);
            String query = buildQuery(criteria);
            ps = buildStatement(cn, query, criteria,from,amount);
            rs = ps.executeQuery();
            List<NewsWorkItem> news = new ArrayList<NewsWorkItem>();
            while (rs.next()) {
                news.add(new NewsWorkItem(rs.getLong(1), rs.getString(2), rs.getString(3),
                        rs.getTimestamp(4), rs.getString(5), rs.getInt(6)));
            }
            return news;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(rs, ps, cn, dataSource);
        }
    }

    /**
     * Get amount of news by criteria
     *
     * @param criteria
     * @return amount of news
     * @throws DAOException
     */
    public int countFoundNews(SearchCriteriaVO criteria) throws DAOException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            cn = DataSourceUtils.doGetConnection(dataSource);
            String query = buildCountQuery(criteria);
            ps = buildStatement(cn, query, criteria);
            rs = ps.executeQuery();
            rs.next();
            int count = rs.getInt(1);
            return count;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(rs, ps, cn, dataSource);
        }
    }

    /**
     * Find defined amount of news' id by given criteria, which includes id of tags and id of author
     *
     * @param criteria
     * @param from
     * @param amount
     * @return list of news id
     * @throws DAOException
     */
    public List<Long> findIdList(SearchCriteriaVO criteria, int from, int amount) throws DAOException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            cn = DataSourceUtils.doGetConnection(dataSource);
            String query = buildIdQuery(criteria, from, amount);
            ps = buildStatement(cn, query, criteria,from,amount);
            rs = ps.executeQuery();

            List<Long> newsIdList = new ArrayList<Long>();
            while (rs.next()) {
                newsIdList.add(rs.getLong(1));
            }
            return newsIdList;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(rs, ps, cn, dataSource);
        }
    }

    /**
     * Add in query string search by author
     *
     * @param query
     */
    private void addAuthorConditionInQuery(StringBuffer query, boolean paginationExist) {
        if(paginationExist) {
            query.append(SQL_SELECT_BY_AUTHOR);
        } else {
            query.append(SQL_SELECT_BY_AUTHOR_WITH_JOIN);
        }
    }

    /**
     * Add in query string search by tags
     *
     * @param query
     * @param tagsAmount
     */
    private void addTagsConditionInQuery(StringBuffer query, int tagsAmount) {
        for(int i = 0; i < tagsAmount; i++) {
            if(i != 0) {
                query.append(SQL_INTERSECT);
            } else {
                query.append(SQL_SELECT_BY_TAGS_BEGIN);
            }
            query.append(SQL_SELECT_BY_TAGS);
        }
        query.append(SQL_CLOSED_BRACKET);
    }

    /**
     * Build string of query to get news id with given criteria with id
     *
     * @param criteria
     * @return string of query
     */
    private String buildIdQuery(SearchCriteriaVO criteria,int from, int amount) {
        StringBuffer query = new StringBuffer(SQL_SELECT_NEWS_ID);
        if(criteria == null) {
            query.append(SQL_SELECT_DEFINED_AMOUNT_OF_NEWS_END);
            return query.toString();
        }
        Long authorId = criteria.getAuthorId();
        List<Long> tagsId = criteria.getTagsId();
        boolean authorExist = (authorId != null);
        boolean tagsExist = (tagsId != null) ? !tagsId.isEmpty() : false;
        if (!authorExist && !tagsExist) {
            query.append(SQL_SELECT_DEFINED_AMOUNT_OF_NEWS_END);
            return query.toString();
        }
        if(authorExist) {
            addAuthorConditionInQuery(query,true);
        }
        if(tagsExist) {
            if(authorExist && tagsExist) {
                query.append(SQL_AND);
            } else {
                query.append(SQL_WHERE);
            }
            int tagsAmount = tagsId.size();
            addTagsConditionInQuery(query, tagsAmount);
        }
        query.append(SQL_SELECT_DEFINED_AMOUNT_OF_NEWS_END);
        return query.toString();
    }
    /**
     * Build string of query with given criteria with id
     *
     * @param criteria
     * @return string of query
     */
    private String buildQuery(SearchCriteriaVO criteria) {
        StringBuffer query = new StringBuffer(SQL_SELECT_DEFINED_AMOUNT_OF_NEWS_START);
        if(criteria == null) {
            query.append(SQL_SELECT_DEFINED_AMOUNT_OF_NEWS_END);
            return query.toString();
        }
        Long authorId = criteria.getAuthorId();
        List<Long> tagsId = criteria.getTagsId();
        boolean authorExist = (authorId != null);
        boolean tagsExist = (tagsId != null) ? !tagsId.isEmpty() : false;
        if (!authorExist && !tagsExist) {
            query.append(SQL_SELECT_DEFINED_AMOUNT_OF_NEWS_END);
            return query.toString();
        }
        if(authorExist) {
            addAuthorConditionInQuery(query,true);
        }
        if(tagsExist) {
            if(authorExist && tagsExist) {
                query.append(SQL_AND);
            } else {
                query.append(SQL_WHERE);
            }
            int tagsAmount = tagsId.size();
            addTagsConditionInQuery(query, tagsAmount);
        }
        query.append(SQL_SELECT_DEFINED_AMOUNT_OF_NEWS_END);
        return query.toString();
    }
    /**
     * Build string of query with given criteria with id
     *
     * @param criteria
     * @return string of query
     */
    private String buildCountQuery(SearchCriteriaVO criteria) {
        StringBuffer query = new StringBuffer(SQL_COUNT_START);
        if(criteria == null) {
            return query.toString();
        }
        Long authorId = criteria.getAuthorId();
        List<Long> tagsId = criteria.getTagsId();
        boolean authorExist = (authorId != null);
        boolean tagsExist = (tagsId != null) ? !tagsId.isEmpty() : false;
        if (!authorExist && !tagsExist) {
            return query.toString();
        }
        if(authorExist) {
            addAuthorConditionInQuery(query,false);
        }
        if(tagsExist) {
            if(authorExist && tagsExist) {
                query.append(SQL_AND);
            } else {
                query.append(SQL_WHERE);
            }
            int tagsAmount = tagsId.size();
            addTagsConditionInQuery(query, tagsAmount);
        }
        return query.toString();
    }
    /**
     * Build preparedStatement with given query and search criteria with id
     *
     * @param cn
     * @param query
     * @param criteria
     * @return preparedStatement
     * @throws SQLException
     */
    private PreparedStatement buildStatement(Connection cn, String query, SearchCriteriaVO criteria) throws SQLException {
        PreparedStatement ps = cn.prepareStatement(query);
        if(criteria == null) {
            return ps;
        }
        Long authorId = criteria.getAuthorId();
        List<Long> tagsId = criteria.getTagsId();
        boolean authorExist = (authorId != null);
        boolean tagsExist = (tagsId != null) ? !tagsId.isEmpty() : false;

        if (!authorExist && !tagsExist) {
            return ps;
        }
        int index = 1;
        if (authorExist) {
            ps.setLong(index, authorId);
            index++;
        }
        if (tagsExist) {
            for (Long tagId : tagsId) {
                ps.setLong(index, tagId);
                index++;
            }
        }
        return ps;
    }


    /**
     * Build preparedStatement with given query and search criteria
     *
     * @param cn
     * @param query
     * @param criteria
     * @return preparedStatement
     * @throws SQLException
     */
    private PreparedStatement buildStatement(Connection cn, String query, SearchCriteriaVO criteria,int from, int amount) throws SQLException {
        PreparedStatement ps = cn.prepareStatement(query);
        int index = 1;
        if(criteria != null) {
            Long authorId = criteria.getAuthorId();
            List<Long> tagsId = criteria.getTagsId();
            boolean authorExist = (authorId != null);
            boolean tagsExist = (tagsId != null) ? !tagsId.isEmpty() : false;
            if (authorExist || tagsExist) {
                if (authorExist) {
                    ps.setLong(index, authorId);
                    index++;
                }
                if (tagsExist) {
                    for (Long tagId : tagsId) {
                        ps.setLong(index, tagId);
                        index++;
                    }
                }
            }
        }
        ps.setInt(index++, from);
        ps.setInt(index, from + amount - 1);
        return ps;
    }
}
