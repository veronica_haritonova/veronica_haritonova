package com.epam.newsmanagement.dao.impl;

import com.epam.newsmanagement.dao.ITagDAO;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.util.DBUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Veronica_Haritonova
 *         Class for performing basic operations with tags in database, implements ITagDAO
 */
@Repository
public class TagDAOImpl implements ITagDAO {
    /**
     * object for getting connections
     */
    @Autowired
    DriverManagerDataSource dataSource;

    private static final String SQL_INSERT_TAG = "INSERT INTO tag (tag_id, tag_name) VALUES (tag_seq.nextval, ?)";
    private static final String SQL_UPDATE_TAG = "UPDATE tag SET tag_name = ? WHERE tag_id = ?";
    private static final String SQL_DELETE_TAG = "DELETE FROM tag WHERE tag_id = ?";
    private static final String SQL_SELECT_ALL_TAGS = "SELECT t.tag_id, t.tag_name FROM tag t";
    private static final String SQL_SELECT_TAG_BY_ID = "SELECT t.tag_id, t.tag_name FROM tag t WHERE t.tag_id = ?";
    private static final String SQL_DELETE_TAG_IN_NEWS = "DELETE FROM news_tag WHERE tag_id = ?";

    /**
     * Insert tag into database
     *
     * @param tag
     * @return id of inserted tags
     * @throws DAOException
     */
    public Long add(Tag tag) throws DAOException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            cn = DataSourceUtils.doGetConnection(dataSource);
            String[] gen_id = {"tag_id"};
            ps = cn.prepareStatement(SQL_INSERT_TAG, gen_id);
            ps.setString(1, tag.getTagName());
            ps.executeUpdate();
            rs = ps.getGeneratedKeys();
            rs.next();
            Long id = rs.getLong(1);
            return id;

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(rs, ps, cn, dataSource);
        }
    }

    /**
     * Update given tag in database
     *
     * @param tag
     * @throws DAOException
     */
    public void update(Tag tag) throws DAOException {
        Connection cn = null;
        PreparedStatement ps = null;
        try {
            cn = DataSourceUtils.doGetConnection(dataSource);
            ps = cn.prepareStatement(SQL_UPDATE_TAG);
            ps.setString(1, tag.getTagName());
            ps.setLong(2, tag.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(ps, cn, dataSource);
        }
    }

    /**
     * Delete tag with given id from database
     *
     * @param tagId
     * @throws DAOException
     */
    public void delete(Long tagId) throws DAOException {
        Connection cn = null;
        PreparedStatement ps = null;
        try {
            cn = DataSourceUtils.doGetConnection(dataSource);
            ps = cn.prepareStatement(SQL_DELETE_TAG);
            ps.setLong(1, tagId);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(ps, cn, dataSource);
        }

    }

    /**
     * Select all tags in database
     *
     * @return List of all tags in database
     * @throws DAOException
     */
    public List<Tag> selectAll() throws DAOException {
        Connection cn = null;
        Statement st = null;
        ResultSet rs = null;
        try {
            cn = DataSourceUtils.doGetConnection(dataSource);
            st = cn.createStatement();
            rs = st.executeQuery(SQL_SELECT_ALL_TAGS);
            List<Tag> tags = new ArrayList<Tag>();
            while (rs.next()) {
                tags.add(new Tag(rs.getLong(1), rs.getString(2)));
            }
            return tags;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(rs, st, cn, dataSource);
        }
    }

    /**
     * Find tag with given id in database
     *
     * @param id
     * @return found tag or null if it doesn't exist
     * @throws DAOException
     */
    public Tag get(Long id) throws DAOException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            cn = DataSourceUtils.doGetConnection(dataSource);
            ps = cn.prepareStatement(SQL_SELECT_TAG_BY_ID);
            ps.setLong(1, id);
            rs = ps.executeQuery();
            Tag tag = null;
            if (rs.next()) {
                tag = new Tag(rs.getLong(1), rs.getString(2));
            }
            return tag;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(rs, ps, cn, dataSource);
        }
    }

    /**
     * Delete link between tag with given id and news
     *
     * @param tagId
     * @throws DAOException
     */
    public void deleteTagInNews(Long tagId) throws DAOException {
        Connection cn = null;
        PreparedStatement ps = null;
        try {
            cn = DataSourceUtils.doGetConnection(dataSource);
            ps = cn.prepareStatement(SQL_DELETE_TAG_IN_NEWS);
            ps.setLong(1, tagId);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(ps, cn, dataSource);
        }
    }

    public DriverManagerDataSource getDataSource() {
        return dataSource;
    }

}
