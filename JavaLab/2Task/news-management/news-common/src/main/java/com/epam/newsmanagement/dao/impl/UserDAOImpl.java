package com.epam.newsmanagement.dao.impl;

import com.epam.newsmanagement.dao.IUserDAO;
import com.epam.newsmanagement.entity.User;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.util.DBUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class UserDAOImpl implements IUserDAO {
    /**
     * object for getting connections
     */
    @Autowired
    DriverManagerDataSource dataSource;

    private static final String SQL_INSERT_USER = "INSERT INTO users (user_id, user_name, login, password) VALUES (users_seq.nextval, ?, ?, ?)";
    private static final String SQL_UPDATE_USER = "UPDATE users SET user_name = ?, login = ?, password = ? WHERE user_id = ?";
    private static final String SQL_DELETE_USER = "DELETE FROM users WHERE user_id = ?";
    private static final String SQL_SELECT_ALL_USERS = "SELECT u.user_id, u.user_name, u.login, u.password FROM users u";
    private static final String SQL_SELECT_ROLES = "SELECT r.role_name FROM roles r WHERE r.user_id = ?";
    private static final String SQL_INSERT_ROLE = "INSERT INTO roles (user_id, role_name) VALUES (?, ?) ";
    private static final String SQL_DELETE_ROLES = "DELETE FROM roles WHERE user_id = ? ";

    /**
     * Insert given user into database
     *
     * @param user to add
     * @return id of inserted user
     * @throws DAOException
     */
    public Long add(User user) throws DAOException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            cn = DataSourceUtils.doGetConnection(dataSource);
            String[] gen_id = {"user_id"};
            ps = cn.prepareStatement(SQL_INSERT_USER, gen_id);
            ps.setString(1, user.getUserName());
            ps.setString(2, user.getLogin());
            ps.setString(3, user.getPassword());
            ps.executeUpdate();
            rs = ps.getGeneratedKeys();
            Long id;
            rs.next();
            id = rs.getLong(1);
            return id;

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(rs, ps, cn, dataSource);
        }
    }

    /**
     * Update data of given user in database
     *
     * @param user to update
     * @throws DAOException
     */
    public void update(User user) throws DAOException {
        Connection cn = null;
        PreparedStatement ps = null;
        try {
            cn = DataSourceUtils.doGetConnection(dataSource);
            ps = cn.prepareStatement(SQL_UPDATE_USER);
            ps.setString(1, user.getUserName());
            ps.setString(2, user.getLogin());
            ps.setString(3, user.getPassword());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(ps, cn, dataSource);
        }

    }

    /**
     * Delete user with given id from database
     *
     * @param userId
     * @throws DAOException
     */
    public void delete(Long userId) throws DAOException {
        Connection cn = null;
        PreparedStatement ps = null;
        try {
            cn = DataSourceUtils.doGetConnection(dataSource);
            ps = cn.prepareStatement(SQL_DELETE_USER);
            ps.setLong(1, userId);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(ps, cn, dataSource);
        }

    }

    /**
     * Get all users
     *
     * @return list of all users
     * @throws DAOException
     */
    public List<User> selectAll() throws DAOException {
        Connection cn = null;
        Statement st = null;
        ResultSet rs = null;
        try {
            cn = DataSourceUtils.doGetConnection(dataSource);
            st = cn.createStatement();
            rs = st.executeQuery(SQL_SELECT_ALL_USERS);
            List<User> users = new ArrayList<User>();
            while (rs.next()) {
                users.add(new User(rs.getLong(1), rs.getString(2), rs.getString(3), rs.getString(4)));
            }
            return users;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(rs, st, cn, dataSource);
        }
    }

    /**
     * Get roles of user with given id
     *
     * @param userId
     * @return list of roles
     * @throws DAOException
     */
    public List<String> getRoles(Long userId) throws DAOException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            cn = DataSourceUtils.doGetConnection(dataSource);
            ps = cn.prepareStatement(SQL_SELECT_ROLES);
            ps.setLong(1, userId);
            rs = ps.executeQuery();
            List<String> roles = new ArrayList<String>();
            while (rs.next()) {
                roles.add(rs.getString(1));
            }
            return roles;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(rs, ps, cn, dataSource);
        }
    }

    /**
     * Add roles to user with given id
     *
     * @param userId
     * @param roles  to add
     * @throws DAOException
     */
    public void addRoles(Long userId, List<String> roles) throws DAOException {
        Connection cn = null;
        PreparedStatement ps = null;
        try {
            cn = DataSourceUtils.doGetConnection(dataSource);
            ps = cn.prepareStatement(SQL_INSERT_ROLE);
            for (String role : roles) {
                ps.setLong(1, userId);
                ps.setString(2, role);
                ps.addBatch();
            }
            ps.executeBatch();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(ps, cn, dataSource);
        }
    }

    /**
     * Delete roles of user
     *
     * @param userId
     * @throws DAOException
     */
    public void deleteRoles(Long userId) throws DAOException {
        Connection cn = null;
        PreparedStatement ps = null;
        try {
            cn = DataSourceUtils.doGetConnection(dataSource);
            ps = cn.prepareStatement(SQL_DELETE_ROLES);
            ps.setLong(1, userId);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(ps, cn, dataSource);
        }
    }

}
