package com.epam.newsmanagement.entity;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Veronica_Haritonova
 *         Class to store data of author
 */
public class Author implements Serializable {
    /**
     * Identificator of author
     */
    private Long id;

    /**
     * Name of author
     */
    @NotNull
    @NotEmpty
    @Size(max = 30)
    private String authorName;

    /**
     * Date when author was expired, null if he wasn't
     */
    private Date expired;

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 4466122375148624812L;

    public Author() {
        super();
    }

    public Author(Long id) {
        this.id = id;
    }

    public Author(String authorName) {
        this.authorName = authorName;
    }

    public Author(Long id, String authorName) {
        this.id = id;
        this.authorName = authorName;
    }

    public Author(Long id, String authorName, Date expired) {
        this.id = id;
        this.authorName = authorName;
        this.expired = expired;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public Date getExpired() {
        return expired;
    }

    public void setExpired(Date expired) {
        this.expired = expired;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Author)) return false;

        Author author = (Author) o;

        if (id != null ? !id.equals(author.id) : author.id != null) return false;
        if (authorName != null ? !authorName.equals(author.authorName) : author.authorName != null) return false;
        return !(expired != null ? !expired.equals(author.expired) : author.expired != null);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (authorName != null ? authorName.hashCode() : 0);
        result = 31 * result + (expired != null ? expired.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Author{" +
                "id=" + id +
                ", authorName='" + authorName + '\'' +
                ", expired=" + expired +
                '}';
    }
}
