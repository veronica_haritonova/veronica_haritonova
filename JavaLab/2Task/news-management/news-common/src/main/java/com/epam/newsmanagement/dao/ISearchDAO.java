package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.entity.NewsWorkItem;
import com.epam.newsmanagement.entity.SearchCriteriaVO;
import com.epam.newsmanagement.exception.DAOException;

import java.util.List;

public interface ISearchDAO {

    /**
     * Find defined amount of news by given criteria, which includes id of tags and id of author
     * @param criteria
     * @param from
     * @param amount
     * @return list of news
     * @throws DAOException
     */
    List<NewsWorkItem> find(SearchCriteriaVO criteria, int from, int amount) throws DAOException;

    /**
     * Get amount of news by criteria
     * @param criteria
     * @return
     * @throws DAOException
     */
    int countFoundNews(SearchCriteriaVO criteria) throws DAOException;
    /**
     * Find defined amount of news' id by given criteria, which includes id of tags and id of author
     * @param criteria
     * @param from
     * @param amount
     * @return list of news id
     * @throws DAOException
     */
    List<Long> findIdList(SearchCriteriaVO criteria, int from, int amount) throws DAOException;

}
