package com.epam.newsmanagement.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author  Veronica_Haritonova
 * Class to store info about news which necessary for news list
 */
public class NewsWorkItem implements Serializable {
    /**
     * Identificator of news
     */
    private Long id;
    /**
     * Title of news
     */
    private String title;
    /**
     * Short text of news
     */
    private String shortText;
    /**
     * Date news created
     */
    private Date creationDate;
    /**
     * Name of news author
     */
    private String authorName;
    /**
     * List of news tags' names
     */
    private List<String> tagsNames;
    /**
     * Amount of news comments
     */
    private int commentsAmount;
    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 3769197090533376436L;

    public NewsWorkItem(Long id, String title, String shortText, Date creationDate, String authorName, int commentsAmount) {
        this.id = id;
        this.title = title;
        this.shortText = shortText;
        this.creationDate = creationDate;
        this.authorName = authorName;
        this.commentsAmount = commentsAmount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortText() {
        return shortText;
    }

    public void setShortText(String shortText) {
        this.shortText = shortText;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public List<String> getTagsNames() {
        return tagsNames;
    }

    public void setTagsNames(List<String> tagsNames) {
        this.tagsNames = tagsNames;
    }

    public int getCommentsAmount() {
        return commentsAmount;
    }

    public void setCommentsAmount(int commentsAmount) {
        this.commentsAmount = commentsAmount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof NewsWorkItem)) return false;

        NewsWorkItem that = (NewsWorkItem) o;

        if (commentsAmount != that.commentsAmount) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (title != null ? !title.equals(that.title) : that.title != null) return false;
        if (shortText != null ? !shortText.equals(that.shortText) : that.shortText != null) return false;
        if (creationDate != null ? !creationDate.equals(that.creationDate) : that.creationDate != null) return false;
        if (authorName != null ? !authorName.equals(that.authorName) : that.authorName != null) return false;
        return !(tagsNames != null ? !tagsNames.equals(that.tagsNames) : that.tagsNames != null);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (shortText != null ? shortText.hashCode() : 0);
        result = 31 * result + (creationDate != null ? creationDate.hashCode() : 0);
        result = 31 * result + (authorName != null ? authorName.hashCode() : 0);
        result = 31 * result + (tagsNames != null ? tagsNames.hashCode() : 0);
        result = 31 * result + commentsAmount;
        return result;
    }

    @Override
    public String toString() {
        return "NewsWorkItem{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", shortText='" + shortText + '\'' +
                ", creationDate=" + creationDate +
                ", authorName='" + authorName + '\'' +
                ", tagsNames=" + tagsNames +
                ", commentsAmount=" + commentsAmount +
                '}';
    }
}
