package com.epam.newsmanagement.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Veronica_Haritonova
 */
public class CommentWorkItem implements Serializable {
    /**
     * Identificator of comment
     */
    private Long id;
    /**
     * Text of comment
     */
    private String text;

    /**
     * Date comment created
     */
    private Date creationDate;
    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -6863123704711283892L;

    public CommentWorkItem(Long id, String text, Date creationDate) {
        this.id = id;
        this.text = text;
        this.creationDate = creationDate;
    }

    public CommentWorkItem() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CommentWorkItem)) return false;

        CommentWorkItem that = (CommentWorkItem) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (text != null ? !text.equals(that.text) : that.text != null) return false;
        return !(creationDate != null ? !creationDate.equals(that.creationDate) : that.creationDate != null);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (text != null ? text.hashCode() : 0);
        result = 31 * result + (creationDate != null ? creationDate.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "CommentWorkItem{" +
                "id=" + id +
                ", text='" + text + '\'' +
                ", creationDate=" + creationDate +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }
}

