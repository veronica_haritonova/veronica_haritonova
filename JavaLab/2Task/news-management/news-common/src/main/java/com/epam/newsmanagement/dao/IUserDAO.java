package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.entity.User;
import com.epam.newsmanagement.exception.DAOException;

import java.util.List;

public interface IUserDAO {
    /**
     * Insert given user into database
     *
     * @param user to add
     * @return id of inserted user
     * @throws DAOException
     */
    Long add(User user) throws DAOException;

    /**
     * Update data of given user in database
     *
     * @param user to update
     * @throws DAOException
     */
    void update(User user) throws DAOException;

    /**
     * Delete user with given id from database
     *
     * @param userId
     * @throws DAOException
     */
    void delete(Long userId) throws DAOException;

    /**
     * Get all users
     *
     * @return list of all users
     * @throws DAOException
     */
    List<User> selectAll() throws DAOException;

    /**
     * Get roles of user with given id
     *
     * @param userId
     * @return list of roles
     * @throws DAOException
     */
    List<String> getRoles(Long userId) throws DAOException;

    /**
     * Add roles to user with given id
     *
     * @param userId
     * @param roles  to add
     * @throws DAOException
     */
    void addRoles(Long userId, List<String> roles) throws DAOException;

    /**
     * Delete roles of user
     *
     * @param userId
     * @throws DAOException
     */
    void deleteRoles(Long userId) throws DAOException;
}
