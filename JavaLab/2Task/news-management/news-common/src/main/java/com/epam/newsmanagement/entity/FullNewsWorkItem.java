package com.epam.newsmanagement.entity;

import java.io.Serializable;
import java.util.List;

/**
 * @author Veronica_Haritonova
 */
public class FullNewsWorkItem implements Serializable {
    /**
     * Contains basic information about news
     */
    private News news;
    /**
     * Author of news, can be null or empty
     */
    private String authorName;
    /**
     * Tags of news, can be null or empty
     */
    private List<String> tagsNames;
    /**
     * Comments of news, can be null or empty
     */
    private List<CommentWorkItem> comments;
    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -5262874564392630809L;

    public News getNews() {
        return news;
    }

    public void setNews(News news) {
        this.news = news;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public List<String> getTagsNames() {
        return tagsNames;
    }

    public void setTagsNames(List<String> tagsNames) {
        this.tagsNames = tagsNames;
    }

    public List<CommentWorkItem> getComments() {
        return comments;
    }

    public void setComments(List<CommentWorkItem> comments) {
        this.comments = comments;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FullNewsWorkItem)) return false;

        FullNewsWorkItem that = (FullNewsWorkItem) o;

        if (news != null ? !news.equals(that.news) : that.news != null) return false;
        if (authorName != null ? !authorName.equals(that.authorName) : that.authorName != null) return false;
        if (tagsNames != null ? !tagsNames.equals(that.tagsNames) : that.tagsNames != null) return false;
        return !(comments != null ? !comments.equals(that.comments) : that.comments != null);

    }

    @Override
    public int hashCode() {
        int result = news != null ? news.hashCode() : 0;
        result = 31 * result + (authorName != null ? authorName.hashCode() : 0);
        result = 31 * result + (tagsNames != null ? tagsNames.hashCode() : 0);
        result = 31 * result + (comments != null ? comments.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "FullNewsWorkItem{" +
                "news=" + news +
                ", authorName='" + authorName + '\'' +
                ", tagsNames=" + tagsNames +
                ", comments=" + comments +
                '}';
    }
}
