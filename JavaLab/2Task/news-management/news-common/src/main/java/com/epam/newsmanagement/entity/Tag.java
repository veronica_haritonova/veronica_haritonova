package com.epam.newsmanagement.entity;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author Veronica_Haritonova
 *         Class to store data of tag
 */
public class Tag implements Serializable {
    /**
     * Identificator of tag
     */
    private Long id;

    /**
     * Name of tag
     */
    @NotEmpty
    @Size(max = 30)
    private String tagName;

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 5498574324973865394L;

    public Tag(String tagName) {
        this.tagName = tagName;
    }

    public Tag() {

    }

    public Tag(Long id) {
        this.id = id;
    }

    public Tag(Long id, String tagName) {
        this.id = id;
        this.tagName = tagName;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Tag)) return false;

        Tag tag = (Tag) o;

        if (id != null ? !id.equals(tag.id) : tag.id != null) return false;
        return !(tagName != null ? !tagName.equals(tag.tagName) : tag.tagName != null);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (tagName != null ? tagName.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Tag{" +
                "id=" + id +
                ", tagName='" + tagName + '\'' +
                '}';
    }
}
