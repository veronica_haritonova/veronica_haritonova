package com.epam.newsmanagement.service;

import com.epam.newsmanagement.entity.User;
import com.epam.newsmanagement.exception.ServiceException;

import java.util.List;

/**
 * @author Veronica_Haritonova
 *         Service for work with users
 */
public interface IUserService {
    /**
     * Add full user info
     *
     * @param user
     * @throws ServiceException
     */
    void addFullUser(User user) throws ServiceException;

    /**
     * Get user's roles
     *
     * @param userId
     * @return list of roles
     * @throws ServiceException
     */
    List<String> getUserRoles(Long userId) throws ServiceException;

    /**
     * Add roles to user with given id
     *
     * @param userId
     * @param roles
     * @throws ServiceException
     */
    void addUserRoles(Long userId, List<String> roles) throws ServiceException;

    /**
     * Add user
     *
     * @param user
     * @return id of added user
     * @throws ServiceException
     */
    Long addUser(User user) throws ServiceException;
}
