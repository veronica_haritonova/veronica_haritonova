package com.epam.newsmanagement.dao.impl;

import com.epam.newsmanagement.dao.IAuthorDAO;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.util.DBUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class AuthorDAOImpl implements IAuthorDAO {
    /**
     * object for getting connections
     */
    @Autowired
    DriverManagerDataSource dataSource;

    private static final String SQL_INSERT_AUTHOR = "INSERT INTO author (author_id, author_name) VALUES (author_seq.nextval, ?)";
    private static final String SQL_UPDATE_AUTHOR = "UPDATE author SET author_name = ? WHERE author_id = ?";
    private static final String SQL_DELETE_AUTHOR = "DELETE FROM author WHERE author_id = ?";
    private static final String SQL_SELECT_ALL_AUTHORS = "SELECT a.author_id, a.author_name, a.expired FROM author a";
    private static final String SQL_UPDATE_EXPIRED = "UPDATE author SET expired = CURRENT_TIMESTAMP WHERE author_id = ?";
    private static final String SQL_SELECT_AUTHOR_BY_ID = "SELECT a.author_id, a.author_name, a.expired FROM author a WHERE a.author_id = ?";
    private static final String SQL_DELETE_AUTHOR_IN_NEWS = "DELETE FROM news_author WHERE author_id = ? ";

    /**
     * Insert author into database
     *
     * @param author
     * @return id of inserted author
     * @throws DAOException
     */
    public Long add(Author author) throws DAOException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            cn = DataSourceUtils.doGetConnection(dataSource);
            String[] gen_id = {"author_id"};
            ps = cn.prepareStatement(SQL_INSERT_AUTHOR, gen_id);
            ps.setString(1, author.getAuthorName());
            ps.executeUpdate();
            rs = ps.getGeneratedKeys();
            rs.next();
            Long id = rs.getLong(1);
            return id;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(rs, ps, cn, dataSource);
        }

    }

    /**
     * Update given author in database
     *
     * @param author
     * @throws DAOException
     */
    public void update(Author author) throws DAOException {
        Connection cn = null;
        PreparedStatement ps = null;
        try {
            cn = DataSourceUtils.doGetConnection(dataSource);
            ps = cn.prepareStatement(SQL_UPDATE_AUTHOR);
            ps.setString(1, author.getAuthorName());
            ps.setLong(2, author.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(ps, cn, dataSource);
        }
    }

    /**
     * Delete author with given id from database
     *
     * @param authorId
     * @throws DAOException
     */
    public void delete(Long authorId) throws DAOException {
        Connection cn = null;
        PreparedStatement ps = null;
        try {
            cn = DataSourceUtils.doGetConnection(dataSource);
            ps = cn.prepareStatement(SQL_DELETE_AUTHOR);
            ps.setLong(1, authorId);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(ps, cn, dataSource);
        }

    }

    /**
     * Select all authors in database
     *
     * @return List of all authors
     * @throws DAOException
     */
    public List<Author> selectAll() throws DAOException {
        Connection cn = null;
        Statement st = null;
        ResultSet rs = null;
        try {
            cn = DataSourceUtils.doGetConnection(dataSource);
            st = cn.createStatement();
            rs = st.executeQuery(SQL_SELECT_ALL_AUTHORS);
            List<Author> authors = new ArrayList<Author>();
            while (rs.next()) {
                authors.add(new Author(rs.getLong(1), rs.getString(2), rs.getTimestamp(3)));
            }
            return authors;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(rs, st, cn, dataSource);
        }
    }

    /**
     * Set date when author expired to author with given id
     *
     * @param authorId
     * @throws DAOException
     */
    public void setExpired(Long authorId) throws DAOException {
        Connection cn = null;
        PreparedStatement ps = null;
        try {
            cn = DataSourceUtils.doGetConnection(dataSource);
            ps = cn.prepareStatement(SQL_UPDATE_EXPIRED);
            ps.setLong(1, authorId);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(ps, cn, dataSource);
        }
    }

    /**
     * Find author with given id in database
     *
     * @param id of author to find
     * @return
     * @throws DAOException
     */
    public Author get(Long id) throws DAOException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            cn = DataSourceUtils.doGetConnection(dataSource);
            ps = cn.prepareStatement(SQL_SELECT_AUTHOR_BY_ID);
            ps.setLong(1, id);
            rs = ps.executeQuery();
            Author author = null;
            if (rs.next()) {
                author = new Author(rs.getLong(1), rs.getString(2), rs.getTimestamp(3));
            }
            return author;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(rs, ps, cn, dataSource);
        }
    }

    /**
     * Delete links between author and news
     *
     * @param authorId
     * @throws DAOException
     */
    public void deleteAuthorInNews(Long authorId) throws DAOException {
        Connection cn = null;
        PreparedStatement ps = null;
        try {
            cn = DataSourceUtils.doGetConnection(dataSource);
            ps = cn.prepareStatement(SQL_DELETE_AUTHOR_IN_NEWS);
            ps.setLong(1, authorId);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(ps, cn, dataSource);
        }
    }
}
