package com.epam.newsmanagement.service;

import com.epam.newsmanagement.entity.*;
import com.epam.newsmanagement.exception.ServiceException;

import java.util.List;

/**
 * @author Veronica_Haritonova
 *         Class to perform basic operations of news with its elements separately
 */
public interface INewsService {
    /**
     * Add full news info
     *
     * @param news
     * @param authorId
     * @param tagsId
     * @return id of news
     * @throws ServiceException
     */
    Long addFullNews(News news, Long authorId, List<Long> tagsId) throws ServiceException;

    /**
     * Delete full news info
     *
     * @param newsId
     * @throws ServiceException
     */
    void deleteFullNews(Long newsId) throws ServiceException;

    /**
     * Update full news info
     *
     * @param news
     * @param authorId
     * @param tagsId
     * @throws ServiceException
     */
    void updateFullNews(News news, Long authorId, List<Long> tagsId) throws ServiceException;


    /**
     * Get full news info
     *
     * @param newsId
     * @return fullNews
     * @throws ServiceException
     */
    FullNewsWorkItem getFullNews(Long newsId) throws ServiceException;

    /**
     * Add tags to news with given id
     *
     * @param newsId
     * @param tagsId
     * @throws ServiceException
     */
    void addTags(Long newsId, List<Long> tagsId) throws ServiceException;

    /**
     * Add author to news with given id
     *
     * @param newsId
     * @param authorId
     * @throws ServiceException
     */
    void addAuthor(Long newsId, Long authorId) throws ServiceException;


    /**
     * Add news
     *
     * @param news
     * @return id of added news
     * @throws ServiceException
     */
    Long addNews(News news) throws ServiceException;

    /**
     * Get tags of news with given id
     *
     * @param newsId
     * @return list of tags
     * @throws ServiceException
     */
    List<Tag> getTags(Long newsId) throws ServiceException;

    /**
     * Get comments of news with given id
     *
     * @param newsId
     * @return list of comments
     * @throws ServiceException
     */
    List<CommentWorkItem> getComments(Long newsId) throws ServiceException;

    /**
     * Get author of news with given id
     *
     * @param newsId
     * @return author
     * @throws ServiceException
     */
    Author getAuthor(Long newsId) throws ServiceException;

    /**
     * Get news with given id
     *
     * @param newsId
     * @return news
     * @throws ServiceException
     */
    News getNews(Long newsId) throws ServiceException;

    /**
     * Update news with given id
     *
     * @param news
     * @throws ServiceException
     */
    void updateNews(News news) throws ServiceException;

    /**
     * Delete all tags of news with given id
     *
     * @param newsId
     * @throws ServiceException
     */
    void deleteTags(Long newsId) throws ServiceException;

    /**
     * Update author of news with given id
     *
     * @param newsId
     * @param authorId
     * @throws ServiceException
     */
    void updateAuthor(Long newsId, Long authorId) throws ServiceException;

    /**
     * Count authors of news with given id
     *
     * @param newsId
     * @return
     * @throws ServiceException
     */
    int countAuthors(Long newsId) throws ServiceException;

    /**
     * Delete author of news with given id
     *
     * @param newsId
     * @throws ServiceException
     */
    void deleteAuthor(Long newsId) throws ServiceException;

    /**
     * Delete all comments of news with given id
     *
     * @param newsId
     * @throws ServiceException
     */
    void deleteComments(Long newsId) throws ServiceException;

    /**
     * Delete news with given id
     *
     * @param newsId
     * @throws ServiceException
     */
    void deleteNews(Long newsId) throws ServiceException;

    /**
     * Get tags' names of news with given id
     * @param newsId
     * @return list of tags' names
     * @throws ServiceException
     */
    List<String> getTagsNames(Long newsId) throws ServiceException;
    /**
     * Find news of defined amount by criteria
     *
     * @param criteria
     * @param from
     * @param amount
     * @return list of news
     * @throws ServiceException
     */
    List<NewsWorkItem> findByCriteria(SearchCriteriaVO criteria,int from, int amount) throws ServiceException;


    /**
     * Get name of author
     *
     * @param newsId
     * @return author name
     * @throws ServiceException
     */
    String getAuthorName(Long newsId) throws ServiceException;

    /**
     * Get amount of all news with criteria
     * @param criteria
     * @return amount of news
     * @throws ServiceException
     */
    int getCriteriaNewsAmount(SearchCriteriaVO criteria) throws ServiceException;

    /**
     * Get author's id of news with given id
     *
     * @param newsId
     * @return authorId
     * @throws ServiceException
     */
    Long getAuthorId(Long newsId) throws ServiceException;

    /**
     * Get tag's id of news with given id
     *
     * @param newsId
     * @return list of tag's id
     * @throws ServiceException
     */
    List<Long> getTagsId(Long newsId) throws ServiceException;
    /**
     * Get defined amount of news' id
     *
     * @param from
     * @param amount
     * @return list of news' id
     * @throws ServiceException
     */
    List<Long> getNewsIdList(SearchCriteriaVO criteria,int from, int amount) throws ServiceException;


}
