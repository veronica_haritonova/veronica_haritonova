package com.epam.newsmanagement.entity;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Veronica_Haritonova
 *         Class to store data of comment
 */
public class Comment implements Serializable {
    /**
     * Identificator of comment
     */
    private Long id;
    /**
     * Text of comment
     */

    @NotEmpty
    @Size(max = 100)
    private String text;

    /**
     * Date comment created
     */
    private Date creationDate;

    /**
     * Id of news
     */
    private Long newsId;

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -6868123704711283892L;

    public Comment() {

    }

    public Comment(Long id, Long newsId, String text, Date creationDate) {
        this.id = id;
        this.text = text;
        this.creationDate = creationDate;
        this.newsId = newsId;
    }

    public Comment(String text, Long newsId) {
        this.text = text;
        this.newsId = newsId;
    }

    public Comment(Long id, Long newsId, String text) {
        this.id = id;
        this.newsId = newsId;
        this.text = text;
    }

    public Long getNewsId() {
        return newsId;
    }

    public void setNewsId(Long newsId) {
        this.newsId = newsId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Comment)) return false;

        Comment comment = (Comment) o;

        if (id != null ? !id.equals(comment.id) : comment.id != null) return false;
        if (text != null ? !text.equals(comment.text) : comment.text != null) return false;
        if (creationDate != null ? !creationDate.equals(comment.creationDate) : comment.creationDate != null)
            return false;
        return !(newsId != null ? !newsId.equals(comment.newsId) : comment.newsId != null);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (text != null ? text.hashCode() : 0);
        result = 31 * result + (creationDate != null ? creationDate.hashCode() : 0);
        result = 31 * result + (newsId != null ? newsId.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "id=" + id +
                ", text='" + text + '\'' +
                ", creationDate=" + creationDate +
                ", newsId=" + newsId +
                '}';
    }
}
