package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;

import java.util.List;

/**
 * @author Veronica_Haritonova
 *         Interface for performing basic operations with tags in database
 */

public interface ITagDAO {
    /**
     * Insert tag into database
     *
     * @param tag
     * @return id of inserted tags
     * @throws DAOException
     */
    Long add(Tag tag) throws DAOException;

    /**
     * Update given tag in database
     *
     * @param tag
     * @throws DAOException
     */
    void update(Tag tag) throws DAOException;

    /**
     * Delete tag with given id from database
     *
     * @param tagId
     * @throws DAOException
     */
    void delete(Long tagId) throws DAOException;

    /**
     * Select all tags in database
     *
     * @return List of all tags in database
     * @throws DAOException
     */
    List<Tag> selectAll() throws DAOException;

    /**
     * Find tag with given id in database
     *
     * @param id
     * @return found tag or null if it doesn't exist
     * @throws DAOException
     */
    Tag get(Long id) throws DAOException;

    /**
     * Delete link between tag with given id and news
     *
     * @param tagId
     * @throws DAOException
     */
    void deleteTagInNews(Long tagId) throws DAOException;
}
