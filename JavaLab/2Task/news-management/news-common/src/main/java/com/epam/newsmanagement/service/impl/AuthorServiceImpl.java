package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.IAuthorDAO;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.IAuthorService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Veronica_Haritonova
 *         Service for work with authors
 */
@Service
@Transactional(rollbackFor = ServiceException.class)
public class AuthorServiceImpl implements IAuthorService {
    Logger logger = Logger.getLogger(AuthorServiceImpl.class);
    @Autowired
    private IAuthorDAO authorDAO;

    public AuthorServiceImpl(IAuthorDAO authorDAO) {
        this.authorDAO = authorDAO;
    }

    public AuthorServiceImpl() {
    }

    /**
     * Add author
     *
     * @param author to add
     * @throws ServiceException
     */
    public void addAuthor(Author author) throws ServiceException {
        try {
            authorDAO.add(author);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * Delete author with given id
     *
     * @param authorId
     * @throws ServiceException
     */
    public void deleteAuthor(Long authorId) throws ServiceException {
        try {
            authorDAO.setExpired(authorId);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * Update author
     *
     * @param author
     * @throws ServiceException
     */
    public void updateAuthor(Author author) throws ServiceException {
        try {
            authorDAO.update(author);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * Get all authors
     *
     * @return list of authors
     * @throws ServiceException
     */
    public List<Author> getAllAuthors() throws ServiceException {
        try {
            List<Author> authors = authorDAO.selectAll();
            return authors;
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * Get author with given id
     *
     * @param authorId
     * @return author
     * @throws ServiceException
     */
    public Author getAuthor(Long authorId) throws ServiceException {
        try {
            Author author = authorDAO.get(authorId);
            return author;
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * Make author expired
     *
     * @param authorId
     * @throws ServiceException
     */
    public void setExpired(Long authorId) throws ServiceException {
        try {
            authorDAO.setExpired(authorId);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }
}
