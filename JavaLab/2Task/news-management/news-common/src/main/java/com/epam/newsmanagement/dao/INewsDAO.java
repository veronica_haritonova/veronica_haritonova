package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.entity.*;
import com.epam.newsmanagement.exception.DAOException;

import java.util.List;

public interface INewsDAO {
    /**
     * Insert news in database
     *
     * @param news to add
     * @return id of inserted news
     * @throws DAOException
     */
    Long add(News news) throws DAOException;

    /**
     * Update given news in database
     *
     * @param news to update
     * @throws DAOException
     */
    void update(News news) throws DAOException;

    /**
     * Delete news with given id from database
     *
     * @param newsId
     * @throws DAOException
     */
    void delete(Long newsId) throws DAOException;

    /**
     * Get all news from database
     *
     * @return list of news
     * @throws DAOException
     */
    List<News> selectAll() throws DAOException;

    /**
     * Count amount of all news in database
     *
     * @return amount of all news
     * @throws DAOException
     */
    int countAll() throws DAOException;

    /**
     * Get news with given id
     *
     * @param newsId
     * @return news
     * @throws DAOException
     */
    News get(Long newsId) throws DAOException;

    /**
     * Add tags to news with given id
     *
     * @param newsId of news
     * @param tagsId to add
     * @throws DAOException
     */
    void addTags(Long newsId, List<Long> tagsId) throws DAOException;

    /**
     * Add author to news with given id
     *
     * @param newsId
     * @param authorId to add
     * @throws DAOException
     */
    void addAuthor(Long newsId, Long authorId) throws DAOException;

    /**
     * Get author of news with given id
     *
     * @param newsId
     * @return author
     * @throws DAOException
     */
    Author getAuthor(Long newsId) throws DAOException;

    /**
     * Delete author from news with given id
     *
     * @param newsId
     * @throws DAOException
     */
    void deleteAuthor(Long newsId) throws DAOException;

    /**
     * Update author of news with given id
     *
     * @param newsId
     * @param authorId - new author
     * @throws DAOException
     */
    void updateAuthor(Long newsId, Long authorId) throws DAOException;

    /**
     * Count authors of news with given id
     *
     * @param newsId
     * @return amount of authors
     * @throws DAOException
     */
    int countAuthors(Long newsId) throws DAOException;

    /**
     * Delete tag with given tagId from news with newsId
     *
     * @param newsId
     * @param tagId
     * @throws DAOException
     */
    void deleteTag(Long newsId, Long tagId) throws DAOException;

    /**
     * Delete all tags of news with given id
     *
     * @param newsId
     * @throws DAOException
     */
    void deleteAllTags(Long newsId) throws DAOException;

    /**
     * Get all news tags by given id
     *
     * @param newsId
     * @return list of tags
     * @throws DAOException
     */
    List<Tag> getTags(Long newsId) throws DAOException;

    /**
     * Get all comments of news with given id
     *
     * @param newsId
     * @return
     * @throws DAOException
     */
    List<CommentWorkItem> getComments(Long newsId) throws DAOException;

    /**
     * Delete all comments of news with given id
     *
     * @param newsId
     * @throws DAOException
     */
    void deleteAllComments(Long newsId) throws DAOException;

    /**
     * Get list of news with defined amount starting from defined number
     * @param from
     * @param amount
     * @return list of news
     * @throws DAOException
     */
    List<NewsWorkItem> getNewsList(int from, int amount) throws DAOException;

    /**
     * Get list of tags names of news with given id
     * @param newsId
     * @return list of tags' names
     * @throws DAOException
     */
    List<String> getTagsNames(Long newsId) throws DAOException;

    /**
     * Get name of news author
     *
     * @param newsId
     * @return author name
     * @throws DAOException
     */
    String getAuthorName(Long newsId) throws DAOException;

    /**
     * Get tagsId of news
     * @param newsId
     * @return list of tags' id
     * @throws DAOException
     */
    List<Long> getTagsId(Long newsId) throws DAOException;

    /**
     * Get id of author of news
     * @param newsId
     * @return author id
     * @throws DAOException
     */
    Long getAuthorId(Long newsId) throws DAOException;
}
