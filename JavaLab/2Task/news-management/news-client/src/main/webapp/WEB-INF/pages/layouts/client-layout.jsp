<%@ page language="java" contentType="text/html;charset=UTF-8"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<html>
  <head>
    <title><tiles:getAsString name="title"/></title>
    <link href="<c:url value='/resources/css/style.css'/>" rel="stylesheet" />
    <script type="text/javascript" src="<c:url value='/resources/js/script.js'/>"></script> </head>
  <body>
       <header>
          <tiles:insertAttribute name="header" />
        </header>
       <section>
       <div class="content">
          <tiles:insertAttribute name="content" />
          </div>
        </section>
      <footer>
          <tiles:insertAttribute name="footer" />
       </footer>
  </body>
</html>