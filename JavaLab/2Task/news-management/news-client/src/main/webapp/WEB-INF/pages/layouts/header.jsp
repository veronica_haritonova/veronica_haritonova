<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<div id="siteTitle">
    <h3><spring:message code="site.title"/></h3>
</div>
<div id="localeBlock">
    <a href="?locale=en_us">EN</a>
    <a href="?locale=ru">RU</a>
</div>

