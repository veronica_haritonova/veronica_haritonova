package com.epam.newsmanagement.tags;

import com.epam.newsmanagement.entity.Author;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.util.List;

/**
 * @author  Veronica_Haritonova
 *
 * Tag for displaying dropdown with authors where current author is selected
 */
public class AuthorDropdownTag extends SimpleTagSupport {
    private Long selectedAuthor;
    private List<Author> authorList;
    @Override
    public void doTag() throws JspException {
        try {
            PageContext pageContext = (PageContext) getJspContext();
            JspWriter out = pageContext.getOut();
            StringBuffer selectList = new StringBuffer();
            for (Author author : authorList) {
                selectList.append("<option value=").append(author.getId());
                 if(selectedAuthor != null) {
                    if (selectedAuthor.equals(author.getId())) {
                        selectList.append(" selected ");
                    }
                }
                selectList.append(">").append(author.getAuthorName()).append("</option>");
    }
            out.write(selectList.toString());
        } catch (IOException e) {
            throw new JspTagException(e.getMessage(), e);
        }
    }
    public void setSelectedAuthor(Long selectedAuthor) {
        this.selectedAuthor = selectedAuthor;
    }

    public void setAuthorList(List<Author> authorList) {
        this.authorList = authorList;
    }
}
