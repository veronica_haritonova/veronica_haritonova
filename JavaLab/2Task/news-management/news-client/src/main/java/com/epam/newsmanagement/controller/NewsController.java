package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.entity.FullNewsWorkItem;
import com.epam.newsmanagement.entity.SearchCriteriaVO;
import com.epam.newsmanagement.exception.ResourceNotFoundException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.manager.ConfigManager;
import com.epam.newsmanagement.service.IAuthorService;
import com.epam.newsmanagement.service.INewsService;
import com.epam.newsmanagement.service.ITagService;
import com.epam.newsmanagement.utils.NewsUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.List;


/**
 * @author Veronica_Haritonova
 *
 * Class for performing basic actions with news
 */
@Controller
@RequestMapping("news")
public class NewsController {
    @Autowired
    private INewsService newsService;
    @Autowired
    private ITagService tagService;
    @Autowired
    private IAuthorService authorService;
    @Autowired
    private NewsUtils newsUtils;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView getNewsList(@ModelAttribute("criteria") SearchCriteriaVO criteria,
                                    @RequestParam(value = "page", defaultValue = "1") Integer page,
                                    @RequestParam(value = "isNewCriteria", defaultValue = "false") Boolean isNewCriteria,
                                    HttpSession session)
            throws ServiceException {
        if (isNewCriteria) {
            session.setAttribute("searchCriteria", criteria);
        } else {
            criteria = (SearchCriteriaVO) session.getAttribute("searchCriteria");
        }
        session.setAttribute("newsIdList", newsUtils.getNewsIdList(criteria, page));
        int newsAmount = newsService.getCriteriaNewsAmount(criteria);
        session.setAttribute("newsAmount", newsAmount);
        session.setAttribute("page",page);
        ModelAndView model = formNewsListModel(criteria, page);
        return model;
    }

    @RequestMapping(value = "/{newsId}", method = RequestMethod.GET)
    public ModelAndView getNews(@PathVariable("newsId") Long newsId, @RequestParam(value = "page", required = false) Integer page,
                                HttpSession session)
            throws ResourceNotFoundException, ServiceException {
        FullNewsWorkItem news = newsService.getFullNews(newsId);
        if (news.getNews() == null) {
            throw new ResourceNotFoundException("No such news");
        }
        ModelAndView model = formNewsModel(newsId, page, session);
        model.addObject("news", news);
        return model;
    }

    @RequestMapping(value = "resetFilter", method = RequestMethod.GET)
    public String resetFilter(HttpSession session) throws ServiceException {
        session.removeAttribute("searchCriteria");
        String redirectUrl = "/news";
        return "redirect:" + redirectUrl;
    }

    private ModelAndView formNewsListModel(SearchCriteriaVO criteria, int page) throws ServiceException {
        ModelAndView model = new ModelAndView(ConfigManager.getProperty("page.news.list"));
        model.addObject("authorList", authorService.getAllAuthors());
        model.addObject("tagList", tagService.getAllTags());
        model.addObject("page", page);
        model.addObject("newsList", newsUtils.getNewsList(criteria,page));
        return model;
    }

    private ModelAndView formNewsModel(Long newsId, Integer page, HttpSession session)
            throws ServiceException {
        ModelAndView model = new ModelAndView(ConfigManager.getProperty("page.news"));
        if(page == null) {
            page = (Integer)session.getAttribute("page");

        } else {
            session.setAttribute("page",page);
        }
        List<Long> newsIdList = (List<Long>) session.getAttribute("newsIdList");
        SearchCriteriaVO criteria = (SearchCriteriaVO) session.getAttribute("searchCriteria");
        int newsIndexInList = newsIdList.indexOf(newsId);
        int listSize = newsIdList.size();
        newsUtils.addPreviousNewsIdList(newsIdList,newsIndexInList,criteria,page);
        newsUtils.addNextNewsIdList(newsIdList,newsIndexInList,listSize,criteria,page);
        if (newsUtils.isLastNewsOnPage(newsIndexInList)) {
            model.addObject("nextPage", page + 1);
        } else {
            model.addObject("nextPage", page);
        }
        if (newsUtils.isFirstNewsOnPage(newsIndexInList)) {
            model.addObject("previousPage", page - 1);
        } else {
            model.addObject("previousPage", page);
        }
        newsIndexInList = newsIdList.indexOf(newsId);
        model.addObject("newsIndex", newsIndexInList);
        return model;
    }
}
