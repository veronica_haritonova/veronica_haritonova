package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.ICommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author Veronica_Haritonova
 *
 * Class for performing basic actions with news comments
 */
@Controller
@RequestMapping("comments")
public class CommentController {
    @Autowired
    private ICommentService commentService;

    @RequestMapping(value = "add", method = RequestMethod.POST)
    public String addComment(@ModelAttribute("comment") Comment comment) throws ServiceException {
        commentService.addComment(comment);
        String redirectUrl = "/news/" + comment.getNewsId();
        return "redirect:" + redirectUrl;
    }

}
