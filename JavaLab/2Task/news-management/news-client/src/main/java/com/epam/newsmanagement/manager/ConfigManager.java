package com.epam.newsmanagement.manager;

import java.util.ResourceBundle;

/**
 * @author  Veronica_Haritonova
 *
 * Class to provide names of jsp files
 */
public class ConfigManager {
    private static ResourceBundle bundle = ResourceBundle.getBundle("config");
    private ConfigManager() {}

    /**
     * Get name of jsp with given key
     *
     * @param key
     * @return name of jsp
     */
    public static String getProperty(String key) {
        return bundle.getString(key);
    }
}
