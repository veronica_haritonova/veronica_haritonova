package com.epam.newsmanagement.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Veronica_Haritonova
 *
 * Excepion for situation when there is no such data in database
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends Exception {
    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -3589894635248361196L;

    public ResourceNotFoundException(String message) {
        super(message);
    }

    public ResourceNotFoundException(String message, Throwable e) {
        super(message, e);
    }

    public ResourceNotFoundException(Throwable e) {
        super(e);
    }
}
