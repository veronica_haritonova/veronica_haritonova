package com.epam.newsmanagement.tags;

import com.epam.newsmanagement.entity.Tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.util.List;

/**
 * @author Veronica_Haritonova
 *
 * Tag for displaying dropdown with tags where current tags are selected
 */
public class TagsDropdownTag extends SimpleTagSupport {

    private List<Long> selectedTags;
    private List<Tag> tagList;

    @Override
    public void doTag() throws JspException {
        try {
            PageContext pageContext = (PageContext) getJspContext();
            JspWriter out = pageContext.getOut();
            StringBuffer checkboxesList = new StringBuffer();
            for (Tag tag : tagList) {
                checkboxesList.append("<label for=tag").append(tag.getId()).append("><input type=checkbox id=tag")
                        .append(tag.getId()).append(" name=tagsId value=").append(tag.getId()).append(" ");

                if(selectedTags != null) {
                    if (selectedTags.contains(tag.getId())) {
                        checkboxesList.append("checked=checked ");
                    }
                }

                checkboxesList.append(">").append(tag.getTagName()).append("</label>");
            }
            out.write(checkboxesList.toString());
        } catch (IOException e) {
            throw new JspTagException(e.getMessage(), e);
        }
    }

    public void setSelectedTags(List<Long> selectedTags) {
        this.selectedTags = selectedTags;
    }

    public void setTagList(List<Tag> tagList) {
        this.tagList = tagList;
    }
}
