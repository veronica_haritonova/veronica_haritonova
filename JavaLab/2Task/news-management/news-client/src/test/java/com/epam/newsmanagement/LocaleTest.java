package com.epam.newsmanagement;

/**
 * Created by Veronica_Haritonova on 7/8/2015.
 */

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.util.Locale;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

/**
 * @author  Veronica_Haritonova
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration("file:src/main/webapp/WEB-INF/spring/spring-dispatcher-servlet.xml")

public class LocaleTest {

private MockMvc mockMvc;
    @Autowired
    private ReloadableResourceBundleMessageSource messageSource;
@Autowired
protected WebApplicationContext wac;

    @Before
    public void setup() {
        this.mockMvc = webAppContextSetup(this.wac).build();
    }

    @Test
    public void getAllNews() throws Exception {
        mockMvc.perform(get("/news?locale=en_us"));
        mockMvc.perform(get("/news"))
                .andExpect(status().isOk())
                .andExpect(view().name("newsList"));
        assertEquals("Not correct value", "<<", messageSource.getMessage("page.first",null, Locale.US));
    }
}
