package com.epam.newsmanagement;



import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;


import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

/**
 * @author  Veronica_Haritonova
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration("file:src/main/webapp/WEB-INF/spring/spring-dispatcher-servlet.xml")
public class NewsControllerTest {
    private MockMvc mockMvc;

    @Autowired
    protected WebApplicationContext wac;

    @Before
    public void setup() {
        this.mockMvc = webAppContextSetup(this.wac).build();
    }

    @Test
    public void getAllNews() throws Exception {
        mockMvc.perform(get("/news"))
                .andExpect(status().isOk())
                .andExpect(view().name("newsList"));
    }
    @Test
    public void getAllPageNews() throws Exception {
        mockMvc.perform(get("/news").param("page","2"))
                .andExpect(status().isOk())
                .andExpect(view().name("newsList"));
    }
    @Test
    public void getAllPage3News() throws Exception {
        mockMvc.perform(get("/news?page=3"))
                .andExpect(status().isOk())
                .andExpect(view().name("newsList"));
    }
    @Test
    public void getNewsByAuthor() throws Exception {
        Long authorId = 2L;
        mockMvc.perform(get("/news").param("authorId", authorId.toString()).param("page", "1"))
                .andExpect(status().isOk())
                .andExpect(view().name("newsList"));
     }
    @Test
    public void getNewsByTags() throws Exception {
        mockMvc.perform(get("/news").param("tagsId", "2", "3").param("page", "1"))
                .andExpect(status().isOk())
                .andExpect(view().name("newsList"));
     }
    @Test
    public void getNewsByTagsAndAuthor() throws Exception {
        mockMvc.perform(get("/news").param("tagsId", "2", "3")
                .param("authorId", "3").param("page", "1"))
                .andExpect(status().isOk())
                .andExpect(view().name("newsList"));
            }
 }
