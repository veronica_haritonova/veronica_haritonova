package com.epam.newsmanagement;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.Filter;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

/**
 * @author  Veronica_Haritonova
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration("file:src/main/webapp/WEB-INF/spring/spring-dispatcher-servlet.xml")
@WithMockUser(roles="ADMIN")
public class NewsControllerTest {
    private MockMvc mockMvc;

    @Autowired
    protected WebApplicationContext wac;
    @Autowired
    private Filter springSecurityFilterChain;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(wac)
                .defaultRequest(get("/").with(user("user").roles("ADMIN")))
                .addFilters(springSecurityFilterChain)
                .build();
    }

    @Test
    public void getAllNews() throws Exception {
        mockMvc.perform(get("/news"))
                .andExpect(status().isOk())
                .andExpect(view().name("newsList"));
    }


    @Test
    public void getAllPageNews() throws Exception {
        mockMvc.perform(get("/news").param("page","2"))
                .andExpect(status().isOk())
                .andExpect(view().name("newsList"));
    }
    @Test
    public void getAllPage3News() throws Exception {
        mockMvc.perform(get("/news?page=3"))
                .andExpect(status().isOk())
                .andExpect(view().name("newsList"));
    }
    @Test
    public void getNewsByAuthor() throws Exception {
        Long authorId = 2L;
        mockMvc.perform(get("/news").param("authorId", authorId.toString()).param("page", "1"))
                .andExpect(status().isOk())
                .andExpect(view().name("newsList"));
     }
    @Test
    public void getNewsByTags() throws Exception {
        mockMvc.perform(get("/news").param("tagsId", "2", "3").param("page", "1"))
                .andExpect(status().isOk())
                .andExpect(view().name("newsList"));
     }
    @Test
    public void getNewsByTagsAndAuthor() throws Exception {
        mockMvc.perform(get("/news").param("tagsId", "2", "3")
                .param("authorId", "3").param("page", "1"))
                .andExpect(status().isOk())
                .andExpect(view().name("newsList"));
            }

    @Test
    public void addNews() throws Exception {
        mockMvc.perform(post("news/add").param("title", "title")
                .param("shortText","shortText").param("fullText","fulltext")
                .param("creationDate", "12/12/2009")
                .param("authorId","2").with(csrf()));
    }
}
