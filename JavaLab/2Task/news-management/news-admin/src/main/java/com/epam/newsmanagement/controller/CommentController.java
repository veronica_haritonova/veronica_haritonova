package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.ICommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

/**
 * @author Veronica_Haritonova
 */
@Controller
@RequestMapping("comments")
@Secured("ROLE_ADMIN")
public class CommentController {
    @Autowired
    private ICommentService commentService;

    @RequestMapping(value = "add", method = RequestMethod.POST)
    public String addComment(@Valid @ModelAttribute("comment") Comment comment, BindingResult bindingResult)
            throws ServiceException {
        if(!bindingResult.hasErrors()) {
            commentService.addComment(comment);
        }
        String redirectUrl = "/news/" + comment.getNewsId();
        return "redirect:" + redirectUrl;
    }
    @RequestMapping(value = "delete", method = RequestMethod.POST)
    public String deleteComment(@RequestParam("commentId")Long commentId, @RequestParam("newsId")Long newsId)
            throws ServiceException {
        commentService.deleteComment(commentId);
        String redirectUrl = "/news/" + newsId;
        return "redirect:" + redirectUrl;
    }
}
