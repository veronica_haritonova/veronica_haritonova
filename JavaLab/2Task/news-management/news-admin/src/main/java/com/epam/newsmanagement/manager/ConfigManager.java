package com.epam.newsmanagement.manager;

import java.util.ResourceBundle;

/**
 * @author Veronica_Haritonova
 */
public class ConfigManager {
    private static ResourceBundle bundle = ResourceBundle.getBundle("config");
    private ConfigManager() {}
    public static String getProperty(String key) {
        return bundle.getString(key);
    }
}
