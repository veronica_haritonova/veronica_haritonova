package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.ResourceNotFoundException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.manager.ConfigManager;
import com.epam.newsmanagement.service.IAuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.List;

/**
 * @author  Veronica_Haritonova
 */
@Controller
@RequestMapping("authors")
@Secured("ROLE_ADMIN")
public class AuthorController {
    @Autowired
    private IAuthorService authorService;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView getAuthors() throws ServiceException, ResourceNotFoundException {
        return formModel();
    }

    @RequestMapping(value = "add", method = RequestMethod.POST)
    public ModelAndView addAuthor(@Valid @ModelAttribute("author")Author author, BindingResult bindingResult) throws ServiceException {
        if(!bindingResult.hasErrors()) {
            authorService.addAuthor(author);
        }
        return formModel();
    }
    @RequestMapping(value = "edit", method = RequestMethod.POST)
    public ModelAndView editAuthor(@Valid @ModelAttribute("author")Author author,BindingResult bindingResult) throws ServiceException {
        if(!bindingResult.hasErrors()) {
            authorService.updateAuthor(author);
        }
        return formModel();
    }
    @RequestMapping(value = "expire", method = RequestMethod.POST)
    public ModelAndView setExpired(@RequestParam("authorId")Long authorId) throws ServiceException, ResourceNotFoundException {
         authorService.setExpired(authorId);
         return formModel();
    }
    @RequestMapping(value = "cancel", method = RequestMethod.GET)
    public String cancelEdit() throws ServiceException {
        String redirectUrl = "/authors";
        return "redirect:" + redirectUrl;
    }
    private ModelAndView formModel() throws ServiceException {
        ModelAndView model = new ModelAndView(ConfigManager.getProperty("page.authors"));
        List<Author> authorList = authorService.getAllAuthors();
        model.addObject("authorList", authorList);
        return model;
    }
}
