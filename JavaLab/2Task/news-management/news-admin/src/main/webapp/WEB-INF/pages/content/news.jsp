<%@ page language="java" contentType="text/html;charset=UTF-8"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="ctg" uri="customtags" %>
<a href = "${ pageContext.request.contextPath }/news?page=${page}">
    <spring:message code="back" var="back"/>
    ${fn:toUpperCase(back)}
</a>
<div id = "newsBlock">
     <h3 class="newsTitle">${news.news.title}</h3>
     <span class="authorName">
           <spring:message code="news.author" arguments="${news.authorName}"/>
     </span>
     <span class="creationDate">
           <ctg:formatDate date="${news.news.creationDate}"/>
     </span>
     <p id="fullNewsText">${news.news.fullText}</p>
     <table id = "comments">
         <c:forEach var="comment" items = "${news.comments}">
            <tr >
                <td>
                    <span class="commentCreationDate">
                        <ctg:formatDate date="${comment.creationDate}"/>
                    </span>
                </td>
            </tr>
            <tr class="commentText">
                 <td>
                    <span >${comment.text}</span>
                    <form action="${ pageContext.request.contextPath }/comments/delete" method="post"  class="inlineBlock floatRight">
                            <input type = "hidden" name = "commentId" value = "${comment.id}"/>
                            <input type = "hidden" name = "newsId" value = "${news.news.id}"/>
                            <input type="hidden" name="${_csrf.parameterName}"
                                            value="${_csrf.token}" />
                            <input type="submit" class="deleteCommentButton" value = "X"/>

                    </form>
                </td>
            </tr>
        </c:forEach>
         <tr>
                <td>
                    <form id="addCommentForm" action = "${ pageContext.request.contextPath }/comments/add" method = "POST">
                        <input type = "hidden" name = "newsId" value = "${news.news.id}"/>
                        <input type="hidden" name="${_csrf.parameterName}"
                                        value="${_csrf.token}" />
                        <textarea rows="10" cols="55" name="text" required maxlength="100"></textarea>
                        <input type="submit" class="button" id="postCommentButton" name = "addCommentButton" value = "<spring:message code="comment.post"/>"/>
                    </form>
               </td>
         </tr>
    </table>
</div>
<div id="newsLinks">
    <c:if test="${not empty newsIdList[newsIndex - 1]}">
        <a id="previousNewsLink" href="${ pageContext.request.contextPath }/news/${newsIdList[newsIndex - 1]}?page=${previousPage}">
            <spring:message code="previous" var="previous"/>
            ${fn:toUpperCase(previous)}
        </a>
    </c:if>
    <c:if test="${not empty newsIdList[newsIndex + 1]}">
            <a id="nextNewsLink" href="${ pageContext.request.contextPath }/news/${newsIdList[newsIndex + 1]}?page=${nextPage}">
                <spring:message code="next" var="next"/>
                ${fn:toUpperCase(next)}
            </a>
        </c:if>
</div>