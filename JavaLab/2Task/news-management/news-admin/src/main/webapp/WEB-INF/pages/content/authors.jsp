<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div id="authorsBlock">
    <form:errors path="author.authorName"/>
    <table id="authorList">
        <c:forEach var="author" items = "${authorList}">
            <tr>
            <td>
                <form action="${ pageContext.request.contextPath }/authors/edit" method="post" class="inlineBlock">
                    <label for="authorNameField${author.id}"><spring:message code="author"/>:</label>
                    <input type="hidden" name="${_csrf.parameterName}"
                                    value="${_csrf.token}" />
                    <input id="authorNameField${author.id}" type="text" name="authorName" value="${author.authorName}" disabled required maxlength="30" />

                    <input type="hidden" name="id" value="${author.id}" />
                    <input class="editAuthorLink hidden" id="updateAuthorButton${author.id}" type="submit" value="<spring:message code="update"/>"/>
                </form>
                <c:if test="${empty author.expired}">
                    <form action="${ pageContext.request.contextPath }/authors/expire" method="post" class="inlineBlock">
                        <input type="hidden" name="${_csrf.parameterName}"
                                                        value="${_csrf.token}" />
                        <input type="hidden" name="authorId" value="${author.id}"/>
                        <input class="editAuthorLink hidden" id="expireAuthorButton${author.id}" type="submit" value="<spring:message code="author.expired"/>"/>
                        <input type="hidden" name="${_csrf.parameterName}"
                                                        value="${_csrf.token}" />
                    </form>
                </c:if>
                <form action="${ pageContext.request.contextPath }/authors/cancel" method="get" class="inlineBlock">
                    <input type="hidden" name="authorId" value="${author.id}"/>
                    <input class="editAuthorLink hidden" id="cancelEditAuthorButton${author.id}" type="submit" value="<spring:message code="cancel"/>"/>

                </form>
                <button onclick="displayEditAuthorLinks(${author.id})" class="displayAuthorLinksButton" id="displayAuthorLinksButton${author.id}"><spring:message code="edit"/></button>
            </td>
            </tr>
        </c:forEach>
    </table>
    <div id="addAuthor">
        <form action="${ pageContext.request.contextPath }/authors/add" method="post">
            <table>
                <tr>
                    <td>
                         <label for="authorName">
                             <spring:message code="author.add"/>:
                         </label>
                         <input type="text" name="authorName" id="authorName" required maxlength="30"/>
                    </td>
                    <td>
                         <input type="hidden" name="${_csrf.parameterName}"
                                            value="${_csrf.token}" />
                         <input type="submit" value="<spring:message code="save"/>"/>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>