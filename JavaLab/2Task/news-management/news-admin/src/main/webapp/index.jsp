<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<html>
<body>
    <sec:authorize access="hasRole('ROLE_ADMIN')">
        <c:redirect url="/news"/>
    </sec:authorize>
    <c:redirect url="/login"/>
</body>
</html>