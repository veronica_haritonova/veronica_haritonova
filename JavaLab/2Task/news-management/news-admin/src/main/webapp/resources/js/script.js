var expanded = false;
    function showCheckboxes() {
        var checkboxes = document.getElementById("checkboxes");
        if (!expanded) {
            checkboxes.style.display = "block";
            expanded = true;
        } else {
            checkboxes.style.display = "none";
            expanded = false;
        }
    }
    function displayEditAuthorLinks(authorId) {
    document.getElementById("authorNameField" + authorId).disabled = false;
    document.getElementById("updateAuthorButton" + authorId).classList.remove("hidden");
    if(document.getElementById("expireAuthorButton" + authorId) != null) {
        document.getElementById("expireAuthorButton" + authorId).classList.remove("hidden");
    }
    document.getElementById("cancelEditAuthorButton" + authorId).classList.remove("hidden");
    document.getElementById("displayAuthorLinksButton" + authorId).classList.add("hidden");
    }
function displayEditTagLinks(tagId) {
    document.getElementById("tagNameField" + tagId).disabled = false;
    document.getElementById("updateTagButton" + tagId).classList.remove("hidden");
    document.getElementById("deleteTagButton" + tagId).classList.remove("hidden");
    document.getElementById("cancelEditTagButton" + tagId).classList.remove("hidden");
    document.getElementById("displayTagLinksButton" + tagId).classList.add("hidden");
    }