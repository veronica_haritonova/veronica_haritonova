package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ResourceNotFoundException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.manager.ConfigManager;
import com.epam.newsmanagement.service.ITagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.List;

/**
 * @tag  Veronica_Haritonova.
 */
@Controller
@RequestMapping("tags")
@Secured("ROLE_ADMIN")
public class TagController {

    @Autowired
    private ITagService tagService;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView getTags() throws ServiceException {
        ModelAndView model = new ModelAndView(ConfigManager.getProperty("page.tags"));
        List<Tag> tagList = tagService.getAllTags();
        model.addObject("tagList", tagList);
        return model;
    }

    @RequestMapping(value = "add", method = RequestMethod.POST)
    public ModelAndView addTag(@Valid @ModelAttribute("tag")Tag tag,BindingResult bindingResult)
            throws ServiceException {
        if(!bindingResult.hasErrors()) {
            tagService.addTag(tag);
        }
        ModelAndView model = new ModelAndView(ConfigManager.getProperty("page.tags"));
        List<Tag> tagList = tagService.getAllTags();
        model.addObject("tagList", tagList);
        return model;
    }

    @RequestMapping(value = "edit", method = RequestMethod.POST)
    public ModelAndView editTag(@Valid @ModelAttribute("tag")Tag tag,BindingResult bindingResult)
            throws ServiceException {
        if(!bindingResult.hasErrors()) {
            tagService.updateTag(tag);
        }
        ModelAndView model = new ModelAndView(ConfigManager.getProperty("page.tags"));
        List<Tag> tagList = tagService.getAllTags();
        model.addObject("tagList", tagList);
        return model;
    }
    @RequestMapping(value = "delete", method = RequestMethod.POST)
    public String deleteTag(@RequestParam("tagId")Long tagId)
            throws ServiceException, ResourceNotFoundException {
        tagService.deleteTag(tagId);
        String redirectUrl = "/tags";
        return "redirect:" + redirectUrl;
    }
    @RequestMapping(value = "cancel", method = RequestMethod.GET)
    public String cancelEdit() throws ServiceException {
        String redirectUrl = "/tags";
        return "redirect:" + redirectUrl;
    }
}
