package com.epam.newsmanagement.viewmodel;

import com.epam.newsmanagement.entity.News;


import javax.validation.constraints.NotNull;
import javax.validation.Valid;
import java.util.List;

/**
 * @author  Veronica_Haritonova
 */
public class AddNewsViewModel {
	@Valid
    private News news;

    @NotNull
    private Long authorId;

    private List<Long> tagsId;

    public AddNewsViewModel() {
    }

    public News getNews() {
        return news;
    }

    public void setNews(News news) {
        this.news = news;
    }

    public List<Long> getTagsId() {
        return tagsId;
    }

    public void setTagsId(List<Long> tagsId) {
        this.tagsId = tagsId;
    }

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    @Override
    public String toString() {
        return "AddNewsViewModel{" +
                "news=" + news +
                ", authorId=" + authorId +
                ", tagsId=" + tagsId +
                '}';
    }
}
