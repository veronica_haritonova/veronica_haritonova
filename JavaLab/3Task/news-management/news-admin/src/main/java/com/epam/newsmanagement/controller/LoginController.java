package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.manager.ConfigManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.Locale;

/**
 * @author  Veronica_Haritonova
 */

@Controller
@RequestMapping("login")
public class LoginController {
    @Autowired
    private ReloadableResourceBundleMessageSource messageSource;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView loginForm(@RequestParam(value = "error", required = false) String error,
                                  @RequestParam(value = "accessDenied", required = false) String accessDenied,
                                  Locale locale) {
        ModelAndView model = new ModelAndView(ConfigManager.getProperty("page.login"));

        if (error != null) {
            model.addObject("loginError", messageSource.getMessage("login.error",null,locale));
        }
        if(accessDenied != null) {
            model.addObject("accessError",  messageSource.getMessage("access.error",null,locale));
        }
        return model;
    }
}
