package com.epam.newsmanagement.utils;

import com.epam.newsmanagement.dao.IAuthorDAO;
import com.epam.newsmanagement.entity.*;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.IAuthorService;
import com.epam.newsmanagement.service.INewsService;
import com.epam.newsmanagement.service.ITagService;
import com.epam.newsmanagement.viewmodel.AddNewsViewModel;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * @author  Veronica_Haritonova
 */
public class NewsUtils {
    @Autowired
    private INewsService newsService;
    @Autowired
    ITagService tagService;
    @Autowired
    IAuthorService authorService;

    public static final int NEWS_AMOUNT_ON_PAGE = 5;

    public List<Long> getNewsIdList(SearchCriteriaVO criteria, int page) throws ServiceException {
        return newsService.getNewsIdList(criteria, ((page - 1) * NEWS_AMOUNT_ON_PAGE) + 1, NEWS_AMOUNT_ON_PAGE);
    }

    public boolean isFirstNewsOnPage(int newsIndexInList) {
        return (newsIndexInList + 1) % NEWS_AMOUNT_ON_PAGE == 1;
    }

    public boolean isLastNewsOnPage(int newsIndexInList) {
        return (newsIndexInList + 1) % NEWS_AMOUNT_ON_PAGE == 0;
    }

    public List<NewsWorkItem> getNewsList(SearchCriteriaVO criteria, int page) throws ServiceException {
        return newsService.findByCriteria(criteria, ((page - 1) * NEWS_AMOUNT_ON_PAGE) + 1, NEWS_AMOUNT_ON_PAGE);
    }

    public void addPreviousNewsIdList(List<Long> newsIdList, int newsIndexInList,SearchCriteriaVO criteria, int page)
            throws ServiceException {
        if (newsIndexInList == 0 && page > 1) {
            int previousPage = page - 1;
            List<Long> previousNewsIdList = getNewsIdList(criteria, previousPage);
            newsIdList.addAll(0, previousNewsIdList);
        }
    }

    public void addNextNewsIdList(List<Long> newsIdList, int newsIndexInList, int listSize, SearchCriteriaVO criteria, int page)
            throws ServiceException {
        if (newsIndexInList == listSize - 1) {
            int nextPage = page + 1;
            List<Long> nextNewsIdList = getNewsIdList(criteria,nextPage);
            newsIdList.addAll(nextNewsIdList);
        }
    }


    public News transformNewsViewModel(AddNewsViewModel newsViewModel) throws ServiceException {
        News news = newsViewModel.getNews();
        List<Long> tagIdList = newsViewModel.getTagsId();
        if(tagIdList != null) {
            List<Tag> tagList = new ArrayList<Tag>(tagIdList.size());
            for (Long tagId : tagIdList) {
                tagList.add(tagService.getTag(tagId));
            }
            news.setTagList(tagList);
        }
        Long authorId = newsViewModel.getAuthorId();
        if(authorId != null) {
            news.setAuthor(authorService.getAuthor(authorId));
        }
        if(news.getModificationDate() == null) {
            news.setModificationDate(news.getCreationDate());
        }
        return news;
    }
    public AddNewsViewModel transformNews(News news) throws ServiceException {
        AddNewsViewModel newsViewModel = new AddNewsViewModel();
        List<Tag> tagList = news.getTagList();
        if(tagList != null) {
            List<Long> tagIdList = new ArrayList<Long>(tagList.size());
            for (Tag tag : tagList) {
                tagIdList.add(tag.getId());
            }
            newsViewModel.setTagsId(tagIdList);
        }
        Author author = news.getAuthor();
        if(author != null) {
            newsViewModel.setAuthorId(author.getId());
        }
        newsViewModel.setNews(news);
        return newsViewModel;
    }
}
