package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchCriteriaVO;
import com.epam.newsmanagement.exception.ResourceNotFoundException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.manager.ConfigManager;
import com.epam.newsmanagement.service.IAuthorService;
import com.epam.newsmanagement.service.INewsService;
import com.epam.newsmanagement.service.ITagService;
import com.epam.newsmanagement.utils.NewsUtils;
import com.epam.newsmanagement.viewmodel.AddNewsViewModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.orm.hibernate4.HibernateOptimisticLockingFailureException;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.persistence.OptimisticLockException;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * @author Veronica_Haritonova
 */
@Controller
@RequestMapping("news")
@Secured("ROLE_ADMIN")
public class NewsController {
    @Autowired
    private INewsService newsService;
    @Autowired
    private ITagService tagService;
    @Autowired
    private IAuthorService authorService;
    @Autowired
    private NewsUtils newsUtils;
    @Autowired
    private ReloadableResourceBundleMessageSource messageSource;

    @ModelAttribute
    public void setCommonAttributes(Model model) throws ServiceException {
        model.addAttribute("authorList", authorService.getAllAuthors());
        model.addAttribute("tagList", tagService.getAllTags());

    }

    @InitBinder
    public final void initBinder(WebDataBinder binder, Locale locale) {
        ResourceBundle bundle = ResourceBundle.getBundle("messages",locale);
        SimpleDateFormat dateFormat = new SimpleDateFormat(bundle.getString("date_format"));
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView getNewsList(@ModelAttribute("criteria") SearchCriteriaVO criteria,
                                    @RequestParam(value = "page", defaultValue = "1") Integer page,
                                    @RequestParam(value = "isNewCriteria", defaultValue = "false") Boolean isNewCriteria,
                                    HttpSession session) throws ServiceException {
        if (isNewCriteria) {
            session.setAttribute("searchCriteria", criteria);
        } else {
            criteria = (SearchCriteriaVO) session.getAttribute("searchCriteria");
        }
        session.setAttribute("newsIdList", newsUtils.getNewsIdList(criteria,page));
        Long newsAmount = newsService.getCriteriaNewsAmount(criteria);
        session.setAttribute("newsAmount", newsAmount);
        session.setAttribute("page",page);
        ModelAndView model = formNewsListModel(criteria, page);
        return model;
    }

    @RequestMapping(value = "/{newsId}", method = RequestMethod.GET)
    public ModelAndView getNews(@PathVariable("newsId") Long newsId,@RequestParam(value = "page",required = false) Integer page,
                                HttpSession session)
            throws ResourceNotFoundException, ServiceException {
        News news = newsService.getNews(newsId);
        if (news == null) {
            throw new ResourceNotFoundException("No such news");
        }
        ModelAndView model = formNewsModel(newsId, page, session);
        model.addObject("news", news);
        return model;
    }

    @RequestMapping(value = "resetFilter", method = RequestMethod.GET)
    public String resetFilter(HttpSession session)
            throws ServiceException {
        session.removeAttribute("searchCriteria");
        String redirectUrl = "/news";
        return "redirect:" + redirectUrl;
    }

    @RequestMapping(value = "add", method = RequestMethod.GET)
    public ModelAndView prepareAddNews() throws ServiceException {
        ModelAndView model = new ModelAndView(ConfigManager.getProperty("page.news.add"));
        model.addObject("creationDate", Calendar.getInstance().getTime());
        System.out.println(Calendar.getInstance().getTime());
        return model;
    }

    @RequestMapping(value = "add", method = RequestMethod.POST)
    public String addNews(@Valid @ModelAttribute("newsModel")AddNewsViewModel newsModel, BindingResult bindingResult)
            throws ServiceException {

       if(bindingResult.hasErrors()) {
            if(newsModel.getNews().getCreationDate() == null) {
               newsModel.getNews().setCreationDate(Calendar.getInstance().getTime());
            }
            return ConfigManager.getProperty("page.news.add");
        }
        Long newsId = newsService.addNews(newsUtils.transformNewsViewModel(newsModel));
        String redirectUrl = "/news/" + newsId;
        return "redirect:" + redirectUrl;
    }

    @RequestMapping(value = "edit/{newsId}", method = RequestMethod.GET)
    public ModelAndView prepareEditNews(@PathVariable("newsId") Long newsId, HttpSession session)
            throws ServiceException, ResourceNotFoundException {
        ModelAndView model = new ModelAndView(ConfigManager.getProperty("page.news.update"));
        News news = newsService.getNews(newsId);
        if(news == null) {
            throw new ResourceNotFoundException("No such news");
        }
        news.setModificationDate(Calendar.getInstance().getTime());
        model.addObject("newsModel", newsUtils.transformNews(news));
        return model;
    }

    @RequestMapping(value = "edit", method = RequestMethod.POST)
    public String editNews(@Valid @ModelAttribute("newsModel")AddNewsViewModel newsModel, BindingResult bindingResult, Model model)
            throws ServiceException {
        try {
            if (bindingResult.hasErrors()) {
                if (newsModel.getNews().getModificationDate() == null) {
                    newsModel.getNews().setModificationDate(Calendar.getInstance().getTime());
                }
                return ConfigManager.getProperty("page.news.update");
            }
            newsService.updateNews(newsUtils.transformNewsViewModel(newsModel));
            String redirectUrl = "/news/" + newsModel.getNews().getId();
            return "redirect:" + redirectUrl;
        } catch (HibernateOptimisticLockingFailureException | OptimisticLockException e) {
            model.addAttribute("isLocked",true);
            return ConfigManager.getProperty("page.news.update");
        }
    }

    @RequestMapping(value = "delete", method = RequestMethod.POST)
    public String deleteSelectedNews(@RequestParam("newsIdList")List<Long> newsIdList, @RequestParam("page")Integer page)
            throws ServiceException {
        for (Long newsId : newsIdList) {
            newsService.deleteNews(newsId);
        }
        String redirectUrl = "/news?page=" + page;
        return "redirect:" + redirectUrl;
    }

    private ModelAndView formNewsListModel(SearchCriteriaVO criteria, int page)
            throws ServiceException {
        ModelAndView model = new ModelAndView(ConfigManager.getProperty("page.news.list"));
        model.addObject("authorList", authorService.getAllAuthors());
        model.addObject("tagList", tagService.getAllTags());
        model.addObject("newsList", newsUtils.getNewsList(criteria, page));
        return model;
    }

    private ModelAndView formNewsModel(Long newsId, Integer page, HttpSession session)
            throws ServiceException {
        ModelAndView model = new ModelAndView(ConfigManager.getProperty("page.news"));
        if(page == null) {
            page = (Integer)session.getAttribute("page");
            if(page == null) {
                return model;
            }
        } else {
            session.setAttribute("page",page);
        }
        List<Long> newsIdList = (List<Long>) session.getAttribute("newsIdList");
        SearchCriteriaVO criteria = (SearchCriteriaVO) session.getAttribute("searchCriteria");
        int newsIndexInList = newsIdList.indexOf(newsId);
        int listSize = newsIdList.size();
        newsUtils.addPreviousNewsIdList(newsIdList,newsIndexInList,criteria,page);
        newsUtils.addNextNewsIdList(newsIdList,newsIndexInList,listSize,criteria,page);
        if (newsUtils.isLastNewsOnPage(newsIndexInList)) {
            model.addObject("nextPage", page + 1);
        } else {
            model.addObject("nextPage", page);
        }
        if (newsUtils.isFirstNewsOnPage(newsIndexInList)) {
            model.addObject("previousPage", page - 1);
        } else {
            model.addObject("previousPage", page);
        }
        model.addObject("newsIndex", newsIdList.indexOf(newsId));
        return model;
    }
}
