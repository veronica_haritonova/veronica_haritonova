<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div id="tagsBlock">
    <form:errors path="tag.tagName"/>
    <table>
        <c:forEach var="tag" items = "${tagList}">
            <tr>
                <td>
                    <form action="${ pageContext.request.contextPath }/tags/edit" method="post" class="inlineBlock">
                        <label for="tagNameField${tag.id}">
                            <spring:message code="tag"/>:
                        </label>
                        <input id="tagNameField${tag.id}" type="text" name="tagName" value="<c:out value='${tag.tagName}'/>" disabled required maxlength="30" oninvalid="this.setCustomValidity('<spring:message code="input.invalid"/>')"/>
                        <input type="hidden" name="id" value="${tag.id}"/>
                        <input type="hidden" name="${_csrf.parameterName}"
                                    value="${_csrf.token}" />
                        <input class="editTagLink hidden" id="updateTagButton${tag.id}" type="submit" value="<spring:message code="update"/>"/>
                    </form>
                    <form action="${ pageContext.request.contextPath }/tags/delete" method="post" class="inlineBlock">
                        <input type="hidden" name="${_csrf.parameterName}"
                            value="${_csrf.token}" />
                        <input type="hidden" name="tagId" value="${tag.id}"/>
                        <input class="editTagLink hidden" id="deleteTagButton${tag.id}" type="submit" value="<spring:message code="delete"/>"/>
                    </form>
                    <form action="${ pageContext.request.contextPath }/tags/cancel" method="get" class="inlineBlock">
                        <input type="hidden" name="tagId" value="${tag.id}"/>
                        <input class="editTagLink hidden" id="cancelEditTagButton${tag.id}" type="submit" value="<spring:message code="cancel"/>"/>
                    </form>
                    <button onclick="displayEditTagLinks(${tag.id})" class="displayTagLinksButton" id="displayTagLinksButton${tag.id}"><spring:message code="edit"/></button>
                </td>
            </tr>
        </c:forEach>
    </table>
    <div id="addTag">
        <form action="${ pageContext.request.contextPath }/tags/add" method="post">
        <table>
            <tr>
                <td>
                    <label for="tagName">
                        <spring:message code="tag.add"/>:
                    </label>
                    <input type="text" name="tagName" id="tagName" required maxlength="30" oninvalid="this.setCustomValidity('<spring:message code="input.invalid"/>')"/>
                </td>
                <td>
                     <input type="hidden" name="${_csrf.parameterName}"
                                    value="${_csrf.token}" />
                     <input type="submit" value="<spring:message code="save"/>"/>
                </td>
            </tr>
        </table>
        </form>
    </div>
</div>