<%@ page language="java" contentType="text/html;charset=UTF-8"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<div id="loginCoverBlock">
    <div id="loginBlock">
        ${loginError}
        ${accessError}
        <form action="<c:url value='login' />" method='POST' >
            <table id="loginTable">
                <tr>
                    <td>
                        <label for="login"><spring:message code="user.login"/></label>
                    </td>
                    <td>
                        <input id="login" type="text" name="login"/>
                    </td>
                <tr>
                    <td>
                        <label for="login"><spring:message code="user.password"/></label>
                    </td>
                    <td>
                        <input id="password" type="password" name="password"/>
                    </td>
                <tr>
                    <td>
                    </td>
                    <td>
                        <input  type="hidden" name="${_csrf.parameterName}"
                                                        value="${_csrf.token}" />
                        <input name="submit" type="submit" class="button" id="loginButton"
                                            value="<spring:message code="user.sign.in"/>" />


                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>