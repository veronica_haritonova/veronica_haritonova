<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="ctg" uri="customtags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<div id="newsFilter">
    <table id="newsFilterTable">
         <tr>
             <td>
                <form action="${ pageContext.request.contextPath }/news" method="get">
                    <div class="multiselect">
                        <div class="selectBox" onclick="showCheckboxes()">
                            <select>
                                <option>
                                    <spring:message code="tags.choose"/>
                                </option>
                            </select>
                            <div class="overSelect"></div>
                        </div>
                        <div id="checkboxes">
                           <ctg:tagsCheckboxes selectedTagsId="${searchCriteria.tagsId}" tagList="${tagList}"/>
                        </div>
                    </div>
                    <select name="authorId">
                        <option selected disabled><spring:message code="author.choose"/></option>
                        <ctg:authorsSelect selectedAuthorId="${searchCriteria.authorId}" authorList="${authorList}"/>
                    </select>
                    <input type="hidden" name="isNewCriteria" value="true"/>
                    <input type="submit" id="filterButton" value="<spring:message code="filter"/>">
                </form>
             </td>
             <td>
                <form action="${ pageContext.request.contextPath }/news/resetFilter" method="get">
                   <input type="submit" id="resetFilterButton" value="<spring:message code="reset"/>">
                </form>
             </td>
         </tr>
    </table>
</div>
<div id="newsList">
    <form action="${ pageContext.request.contextPath }/news/delete" method="POST">
        <input type="hidden" name="${_csrf.parameterName}"
                    value="${_csrf.token}" />
        <c:forEach var="news" items = "${newsList}">
            <div class="shortNews">
                <h3 class="newsTitle">
                    <a href="${ pageContext.request.contextPath }/news/${news.id}" >
                        <c:out value="${news.title}"/>
                    </a>
                </h3>
                <span class="creationDate">
                     <ctg:formatDate date="${news.creationDate}"/>
                </span>
                <span class="authorName">
                      <spring:message code="news.author" arguments="${news.authorName}"/>
                </span>
                <p class="shortText">
                    <c:out value="${news.shortText}"/>
                </p>
                <input type="checkbox" class="deleteCheckBox" name="newsIdList" value="${news.id}">
                <span class="newsEditLink">
                        <a href="${ pageContext.request.contextPath }/news/edit/${news.id}"><spring:message code="edit"/></a>
                </span>
                <span class="commentsAmount"><spring:message code="comments"/>(${news.commentsAmount})</span>
                <span class="tagNames">
                    <c:forEach var="tag" items = "${news.tagsNames}" varStatus="status">
                        <c:out value="${tag}"/>
                        <c:if test="${status.count < fn:length(news.tagsNames)}">,</c:if>
                    </c:forEach>
                </span>
            </div>
        </c:forEach>
        <input type="hidden" name="page" value="${page}"/>
        <c:if test="${newsAmount != 0}">
            <input type="submit" class="button" id="deleteNewsButton" value="<spring:message code="delete"/>"/>
        </c:if>
    </form>
</div>

<c:choose>
    <c:when test="${newsAmount != 0}">
        <div class="pager">
            <ctg:newsPager generalAmount="${newsAmount}" page = "${page}"/>
        </div>
    </c:when>
    <c:otherwise>
        <spring:message code="news.not.found"/>
    </c:otherwise>
</c:choose>




