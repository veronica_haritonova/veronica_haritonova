<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<sec:authorize access="isAuthenticated()">
    <div id="authBlock">
        <p class="inlineBlock"><spring:message code="hello"/>,<sec:authentication property="principal.username" /></p>
        <form  class="inlineBlock" action="logout" method='POST' >
            <input type="hidden" name="${_csrf.parameterName}"  value="${_csrf.token}" />
            <input id ="logoutButton" type="submit" value="<spring:message code="logout"/>"/>
        </form>
    </div>
</sec:authorize>
<div id="siteTitle">
    <h3><spring:message code="site.title"/></h3>
</div>
<div id="localeBlock">
    <a href="?locale=en_us">EN</a>
    <a href="?locale=ru">RU</a>
</div>
