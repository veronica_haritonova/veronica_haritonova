<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<c:if test="${isLocked}">
 <p class="error"><spring:message code="news.lock"/></p>
</c:if>
<form action="${ pageContext.request.contextPath }/news/edit" method="post">
    <table class="newsModificationTable">
        <tr>
            <td>
                <input type="hidden" name="news.id" value="${newsModel.news.id}" />
                <label for="newsTitle"><spring:message code="title"/>:</label>
            </td>
            <td class="fullWidth">
                <form:errors path="newsModel.news.title"/>
                <input id="newsTitle" type="text" name="news.title" value="<c:out value='${newsModel.news.title}'/>" required maxlength="30"
                oninvalid="this.setCustomValidity('<spring:message code="input.invalid"/>')"/>
            </td>
        </tr>
        <tr>
            <td>
                <label for="newsDate"><spring:message code="date"/>:</label>
            </td>
            <td>
                <input id="newsDate" type="text" name="news.modificationDate" required value="<ctg:formatDate date="${newsModel.news.modificationDate}"/>"
                pattern="\d{2}(/|.)\d{2}(/|.)\d{4}" oninvalid="this.setCustomValidity('<spring:message code="input.invalid"/>')" />
                <form:errors path="newsModel.news.modificationDate"/>
            </td>
        </tr>
        <tr>
            <td>
                <label for="newsBrief`"><spring:message code="news.brief"/>:</label>
            </td>
            <td class="fullWidth">
                <form:errors path="newsModel.news.shortText"/>
                 <textarea id="newsBrief"name="news.shortText" rows="4" maxlength="100" required oninvalid="this.setCustomValidity('<spring:message code="input.invalid"/>')">
                    <c:out value="${newsModel.news.shortText}"/>
                </textarea>
            </td>
        </tr>
        <tr>
            <td>
                <label for="newsContent"><spring:message code="news.content"/>:</label>
            </td>
            <td class="fullWidth">
                <form:errors path="newsModel.news.fullText"/>
                <textarea id="newsContent"name="news.fullText" rows="8" maxlength="2000" required oninvalid="this.setCustomValidity('<spring:message code="input.invalid"/>')">
                    <c:out value="${newsModel.news.fullText}"/>
                </textarea>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td id="authorTagChooseBlock">
                <div class="multiselect">
                    <div class="selectBox" onclick="showCheckboxes()">
                        <select>
                            <option><spring:message code="tags.choose"/></option>
                        </select>
                        <div class="overSelect"></div>
                    </div>
                    <div id="checkboxes">
                       <ctg:tagsCheckboxes selectedTagsId="${newsModel.tagsId}" tagList="${tagList}"/>
                    </div>
                </div>
                <select name="authorId">
                    <option selected disabled><spring:message code="author.choose"/></option>
                    <ctg:authorsSelect selectedAuthorId="${newsModel.authorId}" authorList="${authorList}"/>
                </select>
                <form:errors path="newsModel.authorId"/>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                 <input type="hidden" name="${_csrf.parameterName}"
                        value="${_csrf.token}" />
                 <input type="hidden" name="news.version" value="${newsModel.news.version}" />
                 <input type="submit" value="<spring:message code="save"/>" class="floatRight button"/>
            </td>
        </tr>
    </table>
</form>
