<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<sec:authorize access="hasRole('ROLE_ADMIN')">
    <div id="menuCoverBlock">
        <ul id="menu">
            <li><a href="${ pageContext.request.contextPath }/news?isNewCriteria=true"><spring:message code="menu.news.list" /></a></li>
            <li><a href="${ pageContext.request.contextPath }/news/add"><spring:message code="menu.news.add" /></a></li>
            <li><a href="${ pageContext.request.contextPath }/authors"><spring:message code="menu.authors" /></a></li>
            <li><a href="${ pageContext.request.contextPath }/tags"><spring:message code="menu.tags" /></a></li>
        </ul>
    </div>
</sec:authorize>