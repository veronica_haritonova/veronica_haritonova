package com.epam.newsmanagement.dao.impl.eclipselink;

import com.epam.newsmanagement.dao.IAuthorDAO;
import com.epam.newsmanagement.entity.Author;

import com.epam.newsmanagement.exception.DAOException;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Profile("jpa")
@Repository("authorDAO")
@Transactional
public class AuthorDAOImpl implements IAuthorDAO {

    @PersistenceContext
    private EntityManager entityManager;

    private final static String HQL_SET_EXPIRED = "update Author set expired = CURRENT_TIMESTAMP where id = :author_id";

    public Long add(Author author) throws DAOException {
        entityManager.persist(author);
        return author.getId();
    }

    public void update(Author author) throws DAOException {
        Author existingAuthor = entityManager.find(Author.class, author.getId());
        existingAuthor.setAuthorName(author.getAuthorName());
        entityManager.merge(existingAuthor);
    }

    public void delete(Long authorId) throws DAOException {
        Author author = entityManager.find(Author.class, authorId);
        entityManager.remove(author);
    }

    public List<Author> selectAll() throws DAOException {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Author> cq = builder.createQuery(Author.class);
        Root<Author> root = cq.from(Author.class);
        cq.select(root);
        return entityManager.createQuery(cq).getResultList();
    }

    public void setExpired(Long authorId) throws DAOException {
        javax.persistence.Query q = entityManager.createQuery(HQL_SET_EXPIRED).setParameter("author_id", authorId);
        q.executeUpdate();
    }

    public Author get(Long id) throws DAOException {
        return entityManager.find(Author.class, id);
    }
}
