package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.INewsDAO;
import com.epam.newsmanagement.dao.ISearchDAO;
import com.epam.newsmanagement.entity.*;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.INewsService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Veronica_Haritonova
 *         Service for work with news
 */
@Service
@Transactional(rollbackFor = ServiceException.class)
public class NewsServiceImpl implements INewsService {
    Logger logger = Logger.getLogger(NewsServiceImpl.class);
    @Autowired
    private INewsDAO newsDAO;
    @Autowired
    private ISearchDAO searchDAO;

    public NewsServiceImpl(INewsDAO newsDAO, ISearchDAO searchDAO) {
        this.newsDAO = newsDAO;
        this.searchDAO = searchDAO;
    }

    public NewsServiceImpl() {
    }

    public Long addNews(News news) throws ServiceException {
        try {
            Long id = newsDAO.add(news);
            return id;
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    public News getNews(Long newsId) throws ServiceException {
        try {
            News news = newsDAO.get(newsId);
            return news;
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    public void updateNews(News news) throws ServiceException {
        try {
            newsDAO.update(news);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    public void deleteNews(Long newsId) throws ServiceException {
        try {
            newsDAO.delete(newsId);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    public List<String> getTagsNames(Long newsId) throws ServiceException {
        try {
            List<String> tagsNames = newsDAO.getTagsNames(newsId);
            return tagsNames;
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    public List<NewsWorkItem> findByCriteria(SearchCriteriaVO criteria, int from, int amount) throws ServiceException {
        try {
            List<NewsWorkItem> newsList = searchDAO.find(criteria, from, amount);
            return newsList;
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    public Long getCriteriaNewsAmount(SearchCriteriaVO criteria) throws ServiceException {
        try {
            Long amount = searchDAO.countFoundNews(criteria);
            return amount;
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    public List<Long> getNewsIdList(SearchCriteriaVO criteria, int from, int amount) throws ServiceException {
        try {
            List<Long> newsIdList = searchDAO.findIdList(criteria,from, amount);
            return newsIdList;
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }
}


