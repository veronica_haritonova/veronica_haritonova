package com.epam.newsmanagement.util;

import com.epam.newsmanagement.exception.DAOException;
import org.hibernate.Session;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @author Veronica_Haritonova
 */
public class DBUtils {
    public static void closeSession(Session session) {
        if(session != null) {
            session.close();
        }
    }

    /**
     * Close object of ResultSet
     *
     * @param rs - resultSet to close
     * @throws DAOException
     */
    public static void closeResultSet(ResultSet rs) throws DAOException {
        try {
            if (rs != null) {
                rs.close();
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    /**
     * Close statements and preparedStatements
     *
     * @param st -statement to close
     * @throws DAOException
     */
    public static void closeStatement(Statement st) throws DAOException {
        try {
            if (st != null) {
                st.close();
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    /**
     * Close connection
     *
     * @param cn - connection to close
     */
    public static void closeConnection(Connection cn, DriverManagerDataSource dataSource) throws DAOException {
        if (cn != null) {
            try {
                DataSourceUtils.doReleaseConnection(cn, dataSource);
            } catch (SQLException e) {
                throw new DAOException(e);
            }
        }
    }

    /**
     * Close statement and connection
     *
     * @param st
     * @param cn
     */
    public static void close(Statement st, Connection cn, DriverManagerDataSource dataSource) throws DAOException {
        closeStatement(st);
        closeConnection(cn, dataSource);
    }

    /**
     * Close resultSet, statement and connection
     *
     * @param rs
     * @param st
     * @param cn
     */
    public static void close(ResultSet rs, Statement st, Connection cn, DriverManagerDataSource dataSource) throws DAOException {
        closeResultSet(rs);
        closeStatement(st);
        closeConnection(cn, dataSource);
    }
}
