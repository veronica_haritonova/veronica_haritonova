package com.epam.newsmanagement.entity;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

/**
 * @author Veronica_Haritonova
 *         Class to store data of tag
 */
@Entity
@Table(name = "tag")

public class Tag implements Serializable {
    /**
     * Identificator of tag
     */
    @Id
    @GeneratedValue(generator = "tag_seq_gen", strategy= GenerationType.SEQUENCE/*GenerationType.AUTO*/)
    @SequenceGenerator(name="tag_seq_gen", sequenceName="tag_seq", allocationSize = 1)
    @Column(name = "tag_id")
    private Long id;

    /**
     * Name of tag
     */
    @Column(name = "tag_name")
    @NotEmpty
    @Size(max = 30)
    private String tagName;
    @ManyToMany
    @JoinTable(
            name = "news_tag",
            joinColumns = @JoinColumn(name = "tag_id"),
            inverseJoinColumns = @JoinColumn(name = "news_id")
    )
    private List<News> newsList;

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 5498574324973865394L;

    public Tag(String tagName) {
        this.tagName = tagName;
    }

    public Tag() {

    }

    public Tag(Long id) {
        this.id = id;
    }

    public Tag(Long id, String tagName) {
        this.id = id;
        this.tagName = tagName;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<News> getNewsList() {
        return newsList;
    }

    public void setNewsList(List<News> newsList) {
        this.newsList = newsList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Tag)) return false;

        Tag tag = (Tag) o;

        if (id != null ? !id.equals(tag.id) : tag.id != null) return false;
        if (tagName != null ? !tagName.equals(tag.tagName) : tag.tagName != null) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (tagName != null ? tagName.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Tag{" +
                "id=" + id +
                ", tagName='" + tagName +
                '}';
    }
}
