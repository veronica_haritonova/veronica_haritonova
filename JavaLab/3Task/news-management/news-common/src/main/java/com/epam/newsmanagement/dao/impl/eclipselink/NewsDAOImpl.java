package com.epam.newsmanagement.dao.impl.eclipselink;

import com.epam.newsmanagement.dao.INewsDAO;
import com.epam.newsmanagement.entity.*;
import com.epam.newsmanagement.exception.DAOException;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository("newsDAO")
@Transactional
@Profile("jpa")
public class NewsDAOImpl implements INewsDAO {

    @PersistenceContext
    private EntityManager entityManager;

    private static final String GET_AUTHOR = "SELECT n.author FROM News n WHERE n.id = :id ";
    private static final String GET_TAGS = "SELECT n.tagList FROM News n WHERE n.id = :id ";
    private static final String GET_TAGS_NAMES = "SELECT  nt.tagName FROM News n JOIN n.tagList nt  WHERE n.id = :id ";
    private static final String GET_COMMENTS = "SELECT n.commentList FROM News n WHERE n.id = :id ";
    private static final String GET_COUNT = "SELECT COUNT(n) FROM News n";

    public Long add(News news) throws DAOException {
        return entityManager.merge(news).getId();
    }

    public void update(News news) throws DAOException {
        News existingNews = get(news.getId());
        news.setCommentList(existingNews.getCommentList());
        news.setCreationDate(existingNews.getCreationDate());
        entityManager.merge(news);
    }

    public void delete(Long newsId) throws DAOException {
        News news = entityManager.find(News.class,newsId);
        entityManager.remove(news);
    }

    public List<News> selectAll() throws DAOException {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<News> cq = builder.createQuery(News.class);
        Root<News> root = cq.from(News.class);
        cq.select(root);
        return entityManager.createQuery(cq).getResultList();
    }

    public Long countAll() throws DAOException {
        Query q = entityManager.createQuery(GET_COUNT);
        Long count = (Long)q.getSingleResult();
        return count;
    }

    public News get(Long newsId) throws DAOException {
        News news = entityManager.find(News.class, newsId);
        news.getTagList().size();
        news.getCommentList().size();
        news.getAuthor().getId();
        return news;
    }

    public Author getAuthor(Long newsId) throws DAOException {
        TypedQuery<Author> query = entityManager.createQuery(GET_AUTHOR, Author.class);
        query.setParameter("id", newsId);
        Author author = query.getSingleResult();
        return author;
    }

    public List<Tag> getTags(Long newsId) throws DAOException {
        TypedQuery<Tag> query = entityManager.createQuery(GET_TAGS, Tag.class);
        query.setParameter("id", newsId);
        return query.getResultList();
    }


    public List<Comment> getComments(Long newsId) throws DAOException {
        TypedQuery<Comment> query = entityManager.createQuery(GET_COMMENTS, Comment.class);
        query.setParameter("id", newsId);
        return query.getResultList();
    }

    public List<String> getTagsNames(Long newsId) throws DAOException {
        TypedQuery<String> query = entityManager.createQuery(GET_TAGS_NAMES, String.class);
        query.setParameter("id", newsId);
        List<String> tagNames = query.getResultList();
        return tagNames;
    }
}
