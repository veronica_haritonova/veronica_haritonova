package com.epam.newsmanagement.dao.impl.hibernate;

import com.epam.newsmanagement.dao.ITagDAO;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.util.DBUtils;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.*;
import java.util.List;

/**
 * @author Veronica_Haritonova
 *         Class for performing basic operations with tags in database, implements ITagDAO
 */
@Repository("hibernateTagDAO")
@Profile("hibernate")
@Transactional
public class HibernateTagDAOImpl implements ITagDAO {

    @Autowired
    private SessionFactory sessionFactory;

    private static final String HQL_DELETE_TAG = "delete Tag where id = :tag_id ";
    private static final String HQL_UPDATE_TAG = "update Tag set tagName = :tag_name where id = :tag_id";

    public Long add(Tag tag) throws DAOException {
        Session session = sessionFactory.getCurrentSession();
        return (Long)session.save(tag);
    }

    public void update(Tag tag) throws DAOException {
        Session session = sessionFactory.getCurrentSession();
        /*Tag existingTag = (Tag)session.get(Tag.class, tag.getId());
        existingTag.setTagName(tag.getTagName());
        session.update(existingTag);*/
        Query q = session.createQuery(HQL_UPDATE_TAG).setLong("tag_id", tag.getId()).setString("tag_name",tag.getTagName());
        q.executeUpdate();

    }

    public void delete(Long tagId) throws DAOException {
        Session session = sessionFactory.getCurrentSession();
        session.delete(session.get(Tag.class,tagId));
        //Query q = session.createQuery(HQL_DELETE_TAG).setLong("tag_id", tagId);
        //q.executeUpdate();
    }

    public List<Tag> selectAll() throws DAOException {
        Session session = sessionFactory.getCurrentSession();
        List<Tag> tagList = session.createCriteria(Tag.class).list();
        return tagList;
    }
    public Tag get(Long id) throws DAOException {
        Session session = sessionFactory.getCurrentSession();
        return (Tag)session.get(Tag.class,id);
    }
}
