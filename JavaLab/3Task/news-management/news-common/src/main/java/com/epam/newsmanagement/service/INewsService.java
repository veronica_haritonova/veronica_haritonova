package com.epam.newsmanagement.service;

import com.epam.newsmanagement.entity.*;
import com.epam.newsmanagement.exception.ServiceException;

import java.util.List;

/**
 * @author Veronica_Haritonova
 *         Class to perform basic operations of news with its elements separately
 */
public interface INewsService {
    /**
     * Add news
     *
     * @param news
     * @return id of added news
     * @throws ServiceException
     */
    Long addNews(News news) throws ServiceException;

    /**
     * Get news with given id
     *
     * @param newsId
     * @return news
     * @throws ServiceException
     */
    News getNews(Long newsId) throws ServiceException;

    /**
     * Update news with given id
     *
     * @param news
     * @throws ServiceException
     */
    void updateNews(News news) throws ServiceException;

    /**
     * Delete news with given id
     *
     * @param newsId
     * @throws ServiceException
     */
    void deleteNews(Long newsId) throws ServiceException;

    /**
     * Get tags' names of news with given id
     * @param newsId
     * @return list of tags' names
     * @throws ServiceException
     */
    List<String> getTagsNames(Long newsId) throws ServiceException;
    /**
     * Find news of defined amount by criteria
     *
     * @param criteria
     * @param from
     * @param amount
     * @return list of news
     * @throws ServiceException
     */
    List<NewsWorkItem> findByCriteria(SearchCriteriaVO criteria,int from, int amount) throws ServiceException;


    /**
     * Get amount of all news with criteria
     * @param criteria
     * @return amount of news
     * @throws ServiceException
     */
    Long getCriteriaNewsAmount(SearchCriteriaVO criteria) throws ServiceException;

   /**
     * Get defined amount of news' id
     *
     * @param from
     * @param amount
     * @return list of news' id
     * @throws ServiceException
     */
    List<Long> getNewsIdList(SearchCriteriaVO criteria,int from, int amount) throws ServiceException;
}
