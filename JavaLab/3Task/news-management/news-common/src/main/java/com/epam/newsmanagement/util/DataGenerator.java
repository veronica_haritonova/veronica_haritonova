package com.epam.newsmanagement.util;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.log4j.Logger;


import java.io.*;
import java.util.*;

/**
 * Created by Veronica_Haritonova on 8/26/2015.
 */
public class DataGenerator {
    Logger logger = Logger.getLogger(DataGenerator.class);

    private static final String SQL_INSERT_NEWS = "INSERT INTO news (news_id, title, short_text,full_text,creation_date, modification_date) VALUES (news_seq.nextval,%s,%s,%s,%s,%s);";
    private static final String SQL_INSERT_TAG = "INSERT INTO tag (tag_id, tag_name) VALUES (tag_seq.nextval, %s);";
    private static final String SQL_INSERT_AUTHOR = "INSERT INTO author (author_id, author_name) VALUES (author_seq.nextval, %s);";
    private static final String SQL_INSERT_USER = "INSERT INTO users (user_id, user_name, login, password) VALUES (users_seq.nextval, %s, %s, %s);";
    private static final String SQL_INSERT_NEWS_TAG = "INSERT INTO news_tag (news_id, tag_id) VALUES (%s, %s); ";
    private static final String SQL_INSERT_NEWS_AUTHOR = "INSERT news_author (news_id, author_id) VALUES (%s,%s); ";
    private static final String SQL_INSERT_COMMENT = "INSERT INTO comments (comment_id, news_id, comment_text) VALUES (comments_seq.nextval, %s, %s);";

    private PrintWriter pw;
    private File file;
    private Random random = new Random();

    public DataGenerator(File file) {
        this.file = file;
    }
    private void initializeWriter() throws IOException {
        pw = new PrintWriter(new BufferedWriter(new FileWriter(file)));
    }

    private static final int ITEMS_AMOUNT = 10;
    private static final int NEWS_AMOUNT = 10000;
    private static final int LINKS_AMOUNT = 5;
    private static final int MAX_NEWS_ID = 10000;
    private static final int MAX_ITEM_ID = 100;


    private void generateTags() {
        for (int i = 0; i < ITEMS_AMOUNT; i++) {
            pw.printf(SQL_INSERT_TAG, getRandomRangeString(30));
        }
    }

    private void generateNewsTagsLinks() {
        for(int j = 0; j < ITEMS_AMOUNT; j++) {
            int newsId = random.nextInt(MAX_NEWS_ID);
            for (int i = 0; i < LINKS_AMOUNT; i++) {
                pw.printf(SQL_INSERT_NEWS_TAG, newsId, random.nextInt(MAX_ITEM_ID));
            }
        }
    }
    private void generateComments() {
        for(int j = 0; j < ITEMS_AMOUNT; j++) {
            int newsId = random.nextInt(MAX_NEWS_ID);
            for (int i = 0; i < ITEMS_AMOUNT; i++) {
                pw.printf(SQL_INSERT_COMMENT, newsId, getRandomRangeString(100));
            }
        }
    }
    private void generateNewsAuthor() {
        for(int j = 0; j < ITEMS_AMOUNT; j++) {
            int newsId = random.nextInt(MAX_NEWS_ID);
            pw.printf(SQL_INSERT_NEWS_AUTHOR, newsId, random.nextInt(MAX_ITEM_ID));
        }
    }

    private void generateAuthors() {
        for (int i = 0; i < ITEMS_AMOUNT; i++) {
            pw.printf(SQL_INSERT_AUTHOR, getRandomRangeString(30));

        }
    }
    private void generateUsers() {
        for (int i = 0; i < ITEMS_AMOUNT; i++) {
            pw.printf(SQL_INSERT_USER, getRandomRangeString(50),getRandomRangeString(30),getRandomRangeString(32));

        }
    }

    private void closeWriter() {
        pw.close();
    }

    private void generateNews() {
        Date date = Calendar.getInstance().getTime();
        for(int i = 0; i < NEWS_AMOUNT; i++) {
            pw.printf(SQL_INSERT_NEWS, getRandomRangeString(30), getRandomRangeString(100), getRandomRangeString(2000), date, date);
        }
    }
    public void generateScript() {
        try {
            initializeWriter();
            generateTags();
            generateAuthors();
            generateNews();
            generateUsers();
            generateNewsAuthor();
            generateNewsTagsLinks();
            generateComments();
            closeWriter();
        } catch (IOException e) {
            logger.error(e);
        }
    }

    private String getRandomRangeString(int max) {
        return RandomStringUtils.randomAlphanumeric(random.nextInt(max));
    }
}
