package com.epam.newsmanagement.dao.impl.eclipselink;

import com.epam.newsmanagement.dao.ICommentDAO;
import com.epam.newsmanagement.dao.INewsDAO;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.util.DBUtils;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository(value = "commentDAO")
@Transactional
@Profile("jpa")
public class CommentDAOImpl implements ICommentDAO {
    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    @Qualifier("newsDAO")
    INewsDAO newsDAO;

    public Long add(Comment comment) throws DAOException {
        entityManager.persist(comment);
        News news = newsDAO.get(comment.getNewsId());
        news.getCommentList().add(comment);
        entityManager.merge(news);
        entityManager.flush();
        entityManager.refresh(comment);
        return comment.getId();

    }

    public void delete(Long commentId) throws DAOException {
        Comment comment = entityManager.find(Comment.class,commentId);
        javax.persistence.Query query = entityManager.createQuery("delete FROM Comment c where c.id = :id");
        query.setParameter("id", commentId);
        query.executeUpdate();
        News news = newsDAO.get(comment.getNewsId());
        news.getCommentList().remove(comment);
        entityManager.merge(news);
    }

    public void update(Comment comment) throws DAOException {
        entityManager.merge(comment);
    }

    public Comment get(Long commentId) throws DAOException {
        return entityManager.find(Comment.class,commentId);
    }

    public List<Comment> selectAll() throws DAOException {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Comment> cq = builder.createQuery(Comment.class);
        Root<Comment> root = cq.from(Comment.class);
        cq.select(root);
        return entityManager.createQuery(cq).getResultList();
    }
}
