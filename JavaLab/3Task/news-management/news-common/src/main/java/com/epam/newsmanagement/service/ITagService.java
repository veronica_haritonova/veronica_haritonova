package com.epam.newsmanagement.service;

import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ServiceException;

import java.util.List;

/**
 * @author Veronica_Haritonova
 *         Service for work with tags
 */
public interface ITagService {
    /**
     * Add given tag
     *
     * @param tag to add
     * @throws ServiceException
     */
    void addTag(Tag tag) throws ServiceException;

    /**
     * Delete tag with given id
     *
     * @param tagId
     * @throws ServiceException
     */
    void deleteTag(Long tagId) throws ServiceException;

    /**
     * Update given tag
     *
     * @param tag
     * @throws ServiceException
     */
    void updateTag(Tag tag) throws ServiceException;

    /**
     * Get all tags
     *
     * @return list of tags
     * @throws ServiceException
     */
    List<Tag> getAllTags() throws ServiceException;

    /**
     * Get tag with given id
     * @param tagId
     * @return tag
     * @throws ServiceException
     */
    Tag getTag(Long tagId) throws ServiceException;

}
