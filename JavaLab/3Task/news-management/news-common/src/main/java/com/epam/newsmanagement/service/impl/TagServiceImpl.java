package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.ITagDAO;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.ITagService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Veronica_Haritonova
 *         Service for work with tags
 */
@Service
@Transactional(rollbackFor = ServiceException.class)
public class TagServiceImpl implements ITagService {
    Logger logger = Logger.getLogger(TagServiceImpl.class);
    @Autowired
    private ITagDAO tagDAO;

    public TagServiceImpl(ITagDAO tagDAO) {
        this.tagDAO = tagDAO;
    }

    public TagServiceImpl() {
    }

    public void addTag(Tag tag) throws ServiceException {
        try {
            tagDAO.add(tag);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    public void deleteTag(Long tagId) throws ServiceException {
        try {
            tagDAO.delete(tagId);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    public void updateTag(Tag tag) throws ServiceException {
        try {
            tagDAO.update(tag);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    public List<Tag> getAllTags() throws ServiceException {
        try {
            List<Tag> tags = tagDAO.selectAll();
            return tags;
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    public Tag getTag(Long tagId) throws ServiceException {
        try {
            Tag tag = tagDAO.get(tagId);
            return tag;
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }
}
