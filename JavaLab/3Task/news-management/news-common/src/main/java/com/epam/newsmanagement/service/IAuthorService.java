package com.epam.newsmanagement.service;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.ServiceException;

import java.util.List;

public interface IAuthorService {
    /**
     * Add author
     *
     * @param author to add
     * @throws ServiceException
     */
    void addAuthor(Author author) throws ServiceException;

    /**
     * Delete author with given id
     *
     * @param authorId
     * @throws ServiceException
     */
    void deleteAuthor(Long authorId) throws ServiceException;

    /**
     * Update author
     *
     * @param author
     * @throws ServiceException
     */
    void updateAuthor(Author author) throws ServiceException;

    /**
     * Get all authors
     *
     * @return list of authors
     * @throws ServiceException
     */
    List<Author> getAllAuthors() throws ServiceException;

    /**
     * Get author with given id
     *
     * @param authorId
     * @return author
     * @throws ServiceException
     */
    Author getAuthor(Long authorId) throws ServiceException;

    /**
     * Make author expired
     *
     * @param authorId
     * @throws ServiceException
     */
    void setExpired(Long authorId) throws ServiceException;
}
