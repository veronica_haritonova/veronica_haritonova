package com.epam.newsmanagement.entity;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Veronica_Haritonova
 *         Class to store data of comment
 */
@Entity
@Table(name = "comments")
//@NamedQuery(name = "deleteComment", query = "delete from Comment where id = :comment_id ")
public class Comment implements Serializable {
    /**
     * Identificator of comment
     */
    @Id
    @GeneratedValue(generator = "comment_seq_gen", strategy= GenerationType.SEQUENCE/*GenerationType.AUTO*/)
    @SequenceGenerator(name="comment_seq_gen", sequenceName="comments_seq",allocationSize = 1)
    @Column(name = "comment_id")
    private Long id;
    /**
     * Text of comment
     */

    @Column(name = "comment_text")
    @NotEmpty
    @Size(max = 100)
    private String text;

    /**
     * Date comment created
     */
    @Column(name = "creation_date", columnDefinition="TIMESTAMP default CURRENT_TIMESTAMP", updatable = false, insertable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;

    /**
     * Id of news
     */
    @Column(name = "news_id")
    private Long newsId;

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -6868123704711283892L;

    public Comment() {

    }

    public Comment(Long id, Long newsId, String text, Date creationDate) {
        this.id = id;
        this.text = text;
        this.creationDate = creationDate;
        this.newsId = newsId;
    }

    public Comment(String text, Long newsId) {
        this.text = text;
        this.newsId = newsId;
    }

    public Comment(Long id, Long newsId, String text) {
        this.id = id;
        this.newsId = newsId;
        this.text = text;
    }

    public Long getNewsId() {
        return newsId;
    }

    public void setNewsId(Long newsId) {
        this.newsId = newsId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Comment)) return false;

        Comment comment = (Comment) o;

        if (id != null ? !id.equals(comment.id) : comment.id != null) return false;
        if (text != null ? !text.equals(comment.text) : comment.text != null) return false;
        if (creationDate != null ? !creationDate.equals(comment.creationDate) : comment.creationDate != null)
            return false;
        return !(newsId != null ? !newsId.equals(comment.newsId) : comment.newsId != null);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (text != null ? text.hashCode() : 0);
        result = 31 * result + (creationDate != null ? creationDate.hashCode() : 0);
        result = 31 * result + (newsId != null ? newsId.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "id=" + id +
                ", text='" + text + '\'' +
                ", creationDate=" + creationDate +
                ", newsId=" + newsId +
                '}';
    }
}
