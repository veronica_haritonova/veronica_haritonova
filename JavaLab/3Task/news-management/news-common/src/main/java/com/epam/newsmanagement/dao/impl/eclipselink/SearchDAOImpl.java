package com.epam.newsmanagement.dao.impl.eclipselink;

import com.epam.newsmanagement.dao.ISearchDAO;
import com.epam.newsmanagement.entity.*;
import com.epam.newsmanagement.exception.DAOException;
import org.hibernate.*;
import org.hibernate.criterion.*;
import org.hibernate.sql.JoinType;
import org.hibernate.transform.Transformers;
import org.hibernate.type.IntegerType;
import org.hibernate.type.LongType;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository("searchDAO")
@Transactional
@Profile("jpa")
public class SearchDAOImpl implements ISearchDAO {

    @PersistenceContext
    private EntityManager entityManager;

    private static final String SELECT = " SELECT DISTINCT n FROM News n ";

    private static final String SELECT_ID = " SELECT DISTINCT n.id FROM News n ";

    private static final String COUNT = "SELECT COUNT (n.id) FROM News n ";

    private static final String SELECT_BY_AUTHOR = "JOIN n.author na WHERE na.id = :authorId ";
    private static final String SELECT_BY_TAGS = " n.id IN ( SELECT ne.id FROM News ne JOIN ne.tagList nt WHERE nt.id IN :tagIds GROUP BY ne.id  " +
            "HAVING COUNT(nt.id) = :amount )";
    private static final String SELECT_END =
            " ORDER BY size(n.commentList) DESC, n.modificationDate DESC ";
    private static final String SQL_WHERE = " WHERE ";
    private static final String SQL_AND = " AND ";


    public List<NewsWorkItem> find(SearchCriteriaVO criteria, int from, int amount) throws DAOException {
        TypedQuery<News> query = entityManager.createQuery(buildQuery(criteria, SELECT), News.class);
        addParameters(query, criteria);
        query.setFirstResult(from - 1);
        query.setMaxResults(amount);
        return getNewsList(query);
    }

    public Long countFoundNews(SearchCriteriaVO criteria) throws DAOException {
        Query query = entityManager.createQuery(buildQuery(criteria, COUNT));
        addParameters(query, criteria);
        return (Long)query.getSingleResult();
    }

    public List<Long> findIdList(SearchCriteriaVO criteria, int from, int amount) throws DAOException {
        TypedQuery<Long> query = entityManager.createQuery(buildQuery(criteria, SELECT_ID), Long.class);
        addParameters(query, criteria);
        query.setFirstResult(from - 1);
        query.setMaxResults(amount);
        return query.getResultList();
    }
    private String buildQuery(SearchCriteriaVO criteria, String startQuery) {
        StringBuffer query = new StringBuffer(startQuery);
        if(criteria == null) {
            query.append(SELECT_END);
            return query.toString();
        }
        Long authorId = criteria.getAuthorId();
        List<Long> tagsId = criteria.getTagsId();
        boolean authorExist = (authorId != null);
        boolean tagsExist = (tagsId != null) ? !tagsId.isEmpty() : false;
        if(authorExist) {
            query.append(SELECT_BY_AUTHOR);
        }
        if(tagsExist) {
            if(authorExist && tagsExist) {
                query.append(SQL_AND);
            } else {
                query.append(SQL_WHERE);
            }
            query.append(SELECT_BY_TAGS);

        }
        query.append(SELECT_END);
        return query.toString();
    }
    private void addParameters(Query query, SearchCriteriaVO criteria) {
        if(criteria == null) {
            return;
        }
        Long authorId = criteria.getAuthorId();
        List<Long> tagsId = criteria.getTagsId();
        boolean authorExist = (authorId != null);
        boolean tagsExist = (tagsId != null) ? !tagsId.isEmpty() : false;
        if (authorExist || tagsExist) {
            if (authorExist) {
                query.setParameter("authorId", authorId);
            }
            if (tagsExist) {
                query.setParameter("tagIds", tagsId);
                query.setParameter("amount", tagsId.size());
            }
        }
    }
    private List<NewsWorkItem> getNewsList(TypedQuery<News> query) {
        List<NewsWorkItem> newsList = new ArrayList<NewsWorkItem>();
        for (News news : query.getResultList()) {
            if(news != null) {
                NewsWorkItem item = new NewsWorkItem();
                item.setId(news.getId());
                item.setTitle(news.getTitle());
                item.setShortText(news.getShortText());
                item.setCreationDate(news.getCreationDate());
                Author author = news.getAuthor();
                if(author != null) {
                    item.setAuthorName(news.getAuthor().getAuthorName());
                }
                item.setCommentsAmount(news.getCommentList().size());
                List<Tag> tagList = news.getTagList();
                List<String> tagNamesList = new ArrayList<String>(tagList.size());
                for (Tag tag : tagList) {
                    if(tag != null) {
                        tagNamesList.add(tag.getTagName());
                    }
                }
                item.setTagsNames(tagNamesList);
                newsList.add(item);
            }
        }
        return newsList;
    }
}
