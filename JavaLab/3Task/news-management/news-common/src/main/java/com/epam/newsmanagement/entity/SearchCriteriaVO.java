package com.epam.newsmanagement.entity;

import java.util.List;

/**
 * @author Veronica_Haritonova
 *         Class to store criteria for search as list of id
 */
public class SearchCriteriaVO {
    /**
     * id of author to search
     */
    private Long authorId;
    /**
     * List of tags id
     */
    private List<Long> tagsId;


    public SearchCriteriaVO(Long authorId, List<Long> tagsId) {
        this.authorId = authorId;
        this.tagsId = tagsId;
    }

    public SearchCriteriaVO() {
    }

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public List<Long> getTagsId() {
        return tagsId;
    }

    public void setTagsId(List<Long> tagsId) {
        this.tagsId = tagsId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SearchCriteriaVO)) return false;

        SearchCriteriaVO that = (SearchCriteriaVO) o;

        if (authorId != null ? !authorId.equals(that.authorId) : that.authorId != null) return false;
        return !(tagsId != null ? !tagsId.equals(that.tagsId) : that.tagsId != null);

    }

    @Override
    public int hashCode() {
        int result = authorId != null ? authorId.hashCode() : 0;
        result = 31 * result + (tagsId != null ? tagsId.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "SearchCriteriaVO{" +
                "authorId=" + authorId +
                ", tagsId=" + tagsId +
                '}';
    }
}
