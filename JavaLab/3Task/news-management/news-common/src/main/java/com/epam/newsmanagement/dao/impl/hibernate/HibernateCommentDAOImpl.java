package com.epam.newsmanagement.dao.impl.hibernate;

import com.epam.newsmanagement.dao.ICommentDAO;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.util.DBUtils;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository("hibernateCommentDAO")
@Profile("hibernate")
@Transactional
public class HibernateCommentDAOImpl implements ICommentDAO {
    /**
     * object for getting connections
     */
    @Autowired
    private SessionFactory sessionFactory;

    private static final String HQL_DELETE_COMMENT = "delete Comment where comment_id = :comment_id ";

    public Long add(Comment comment) throws DAOException {
        Session session = sessionFactory.getCurrentSession();
        return (Long)session.save(comment);
    }


    public void delete(Long commentId) throws DAOException {
        Session session = sessionFactory.getCurrentSession();
        //session.delete(session.get(Comment.class, commentId));
        //Query q = session.getNamedQuery("deleteComment").setLong("comment_id", commentId);
        Query q = session.createQuery(HQL_DELETE_COMMENT).setLong("comment_id", commentId);
        q.executeUpdate();
    }

    public void update(Comment comment) throws DAOException {
        Session session = sessionFactory.getCurrentSession();
        session.update(comment);
    }

    public Comment get(Long commentId) throws DAOException {
        Session session = sessionFactory.getCurrentSession();
        return (Comment)session.get(Comment.class,commentId);
    }

    public List<Comment> selectAll() throws DAOException {
        Session session = sessionFactory.getCurrentSession();
        List<Comment> commentList = session.createCriteria(Comment.class).list();
        return commentList;
    }
}
