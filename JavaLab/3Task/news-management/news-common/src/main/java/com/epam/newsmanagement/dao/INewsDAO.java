package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.entity.*;
import com.epam.newsmanagement.exception.DAOException;

import java.util.List;

public interface INewsDAO {
    /**
     * Insert news in database
     *
     * @param news to add
     * @return id of inserted news
     * @throws DAOException
     */
    Long add(News news) throws DAOException;

    /**
     * Update given news in database
     *
     * @param news to update
     * @throws DAOException
     */
    void update(News news) throws DAOException;

    /**
     * Delete news with given id from database
     *
     * @param newsId
     * @throws DAOException
     */
    void delete(Long newsId) throws DAOException;

    /**
     * Get all news from database
     *
     * @return list of news
     * @throws DAOException
     */
    List<News> selectAll() throws DAOException;

    /**
     * Count amount of all news in database
     *
     * @return amount of all news
     * @throws DAOException
     */
    Long countAll() throws DAOException;

    /**
     * Get news with given id
     *
     * @param newsId
     * @return news
     * @throws DAOException
     */
    News get(Long newsId) throws DAOException;

    /**
     * Get author of news with given id
     *
     * @param newsId
     * @return author
     * @throws DAOException
     */
    Author getAuthor(Long newsId) throws DAOException;
    /**
     * Get all news tags by given id
     *
     * @param newsId
     * @return list of tags
     * @throws DAOException
     */
    List<Tag> getTags(Long newsId) throws DAOException;

    /**
     * Get all comments of news with given id
     *
     * @param newsId
     * @return
     * @throws DAOException
     */
    List<Comment> getComments(Long newsId) throws DAOException;

    /**
     * Get list of tags names of news with given id
     * @param newsId
     * @return list of tags' names
     * @throws DAOException
     */
    List<String> getTagsNames(Long newsId) throws DAOException;
}
