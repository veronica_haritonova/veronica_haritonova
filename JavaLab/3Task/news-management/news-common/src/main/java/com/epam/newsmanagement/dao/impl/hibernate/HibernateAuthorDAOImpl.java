package com.epam.newsmanagement.dao.impl.hibernate;

import com.epam.newsmanagement.dao.IAuthorDAO;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DAOException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository("hibernateAuthorDAO")
@Profile("hibernate")
@Transactional
public class HibernateAuthorDAOImpl implements IAuthorDAO {

    @Autowired
    private SessionFactory sessionFactory;


    private final static String HQL_DELETE_AUTHOR = "delete Author where id = :author_id ";
    private final static String HQL_UPDATE_AUTHOR = "update Author set authorName = :author_name where id = :author_id";
    private final static String HQL_SET_EXPIRED = "update Author set expired = CURRENT_TIMESTAMP() where id = :author_id";


    public Long add(Author author) throws DAOException {
        Session session = sessionFactory.getCurrentSession();
        return (Long)session.save(author);
    }


    public void update(Author author) throws DAOException {
        /*Session session = sessionFactory.getCurrentSession();
        Author existingAuthor = (Author)session.load(Author.class, author.getId());
        existingAuthor.setAuthorName(author.getAuthorName());
        session.update(author);*/
        Session session = sessionFactory.getCurrentSession();
        Query q = session.createQuery(HQL_UPDATE_AUTHOR).setLong("author_id", author.getId()).setString("author_name", author.getAuthorName());
        q.executeUpdate();
    }

    public void delete(Long authorId) throws DAOException {
        Session session = sessionFactory.getCurrentSession();
        //Query q = session.getNamedQuery("deleteAuthor").setLong("author_id", authorId);
        //Query q = session.createQuery(HQL_DELETE_AUTHOR).setLong("author_id", authorId);
        //q.executeUpdate();
        session.delete(session.get(Author.class,authorId));
    }

    public List<Author> selectAll() throws DAOException {
        Session session = sessionFactory.getCurrentSession();
        List<Author> authorList = session.createCriteria(Author.class).list();
        return authorList;
    }

    public void setExpired(Long authorId) throws DAOException {
        Session session = sessionFactory.getCurrentSession();
        Query q = session.createQuery(HQL_SET_EXPIRED).setLong("author_id", authorId);
        q.executeUpdate();
    }

    public Author get(Long id) throws DAOException {
        Session session = sessionFactory.getCurrentSession();
        return (Author)session.get(Author.class,id);
    }
}
