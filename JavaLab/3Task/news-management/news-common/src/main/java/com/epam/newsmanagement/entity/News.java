package com.epam.newsmanagement.entity;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Class to store basic data of news
 */
@Entity
@Table(name = "news")
public class News implements Serializable {
    /**
     * Identificator of news
     */
    @Id
    @GeneratedValue(generator = "news_seq_gen", strategy= GenerationType.SEQUENCE)
    @SequenceGenerator(name="news_seq_gen", sequenceName="news_seq", allocationSize = 1)
    @Column(name = "news_id")
    private Long id;
    /**
     * Title of news
     */
    @NotEmpty
    @Size(max = 30)
    @Column(name = "title")
    private String title;
    /**
     * Short text of news
     */
    @NotEmpty
    @Size(max = 100)
    @Column(name = "short_text")
    private String shortText;
    /**
     * Full text of news
     */
    @Column(name = "full_text")
    @NotEmpty
    @Size(max = 2000)
    private String fullText;
    /**
     * Date news created
     */
    @Column(name = "creation_date",updatable = false)
    @Past
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;
    /**
     * Date news modificated
     */
    @Column(name = "modification_date")
    @Past
    @Temporal(TemporalType.DATE)
    private Date modificationDate;

    @ManyToOne
    @JoinTable(
            name = "news_author",
            joinColumns = @JoinColumn(name = "news_id"),
            inverseJoinColumns = @JoinColumn(name = "author_id")
    )
    private Author author;

    @ManyToMany
    @JoinTable(
            name = "news_tag",
            joinColumns = @JoinColumn(name = "news_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id")
    )
    private List<Tag> tagList;


    @OneToMany(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "news_id", updatable = false)
    private List<Comment> commentList;


    @Version
    @Column(name = "OPTLOCK")
    private Integer version;

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 3769097090533376436L;

    public News() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortText() {
        return shortText;
    }

    public void setShortText(String shortText) {
        this.shortText = shortText;
    }

    public String getFullText() {
        return fullText;
    }

    public void setFullText(String fullText) {
        this.fullText = fullText;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public News(String title, String shortText, String fullText) {
        this.title = title;
        this.shortText = shortText;
        this.fullText = fullText;
    }

    public News(Long id, String title, String shortText, String fullText) {
        this.id = id;
        this.title = title;
        this.shortText = shortText;
        this.fullText = fullText;
    }

    public News(Long id, String title, String shortText, String fullText,
                Date creationDate, Date modificationDate) {
        this.id = id;
        this.title = title;
        this.shortText = shortText;
        this.fullText = fullText;
        this.creationDate = creationDate;
        this.modificationDate = modificationDate;
    }

    public List<Tag> getTagList() {
        return tagList;
    }

    public void setTagList(List<Tag> tagList) {
        this.tagList = tagList;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public List<Comment> getCommentList() {
        return commentList;
    }

    public void setCommentList(List<Comment> commentList) {
        this.commentList = commentList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof News)) return false;

        News news = (News) o;

        if (id != null ? !id.equals(news.id) : news.id != null) return false;
        if (title != null ? !title.equals(news.title) : news.title != null) return false;
        if (shortText != null ? !shortText.equals(news.shortText) : news.shortText != null) return false;
        if (fullText != null ? !fullText.equals(news.fullText) : news.fullText != null) return false;
        if (creationDate != null ? !creationDate.equals(news.creationDate) : news.creationDate != null) return false;
        if (modificationDate != null ? !modificationDate.equals(news.modificationDate) : news.modificationDate != null)
            return false;
         return true;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (shortText != null ? shortText.hashCode() : 0);
        result = 31 * result + (fullText != null ? fullText.hashCode() : 0);
        result = 31 * result + (creationDate != null ? creationDate.hashCode() : 0);
        result = 31 * result + (modificationDate != null ? modificationDate.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "News{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", shortText='" + shortText + '\'' +
                ", fullText='" + fullText + '\'' +
                ", creationDate=" + creationDate +
                ", modificationDate=" + modificationDate +
                 '}';
    }
}
