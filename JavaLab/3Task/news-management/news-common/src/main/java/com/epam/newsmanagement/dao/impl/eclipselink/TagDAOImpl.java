package com.epam.newsmanagement.dao.impl.eclipselink;

import com.epam.newsmanagement.dao.ITagDAO;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * @author Veronica_Haritonova
 *         Class for performing basic operations with tags in database, implements ITagDAO
 */
@Repository("tagDAO")
@Profile("jpa")
@Transactional
public class TagDAOImpl implements ITagDAO {

    @PersistenceContext
    private EntityManager entityManager;

    public Long add(Tag tag) throws DAOException {
        entityManager.persist(tag);
        return tag.getId();
    }

    public void update(Tag tag) throws DAOException {
        Tag existingTag = entityManager.find(Tag.class, tag.getId());
        existingTag.setTagName(tag.getTagName());
        entityManager.merge(existingTag);
    }

    public void delete(Long tagId) throws DAOException {
        Tag tag = entityManager.find(Tag.class, tagId);
        entityManager.remove(tag);
    }

    public List<Tag> selectAll() throws DAOException {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Tag> cq = builder.createQuery(Tag.class);
        Root<Tag> root = cq.from(Tag.class);
        cq.select(root);
        return entityManager.createQuery(cq).getResultList();
    }

    public Tag get(Long id) throws DAOException {
        return entityManager.find(Tag.class,id);
    }
}
