package com.epam.newsmanagement.dao.impl.hibernate;

import com.epam.newsmanagement.dao.INewsDAO;
import com.epam.newsmanagement.entity.*;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.util.DBUtils;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.JoinType;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository("hibernateNewsDAO")
@Profile("hibernate")
@Transactional
public class HibernateNewsDAOImpl implements INewsDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public Long add(News news) throws DAOException {
        Session session = sessionFactory.getCurrentSession();
        return (Long)session.save(news);
    }

    public void update(News news) throws DAOException {
        Session session = sessionFactory.getCurrentSession();
        session.update(news);
    }

    public void delete(Long newsId) throws DAOException {
        Session session = sessionFactory.getCurrentSession();
        News news = (News)session.load(News.class, newsId);
        session.delete(news);
    }

    public List<News> selectAll() throws DAOException {
        Session session = sessionFactory.getCurrentSession();
        List<News> newsList = session.createCriteria(News.class).list();
        return newsList;
    }

    public Long countAll() throws DAOException {
        Session session = sessionFactory.getCurrentSession();
        Long count = (Long)session.createCriteria(News.class.getName()).setProjection(Projections.rowCount()).uniqueResult();
        return count;
    }

    public News get(Long newsId) throws DAOException {
        Session session = sessionFactory.getCurrentSession();
        News news = (News)session.get(News.class,newsId);;
        news.getTagList().size();
        news.getCommentList().size();
        news.getAuthor().getId();
        return news;
    }

    public Author getAuthor(Long newsId) throws DAOException {
        Session session = sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(News.class);
        criteria.add(Restrictions.eq("id",newsId));
        ProjectionList projectionList = Projections.projectionList();
        criteria.createAlias("author", "na", org.hibernate.sql.JoinType.LEFT_OUTER_JOIN);
        projectionList.add(Projections.property("na.id"), "id");
        projectionList.add(Projections.property("na.authorName"), "authorName");

        criteria.setProjection(projectionList);
        criteria.setResultTransformer(Transformers.aliasToBean(Author.class));
        return (Author)criteria.uniqueResult();
    }

    public List<Tag> getTags(Long newsId) throws DAOException {
        Session session = sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(News.class);
        criteria.add(Restrictions.eq("id",newsId));
        ProjectionList projectionList = Projections.projectionList();
        criteria.createAlias("tagList", "nt", org.hibernate.sql.JoinType.INNER_JOIN);
        projectionList.add(Projections.property("nt.id"), "id");
        projectionList.add(Projections.property("nt.tagName"), "tagName");
        criteria.setProjection(projectionList);
        criteria.setResultTransformer(Transformers.aliasToBean(Tag.class));
        return criteria.list();
    }

    public List<Comment> getComments(Long newsId) throws DAOException {
        Session session = sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(News.class);
        criteria.add(Restrictions.eq("id",newsId));
        ProjectionList projectionList = Projections.projectionList();
        criteria.createAlias("commentList", "c", org.hibernate.sql.JoinType.INNER_JOIN);
        projectionList.add(Projections.property("c.id"), "id");
        projectionList.add(Projections.property("c.text"), "text");
        projectionList.add(Projections.property("c.newsId"), "newsId");
        projectionList.add(Projections.property("c.creationDate"));
        criteria.setProjection(projectionList);
        criteria.setResultTransformer(Transformers.aliasToBean(Comment.class));
        return criteria.list();
    }

    public List<String> getTagsNames(Long newsId) throws DAOException {
        Session session = sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(News.class);
        criteria.add(Restrictions.eq("id", newsId));
        ProjectionList projectionList = Projections.projectionList();
        criteria.createAlias("tagList", "nt", org.hibernate.sql.JoinType.INNER_JOIN);
        projectionList.add(Projections.property("nt.tagName"), "tagName");
        criteria.setProjection(projectionList);
        return criteria.list();
    }
}
