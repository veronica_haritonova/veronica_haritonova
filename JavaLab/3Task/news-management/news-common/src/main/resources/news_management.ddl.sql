CREATE SEQUENCE news_seq
START WITH 1 
INCREMENT BY 1 
NOMAXVALUE;


create table news (
   news_id  number(20)   not null
  , title  nvarchar2(30) not null
  , short_text nvarchar2(100) not null
  , full_text nvarchar2(2000) not null
  , creation_date timestamp DEFAULT CURRENT_TIMESTAMP not null 
  , modification_date date default sysdate not null
  , constraint pk_news primary key (news_id)
);


create table author (
   author_id  number(20)   not null
  , author_name  nvarchar2(30) not null 
  , expired timestamp
);

alter table author
  add constraint pk_author
  primary key (author_id);

CREATE SEQUENCE author_seq
START WITH 1 
INCREMENT BY 1 
NOMAXVALUE;

CREATE SEQUENCE tag_seq
START WITH 1 
INCREMENT BY 1 
NOMAXVALUE;


create table tag (
   tag_id  number(20) not null
  ,tag_name  nvarchar2(30) not null 
  ,constraint pk_tag  primary key (tag_id)
);



create table news_author (
   news_id  number(20)   not null
  , author_id  number(20) not null
  , constraint FK_news_author_news_id foreign key (news_id) references news(news_id)
  , constraint FK_news_author_author_id foreign key (author_id) references author(author_id)
);



create table news_tag (
   news_id  number(20) not null
  ,tag_id  number(20) not null
  , constraint FK_news_tag_tag_id foreign key (tag_id) references tag(tag_id)
  , constraint FK_news_tag_news_id foreign key (news_id) references news(news_id)
);


create table comments (
   comment_id  number(20)   not null
  , news_id  number(20) not null
  , comment_text nvarchar2(100) not null 
  , creation_date timestamp DEFAULT CURRENT_TIMESTAMP not null
);

alter table comments
  add constraint pk_comments
  primary key (comment_id);
alter table comments
  add constraint FK_comments_news_id
  foreign key (news_id)
  references news(news_id) ;

CREATE SEQUENCE comments_seq
START WITH 1 
INCREMENT BY 1 
NOMAXVALUE;


create table users (
   user_id  number(20) not null
  ,user_name  nvarchar2(50) not null 
  , login varchar2(30) not null
  , password varchar2(32) not null
  , constraint pk_users primary key (user_id)
);

alter table users
  add constraint ux_users_login
  unique (login);

CREATE SEQUENCE users_seq
START WITH 1 
INCREMENT BY 1 
NOMAXVALUE;


create table roles (
   user_id  number(20) not null
  ,role_name  varchar2(50) not null
  , constraint FK_roles_user_id foreign key (user_id) references users(user_id)
);
