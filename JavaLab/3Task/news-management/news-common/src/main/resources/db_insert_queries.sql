INSERT INTO news (news_id, title, short_text, full_text) 
VALUES (news_seq.nextval, 
DBMS_RANDOM.STRING('A',DBMS_RANDOM.VALUE(1, 30)),
DBMS_RANDOM.STRING('A',DBMS_RANDOM.VALUE(1, 100)),
DBMS_RANDOM.STRING('A',DBMS_RANDOM.VALUE(1, 2000)));


INSERT INTO author (author_id, author_name)
VALUES (author_seq.nextval, 
DBMS_RANDOM.STRING('A',DBMS_RANDOM.VALUE(1, 30)));


INSERT INTO tag (tag_id, tag_name)
VALUES (tag_seq.nextval, 
DBMS_RANDOM.STRING('A',DBMS_RANDOM.VALUE(1, 30)));


INSERT INTO news_tag (news_id, tag_id)
SELECT * FROM (SELECT n.news_id, t.tag_id
FROM news n,tag t 
ORDER BY dbms_random.value)
WHERE ROWNUM <= 100;


INSERT INTO news_author (news_id, author_id)
SELECT * FROM 
(SELECT n.news_id, 
(SELECT * FROM (SELECT a.author_id FROM author a ORDER BY dbms_random.value) WHERE ROWNUM = 1) as a_id
FROM news n
);



INSERT INTO comments (comment_id,news_id,comment_text) 
VALUES (comments_seq.nextval,
(SELECT * FROM (SELECT n.news_id
FROM news n 
ORDER BY DBMS_RANDOM.VALUE
) 
WHERE ROWNUM = 1),
DBMS_RANDOM.STRING('A',DBMS_RANDOM.VALUE(1, 100)));


/*----------- procedures --------------------*/
create or replace PROCEDURE insert_news
AS
BEGIN

  INSERT INTO news (news_id, title, short_text, full_text) 
VALUES (news_seq.nextval, 
DBMS_RANDOM.STRING('A',DBMS_RANDOM.VALUE(1, 30)),
DBMS_RANDOM.STRING('A',DBMS_RANDOM.VALUE(1, 100)),
DBMS_RANDOM.STRING('A',DBMS_RANDOM.VALUE(1, 2000)));
 COMMIT;

END;

create or replace PROCEDURE insert_tag
AS
BEGIN
  INSERT INTO tag (tag_id, tag_name)
VALUES (tag_seq.nextval, 
DBMS_RANDOM.STRING('A',DBMS_RANDOM.VALUE(1, 30)));
 COMMIT;

END;


create or replace PROCEDURE insert_author
AS
BEGIN
  INSERT INTO author (author_id, author_name)
VALUES (author_seq.nextval, 
DBMS_RANDOM.STRING('A',DBMS_RANDOM.VALUE(1, 30)));
 COMMIT;

END;


create or replace PROCEDURE insert_comment
AS
BEGIN
  INSERT INTO comments (comment_id,news_id,comment_text) 
VALUES (comments_seq.nextval,
(SELECT * FROM (SELECT n.news_id
FROM news n 
ORDER BY DBMS_RANDOM.VALUE
) 
WHERE ROWNUM = 1),
DBMS_RANDOM.STRING('A',DBMS_RANDOM.VALUE(1, 100)));
 COMMIT;

END;


create or replace PROCEDURE insert_news_tags
AS
BEGIN
  INSERT INTO news_tag (news_id, tag_id)
SELECT * FROM (SELECT n.news_id, t.tag_id
FROM news n,tag t 
ORDER BY dbms_random.value)
WHERE ROWNUM <= 100;
 COMMIT;

END;

/*------------incorrect-----------------------*/
create or replace PROCEDURE insert_news_author
AS
BEGIN
  INSERT INTO news_author (news_id, author_id)
SELECT * FROM 
(SELECT n.news_id, 
(SELECT * FROM (SELECT a.author_id FROM author a ORDER BY dbms_random.value) WHERE ROWNUM = 1) as a_id
FROM news n
);

 COMMIT;

END;




DECLARE
   a number(2);
BEGIN
	FOR a IN 1 .. 1000 LOOP
   		insert_news();
    END LOOP;
    FOR a IN 1 .. 10 LOOP
   		insert_tag();
    END LOOP;
    FOR a IN 1 .. 10 LOOP
   		insert_author();
    END LOOP;
    FOR a IN 1 .. 100 LOOP
   		insert_comment();
    END LOOP;
    insert_news_tag();
END;










/*----------Incorrect-----------------------------*/
SELECT * FROM (SELECT nt.news_id, author_id FROM 
(SELECT * FROM 
(SELECT n.news_id, a.author_id
FROM news n, author a
ORDER BY dbms_random.value
) ns, news nt
WHERE ns.news_id = nt.news_id)
WHERE ROWNUM = 1
);



SELECT * FROM 
(SELECT * FROM 
(SELECT n.news_id, a.author_id
FROM news n, author a
ORDER BY dbms_random.value
) ns
WHERE ns.news_id = 1110) nt
WHERE ROWNUM = 1
;