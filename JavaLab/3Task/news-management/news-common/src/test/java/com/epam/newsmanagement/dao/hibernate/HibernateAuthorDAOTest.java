package com.epam.newsmanagement.dao.hibernate;

import com.epam.newsmanagement.dao.IAuthorDAO;
import com.epam.newsmanagement.dao.INewsDAO;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import java.io.FileInputStream;
import java.util.Locale;
import java.util.ResourceBundle;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author Veronica_Haritonova
 *         Class for testing basic operations with authors in database
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:test-spring.xml")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
@DatabaseSetup("classpath:dataset.xml")
@ActiveProfiles("hibernate")
public class HibernateAuthorDAOTest {

    @Autowired
    @Qualifier(value = "hibernateAuthorDAO")
    private IAuthorDAO authorDAOImpl;
    @Autowired
    @Qualifier(value = "hibernateNewsDAO")
    private INewsDAO newsDAO;

    /**
     * Test method which inserts author into database
     *
     * @throws DAOException
     */
    @Test
    public void addAuthor() throws DAOException {
        authorDAOImpl.add(new Author("Nick"));
        assertEquals("Amount of updated tags is incorrect", 4, authorDAOImpl.selectAll().size());

    }

    /**
     * Test method which updates author
     *
     * @throws DAOException
     */
    @Test
    public void updateAuthor() throws DAOException {
        Long authorId = 2L;
        authorDAOImpl.update(new Author(authorId, "Nick"));
        assertEquals("Updated name is incorrect", "Nick", authorDAOImpl.get(authorId).getAuthorName());
        assertEquals("News author is incorrect", "Nick",newsDAO.getAuthor(2L).getAuthorName());

    }

    /**
     * Test method which deletes author with given id
     *
     * @throws DAOException
     */
    @Test
    public void deleteAuthor() throws DAOException {
        Long authorId = 3L;
        authorDAOImpl.delete(authorId);
        assertEquals("Amount of updated tags is incorrect", 2, authorDAOImpl.selectAll().size());
    }

    /**
     * Test method which gets all authors from database
     *
     * @throws DAOException
     */
    @Test
    public void selectAllAuthors() throws DAOException {
        assertEquals("Amount of updated tags is incorrect", 3, authorDAOImpl.selectAll().size());
    }

    /**
     * Test method which finds author by its id
     *
     * @throws DAOException
     */
    @Test
    public void findById() throws DAOException {
        Long authorId = 3L;
        String expectedName = "Patrick";
        Author author = authorDAOImpl.get(authorId);
        assertEquals("Name of found author is incorrect", expectedName, author.getAuthorName());
    }

    /**
     * Test method which sets expired date to author with given id
     *
     * @throws DAOException
     */
    @Test
    public void setExpired() throws DAOException {
        Long authorId = 2L;
        authorDAOImpl.setExpired(authorId);
        assertNotNull("Expired date wasn't set", authorDAOImpl.get(authorId).getExpired());
    }

}
