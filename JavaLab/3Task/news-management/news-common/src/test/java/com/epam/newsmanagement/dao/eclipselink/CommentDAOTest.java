package com.epam.newsmanagement.dao.eclipselink;

import com.epam.newsmanagement.dao.ICommentDAO;
import com.epam.newsmanagement.dao.INewsDAO;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author Veronica_Haritonova
 *         Class for testing basic operations with comments in database
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:test-spring.xml")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
@Transactional
@DatabaseSetup("classpath:dataset.xml")
@ActiveProfiles("jpa")
public class CommentDAOTest {
    @Autowired
    @Qualifier("commentDAO")
    ICommentDAO commentDAOImpl;
    @Autowired
    @Qualifier("newsDAO")
    INewsDAO newsDAO;
    /**
     * Test method which inserts comment into database
     *
     * @throws DAOException
     */
    @Test
    public void addComment() throws DAOException {
        assertNotNull(commentDAOImpl);
        commentDAOImpl.add(new Comment("some comment", 2L));
        assertEquals("Amount of all comments after adding new comment is incorrect", 7, commentDAOImpl.selectAll().size());
    }

    /**
     * Test method which deletes comment from database
     *
     * @throws DAOException
     */
    @Test
    public void deleteComment() throws DAOException {
        commentDAOImpl.delete(1L);
        assertEquals("Amount of all comments after deleting existing comment is incorrect", 5, commentDAOImpl.selectAll().size());
        }

    /**
     * Test method which get all comments from database
     *
     * @throws DAOException
     */
    @Test
    public void getComments() throws DAOException {
        assertEquals("Amount of all comments comment is incorrect", 6, commentDAOImpl.selectAll().size());
    }

    /**
     * Test method which updates comment in database
     *
     * @throws DAOException
     */
    @Test
    public void updateComment() throws DAOException {
        Comment comment = new Comment(1L, 2L, "text");
        commentDAOImpl.update(comment);
        assertEquals("NewsId of updated comment is incorrect", comment.getNewsId(), commentDAOImpl.get(comment.getId()).getNewsId());
    }

    /**
     * Test method which select comment by id in database
     *
     * @throws DAOException
     */
    @Test
    public void getComment() throws DAOException {
        Long newsId = 4L;
        Long commentId = 1L;
        assertEquals("NewsId of comment is incorrect", newsId, commentDAOImpl.get(commentId).getNewsId());
    }
}
