package com.epam.newsmanagement.utils;

import com.epam.newsmanagement.util.DataGenerator;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertTrue;

/**
 * @author  Veronica_Haritonova
 */
public class DataGeneratorTest {
    @Rule
    public TemporaryFolder folder = new TemporaryFolder();
    //@Test
    @Ignore
    public void generatorTest() throws IOException {
        String filename = "sql_script.txt";
        File file = folder.newFile/* new File*/(filename);
        DataGenerator generator = new DataGenerator(file);
        generator.generateScript();
        System.out.println(file.length());
        assertTrue(file.length() != 0);
    }
}
