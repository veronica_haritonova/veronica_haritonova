package com.epam.newsmanagement.dao.eclipselink;

import com.epam.newsmanagement.dao.INewsDAO;
import com.epam.newsmanagement.dao.ITagDAO;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author Veronica_Haritonova
 *         Class for testing basic operations with tags in database
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:test-spring.xml")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
@DatabaseSetup("classpath:dataset.xml")
@ActiveProfiles("jpa")
public class TagDAOTest {

    @Autowired
    @Qualifier(value = "tagDAO")
    private ITagDAO tagDAOImpl;

    @Autowired
    @Qualifier(value = "newsDAO")
    private INewsDAO newsDAOImpl;

    /**
     * Test the select method by comparing amount of all tags in database with expected amount
     *
     * @throws DAOException
     */
    @Test
    public void selectAllTags() throws DAOException {
        assertNotNull(tagDAOImpl);
        assertEquals("Amount of tags is incorrect", 5, tagDAOImpl.selectAll().size());
    }
    /**
     * Test method which finds tag with given id
     *
     * @throws DAOException
     */
    @Test
    public void findTag() throws DAOException {
        assertNotNull("Not found tag",tagDAOImpl.get(1L));
    }
    /**
     * Test method which inserts tag into database
     *
     * @throws DAOException
     */
    @Test
    public void addTag() throws DAOException {
        assertNotNull(tagDAOImpl);
        tagDAOImpl.add(new Tag("nature"));
        assertEquals("Amount of new tags is incorrect", 6, tagDAOImpl.selectAll().size());
    }

    /**
     * Test method which deletes tag with given id from database
     *
     * @throws DAOException
     */
    @Test
    public void deleteTag() throws DAOException {
        assertNotNull(tagDAOImpl);
        tagDAOImpl.delete(1L);
        assertEquals("Amount of tags is incorrect", 4, tagDAOImpl.selectAll().size());
  }

    /**
     * Test method which updates given tag
     *
     * @throws DAOException
     */
    @Test
    public void updateTag() throws DAOException {
        tagDAOImpl.update(new Tag(1L, "nature"));
        assertEquals("Not updated value", "nature", tagDAOImpl.get(1L).getTagName());
        assertEquals("Incorrect amount of news", 1, newsDAOImpl.getTags(2L).size());
    }
}

