package com.epam.newsmanagement.dao.eclipselink;

import com.epam.newsmanagement.dao.ISearchDAO;
import com.epam.newsmanagement.entity.NewsWorkItem;
import com.epam.newsmanagement.entity.SearchCriteriaVO;
import com.epam.newsmanagement.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * @author Veronica_Haritonova
 *         Class for testing HibernateSearchDAOImpl
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:test-spring.xml")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
@DatabaseSetup("classpath:dataset.xml")
@ActiveProfiles("jpa")
public class SearchDAOTest {
    @Autowired
    @Qualifier("searchDAO")
    private ISearchDAO searchDAO;

    @Test
    public void searchByFullCriteriaId() throws DAOException {
        List<Long> tagsId = new ArrayList<Long>(2);
        tagsId.add(3L);
        tagsId.add(2L);
        Long authorId = 3L;
        SearchCriteriaVO criteria = new SearchCriteriaVO();
        criteria.setAuthorId(authorId);
        criteria.setTagsId(tagsId);
        int expectedAmount = 1;
        List<NewsWorkItem> foundNews = searchDAO.find(criteria, 1, 5);
        assertEquals("Size of found news is incorrect", expectedAmount, foundNews.size());
    }

    @Test
    public void searchByAuthorId() throws DAOException {
        Long authorId = 3L;
        SearchCriteriaVO criteria = new SearchCriteriaVO();
        criteria.setAuthorId(authorId);
        int expectedAmount = 2;
        List<NewsWorkItem> foundNews = searchDAO.find(criteria, 1, 5);
        System.out.println(foundNews);
        assertEquals("Size of found news by author is incorrect", expectedAmount, foundNews.size());
    }
    @Test
    public void searchByTagsId() throws DAOException {
        Long tagId = 3L;
        Long tag2Id = 2L;
        List<Long> tagsId = new ArrayList<Long>(2);
        tagsId.add(tagId);
        tagsId.add(tag2Id);
        SearchCriteriaVO criteria = new SearchCriteriaVO();
        criteria.setTagsId(tagsId);
        int expectedAmount = 1;
        List<NewsWorkItem> foundNews = searchDAO.find(criteria, 1, 5);
        System.out.println(foundNews);
        assertEquals("Size of found news by tags is incorrect", expectedAmount, foundNews.size());
    }
    @Test
    public void searchByEmptyCriteriaId() throws DAOException {
        SearchCriteriaVO criteria = new SearchCriteriaVO();
        int expectedAmount = 5;
        List<NewsWorkItem> foundNews = searchDAO.find(criteria, 1, expectedAmount);
        System.out.println(foundNews);
        assertEquals("Size of found news is incorrect", expectedAmount, foundNews.size());
    }
    @Test
    public void searchByEmptyCriteria() throws DAOException {
        SearchCriteriaVO criteria = new SearchCriteriaVO();
        int expectedAmount = 5;
        List<NewsWorkItem> foundNews = searchDAO.find(criteria, 6, expectedAmount);
        System.out.println(foundNews);
        assertEquals("Size of found news is incorrect", expectedAmount, foundNews.size());
    }

    /**
     * Test search of news when search criteria contains only tagsId
     * Must return empty result
     * @throws DAOException
     */
    @Test
    public void emptyResultByTagsId() throws DAOException {
        Long tagId = 2L;
        Long tag2Id = 1L;
        List<Long> tagsId = new ArrayList<Long>(2);
        tagsId.add(tagId);
        tagsId.add(tag2Id);
        SearchCriteriaVO criteria = new SearchCriteriaVO();
        criteria.setTagsId(tagsId);
        int expectedAmount = 0;
        List<NewsWorkItem> foundNews = searchDAO.find(criteria,1, 5);
        assertEquals("Size of found news by tags is incorrect", expectedAmount, foundNews.size());
    }
    /**
     * Test search of news when search criteria contains only tagsId
     * Must return empty result
     * @throws DAOException
     */
    @Test
    public void searchByOneTagId() throws DAOException {
        Long tagId = 2L;
        List<Long> tagsId = new ArrayList<Long>(1);
        tagsId.add(tagId);
        SearchCriteriaVO criteria = new SearchCriteriaVO();
        criteria.setTagsId(tagsId);
        int expectedAmount = 1;
        List<NewsWorkItem> foundNews = searchDAO.find(criteria,1, 5);
        assertEquals("Size of found news by tags is incorrect", expectedAmount, foundNews.size());
    }


    /**
     * Test method which counts news when search criteria has only author
     *
     * @throws DAOException
     */
    @Test
    public void countFoundNewsByAuthorId() throws DAOException {
        Long authorId = 3L;
        SearchCriteriaVO criteria = new SearchCriteriaVO();
        criteria.setAuthorId(authorId);
        Long expectedAmount = 2L;
        Long actualAmount = searchDAO.countFoundNews(criteria);
        assertEquals("Size of found news by author is incorrect", expectedAmount, actualAmount);
    }
    /**
     * Test method which counts news when search criteria contains tagsId and authorId
     *
     * @throws DAOException
     */
    @Test
    public void countFoundNewsByFullCriteriaId() throws DAOException {
        List<Long> tagsId = new ArrayList<Long>(2);
        tagsId.add(3L);
        tagsId.add(2L);
        Long authorId = 3L;
        SearchCriteriaVO criteria = new SearchCriteriaVO();
        criteria.setAuthorId(authorId);
        criteria.setTagsId(tagsId);
        Long expectedAmount = 1L;
        Long actualAmount = searchDAO.countFoundNews(criteria);
        assertEquals("Size of found news is incorrect", expectedAmount, actualAmount);
    }

    /**
     * Test method which counts news when search criteria contains only tagsId
     *
     * @throws DAOException
     */
    @Test
    public void countFoundNewsByTagsId() throws DAOException {
        Long tagId = 3L;
        List<Long> tagsId = new ArrayList<Long>(1);
        tagsId.add(tagId);
        SearchCriteriaVO criteria = new SearchCriteriaVO();
        criteria.setTagsId(tagsId);
        Long expectedAmount = 1L;
        Long actualAmount = searchDAO.countFoundNews(criteria);
        assertEquals("Size of found news by tags is incorrect", expectedAmount, actualAmount);
    }

    /**
     * Test method which counts news when search criteria is empty
     *
     * @throws DAOException
     */
    @Test
    public void countFoundNewsByEmptyCriteriaId() throws DAOException {
        SearchCriteriaVO criteria = new SearchCriteriaVO();
        Long expectedAmount = 11L;
        Long actualAmount = searchDAO.countFoundNews(criteria);
        assertEquals("Size of found news is incorrect", expectedAmount, actualAmount);
    }
    /**
     * Test search of news when search criteria contains tags and author
     *
     * @throws DAOException
     */
    @Test
    public void searchIdByFullCriteria() throws DAOException {
        List<Long> tagsId = new ArrayList<Long>(2);
        tagsId.add(3L);
        tagsId.add(2L);
        Long authorId = 3L;
        SearchCriteriaVO criteria = new SearchCriteriaVO();
        criteria.setAuthorId(authorId);
        criteria.setTagsId(tagsId);
        int expectedAmount = 1;
        List<Long> foundIds = searchDAO.findIdList(criteria,1,10);
        int actualAmount = foundIds.size();
        assertEquals("Size of found news is incorrect", expectedAmount, actualAmount);
        Long expectedNewsId = 9L;
        assertEquals("Found news is incorrect", expectedNewsId,foundIds.get(0));
    }
}
