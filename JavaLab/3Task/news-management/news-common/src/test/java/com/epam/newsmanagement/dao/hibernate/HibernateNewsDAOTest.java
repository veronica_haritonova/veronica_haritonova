package com.epam.newsmanagement.dao.hibernate;

import com.epam.newsmanagement.dao.IAuthorDAO;
import com.epam.newsmanagement.dao.INewsDAO;
import com.epam.newsmanagement.dao.ITagDAO;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static org.junit.Assert.*;
import static org.junit.Assert.assertNotNull;

/**
 * @author Veronica_Haritonova
 *         Class for testing operations with news
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:test-spring.xml")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
@Transactional
@DatabaseSetup("classpath:dataset.xml")
@ActiveProfiles("hibernate")
public class HibernateNewsDAOTest {
    @Autowired
    @Qualifier(value = "hibernateNewsDAO")
    private INewsDAO newsDAO;

    @Autowired
    @Qualifier(value = "hibernateAuthorDAO")
    private IAuthorDAO authorDAO;

    @Autowired
    @Qualifier(value = "hibernateTagDAO")
    private ITagDAO tagDAO;

    /**
     * Test method which inserts news into database
     *
     * @throws DAOException
     */
    @Test
    public void addNews() throws DAOException {
        News news = new News("News1", "short text", "full text");
        news.setCreationDate(Calendar.getInstance().getTime());
        news.setModificationDate(Calendar.getInstance().getTime());
        Author author = authorDAO.get(2L);
        List<Tag> tags = tagDAO.selectAll();
        news.setTagList(tags);
        news.setAuthor(author);
        Long id = newsDAO.add(news);
        news = newsDAO.get(id);
        assertNotNull("No inserted news", news);
        assertEquals("Wrong amount of tags", tags.size(),news.getTagList().size());
        assertEquals("Wrong author", author.getId(), news.getAuthor().getId());
    }

    /**
     * Test method which updates news in database
     *
     * @throws DAOException
     */
    @Test
    public void updateNews() throws DAOException {
        String newTitle = "News1";
        Long id = 2L;
        News news = newsDAO.get(2L);
        news.setTitle(newTitle);
        Long authorId = 3L;
        news.setAuthor(authorDAO.get(authorId));
        news.setTagList(new ArrayList<Tag>());
        news.setModificationDate(Calendar.getInstance().getTime());
        newsDAO.update(news);
        assertEquals("Updates news title is incorrect", newTitle, newsDAO.get(id).getTitle());
        assertEquals("Updates news author is incorrect",authorId, newsDAO.get(id).getAuthor().getId());
    }

    /**
     * Test method which deletes news from database
     *
     * @throws DAOException
     */
    @Test
    public void deleteNews() throws DAOException {
        Long id = 2L;
        newsDAO.delete(id);
        assertEquals("Amount of all news after deleting is incorrect", 10, newsDAO.selectAll().size());
        assertTrue("Tags weren't deleted", newsDAO.getTags(id).isEmpty());
        assertTrue("Comments weren't deleted", newsDAO.getComments(id).isEmpty());
        assertNull("Author wasn't deleted", newsDAO.getAuthor(id));
    }

    /**
     * Test method which gets all news from database
     *
     * @throws DAOException
     */
    @Test
    public void selectAll() throws DAOException {
        assertEquals("Amount of all news is incorrect", 11, newsDAO.selectAll().size());
    }

    /**
     * Test method which counts all news in database
     *
     * @throws DAOException
     */
    @Test
    public void countAllNews() throws DAOException {
        Long expectedAmount = 11L;
        assertEquals("Amount of all news is incorrect", expectedAmount, newsDAO.countAll());
    }

    /**
     * Test method which select news with given id from database
     *
     * @throws DAOException
     */
    @Test
    public void getNewsById() throws DAOException {
        News news = newsDAO.get(2L);
        System.out.println(news.getCommentList());
        assertNotNull("No given news", news);
        assertFalse("No tags", news.getTagList().isEmpty());
        assertNotNull("No author", news.getAuthor());
        assertEquals("Title is incorrect", "News2", news.getTitle());
    }

    /**
     * Test method which gets author of news with given id
     *
     * @throws DAOException
     */
    @Test
    public void getNewsAuthor() throws DAOException {
        Long id = 2L;
        Long authorId = 2L;
        assertEquals("Author is incorrect", authorId, newsDAO.getAuthor(id).getId());
    }

    /**
     * Test method which gets all tags of news with given id
     *
     * @throws DAOException
     */
    @Test
    public void getNewsTags() throws DAOException {
        assertFalse("No tags in news", newsDAO.getTags(2L).isEmpty());
    }

    /**
     * Test method which gets all comments of news with given id
     *
     * @throws DAOException
     */
    @Test
    public void getComments() throws DAOException {
        assertFalse("No comments of news", newsDAO.getComments(2L).isEmpty());
        assertEquals("Wrong amount of comments", 2,  newsDAO.getComments(9L).size() );
        assertTrue("Wrong amount of comments", newsDAO.getComments(12L).isEmpty());
    }

    /**
     * Test method which gets list tags' names of defined news
     * @throws DAOException
     */
    @Test
    public void getTagsNames() throws DAOException {
        Long newsId = 9L;
        int expectedAmount = 2;
        List<String> tagNames = newsDAO.getTagsNames(newsId);
        assertEquals("Amount of tags is incorrect", expectedAmount, tagNames.size());
        assertEquals("Tag name is incorrect", "economy", tagNames.get(0));
    }
}
