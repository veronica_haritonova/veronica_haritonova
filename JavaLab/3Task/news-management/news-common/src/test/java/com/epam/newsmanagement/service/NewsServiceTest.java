package com.epam.newsmanagement.service;

import com.epam.newsmanagement.dao.INewsDAO;
import com.epam.newsmanagement.dao.ISearchDAO;
import com.epam.newsmanagement.entity.*;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.impl.NewsServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Class for testing NewsServiceImpl
 */
@RunWith(MockitoJUnitRunner.class)
public class NewsServiceTest {
    @Mock
    private INewsDAO newsDAO;
    @Mock
    private ISearchDAO searchDAO;

    private INewsService newsService;

    private News news;

    private Author author;

    private Long newsId = 1L;

    @Before
    public void createData() {
        newsService = new NewsServiceImpl(newsDAO, searchDAO);
        news = new News(newsId, "title", "short text", "full text");
        author = new Author();
    }

    @Test
    public void getFullNews() throws ServiceException, DAOException {
        when(newsDAO.get(anyLong())).thenReturn(news);
        News foundNews = newsService.getNews(newsId);
        assertNotNull("No found news", foundNews);
    }

    @Test
    public void addNews() throws ServiceException, DAOException {
        newsService.addNews(news);
        verify(newsDAO).add(news);
    }

    @Test
    public void getNews() throws ServiceException, DAOException {
        when(newsDAO.get(anyLong())).thenReturn(news);
        assertNotNull(newsService.getNews(newsId));
    }

    @Test
    public void updateNews() throws ServiceException, DAOException {
        newsService.updateNews(news);
        verify(newsDAO).update(news);
    }

    @Test
    public void deleteNews() throws ServiceException, DAOException {
        newsService.deleteNews(newsId);
        verify(newsDAO).delete(newsId);
    }
}
