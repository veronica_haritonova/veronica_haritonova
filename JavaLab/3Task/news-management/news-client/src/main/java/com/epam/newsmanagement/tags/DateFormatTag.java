package com.epam.newsmanagement.tags;

import com.epam.newsmanagement.utils.NewsUtils;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * @author  Veronica_Haritonova
 *
 * Tag for displaying date in specific format
 */
public class DateFormatTag extends SimpleTagSupport {

    private Date date;

    @Override
    public void doTag() throws JspException {
        try {
            PageContext pageContext = (PageContext) getJspContext();
            JspWriter out = pageContext.getOut();
            SimpleDateFormat format;
            HttpServletRequest request = (HttpServletRequest)pageContext.getRequest();
            Locale locale = RequestContextUtils.getLocale(request);
            format = NewsUtils.getLocaleDateFormat(locale);
            String dateString = format.format(date);
            out.write(dateString);
        } catch (IOException e) {
            throw new JspTagException(e.getMessage(), e);
        }
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
