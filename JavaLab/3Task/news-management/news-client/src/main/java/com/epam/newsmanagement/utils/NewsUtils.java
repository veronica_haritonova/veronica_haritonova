package com.epam.newsmanagement.utils;

import com.epam.newsmanagement.entity.NewsWorkItem;
import com.epam.newsmanagement.entity.SearchCriteriaVO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.INewsService;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

/**
 * @author  Veronica_Haritonova
 */
public class NewsUtils {
    @Autowired
    private INewsService newsService;

    public static final int NEWS_AMOUNT_ON_PAGE = 5;
    private static final String EN = "en";

    public List<Long> getNewsIdList(SearchCriteriaVO criteria, int page) throws ServiceException {
        return newsService.getNewsIdList(criteria, ((page - 1) * NEWS_AMOUNT_ON_PAGE) + 1, NEWS_AMOUNT_ON_PAGE);
    }
    public boolean isFirstNewsOnPage(int newsIndexInList) {
        return (newsIndexInList + 1) % NEWS_AMOUNT_ON_PAGE == 1;
    }
    public boolean isLastNewsOnPage(int newsIndexInList) {
        return (newsIndexInList + 1) % NEWS_AMOUNT_ON_PAGE == 0;
    }
    public List<NewsWorkItem> getNewsList(SearchCriteriaVO criteria, int page) throws ServiceException {
        return newsService.findByCriteria(criteria, ((page - 1) * NEWS_AMOUNT_ON_PAGE) + 1, NEWS_AMOUNT_ON_PAGE);
    }
    public void addPreviousNewsIdList(List<Long> newsIdList, int newsIndexInList,SearchCriteriaVO criteria, int page)
            throws ServiceException {
        if (newsIndexInList == 0 && page > 1) {
            int previousPage = page - 1;
            List<Long> previousNewsIdList = getNewsIdList(criteria, previousPage);
            newsIdList.addAll(0, previousNewsIdList);
        }
    }

    public void addNextNewsIdList(List<Long> newsIdList, int newsIndexInList, int listSize, SearchCriteriaVO criteria, int page)
            throws ServiceException {
        if (newsIndexInList == listSize - 1) {
            int nextPage = page + 1;
            List<Long> nextNewsIdList = getNewsIdList(criteria,nextPage);
            newsIdList.addAll(nextNewsIdList);
        }
    }
    public static SimpleDateFormat getLocaleDateFormat(Locale locale) {
        SimpleDateFormat dateFormat;
        if(locale.getLanguage().equals(EN)) {
            dateFormat = new SimpleDateFormat("dd/MM/yyyy"); // dd.MM.yyyy
        } else {
            dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        }
        return dateFormat;
    }
}
