<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="ctg" uri="customtags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<div id="newsFilter">
    <table id="newsFilterTable">
         <tr>
              <td>
                    <form  action="${ pageContext.request.contextPath }/news" method="get">
                        <div class="multiselect">
                                <div class="selectBox" onclick="showCheckboxes()">
                                    <select>
                                        <option disabled><spring:message code="tags.choose"/></option>
                                    </select>
                                    <div class="overSelect"></div>
                                </div>
                                <div id="checkboxes">
                                    <ctg:tagsCheckboxes selectedTagsId="${searchCriteria.tagsId}" tagList="${tagList}"/>
                                    </div>
                         </div>
                        <select name="authorId">
                            <option selected disabled><spring:message code="author.choose"/></option>
                            <ctg:authorsSelect selectedAuthorId="${searchCriteria.authorId}" authorList="${authorList}"/>
                        </select>
                        <input type="hidden" name="isNewCriteria" value="true"/>
                        <input type="submit" id="filterButton" value="<spring:message code="filter"/>">
                    </form>
              </td>
              <td></td>
              <td>
                    <form action="${ pageContext.request.contextPath }/news/resetFilter" method="get">
                    <input type="submit" id="resetFilterButton" value="<spring:message code="reset"/>">
                    </form>
              </td>
         </tr>
    </table>
</div>
<div id="newsList">
    <c:forEach var="news" items = "${newsList}">
        <div class="shortNews">
            <h3 class="newsTitle">${news.title}</h3>
            <span class="authorName">
                <spring:message code="news.author" arguments="${news.authorName}"/>
            </span>
            <span class="creationDate">
                <ctg:formatDate date="${news.creationDate}"/>
            </span>
            <p class="shortText">${news.shortText}</p>
            <span class="newsViewLink">
                 <a href="${ pageContext.request.contextPath }/news/${news.id}?page=${page}" ><spring:message code="news.view"/></a>
            </span>
            <span class="commentsAmount"><spring:message code="comments"/>(${news.commentsAmount})</span>
            <span class="tagNames">
                <c:forEach var="tag" items = "${news.tagsNames}" varStatus="status">
                    ${tag}
                    <c:if test="${status.count < fn:length(news.tagsNames)}">,</c:if>
                </c:forEach>
            </span>
        </div>
    </c:forEach>
</div>
<c:choose>
    <c:when test="${newsAmount != 0}">
        <div class="pager">
            <ctg:newsPager generalAmount="${newsAmount}" page = "${page}"/>
        </div>
    </c:when>
    <c:otherwise>
        <spring:message code="news.not.found"/>
    </c:otherwise>
</c:choose>


